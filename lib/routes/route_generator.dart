import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:sipolair/core/params/register_params.dart';
import 'package:sipolair/core/utils/pref_utils.dart';
import 'package:sipolair/cubit/bimas/bimas_cubit.dart';
import 'package:sipolair/cubit/bimas_form/bimas_form_cubit.dart';
import 'package:sipolair/cubit/patrol/patrol_cubit.dart';
import 'package:sipolair/cubit/patrol_form/patrol_form_cubit.dart';
import 'package:sipolair/cubit/profile/profile_cubit.dart';
import 'package:sipolair/cubit/report/cubit.dart';
import 'package:sipolair/cubit/report_form/cubit.dart';
import 'package:sipolair/models/report_detail.dart';
import 'package:sipolair/repositories/bimas_repository.dart';
import 'package:sipolair/repositories/patrol_repository.dart';
import 'package:sipolair/repositories/profile_repository.dart';
import 'package:sipolair/repositories/report_repository.dart';
import 'package:sipolair/routes/routes.dart';
import 'package:sipolair/screen/helpers/shared_axis_route.dart';
import 'package:sipolair/screen/ui/authentication/login_page.dart';
import 'package:sipolair/screen/ui/authentication/register_detail_page.dart';
import 'package:sipolair/screen/ui/authentication/register_page.dart';
import 'package:sipolair/screen/ui/bimas/bimas_detail_page.dart';
import 'package:sipolair/screen/ui/bimas/bimas_form_page.dart';
import 'package:sipolair/screen/ui/home_page.dart';
import 'package:sipolair/screen/ui/patrol/patrol_detail_page.dart';
import 'package:sipolair/screen/ui/patrol/patrol_form_page.dart';
import 'package:sipolair/screen/ui/profile/profile_page.dart';
import 'package:sipolair/screen/ui/report/detail_report_page.dart';
import 'package:sipolair/screen/ui/report/detail_report_status.dart';
import 'package:sipolair/screen/ui/report/report_form_page.dart';
import 'package:sipolair/screen/widgets/app_webview.dart';

class RouteGenerator {
  static RouteGenerator instance = Get.find();

  static GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final arguments = settings.arguments;

    switch (settings.name) {
      case '/':
        return MaterialPageRoute(
            builder: (context) {
              if (PrefUtil.instance.isLogin) {
                return BlocProvider(
                    create: (context) =>
                        ReportCubit(Get.find<ReportRepository>())..getReports(),
                    child: HomePage());
              } else {
                return LoginPage();
              }
            },
            settings: settings);
      case Routes.LOGINPAGE:
        return SharedAxisPageRoute(
            page: LoginPage(),
            transitionType: SharedAxisTransitionType.horizontal,
            settings: settings);

      case Routes.REGISTERPAGE:
        return SharedAxisPageRoute(
            page: RegisterPage(),
            transitionType: SharedAxisTransitionType.horizontal,
            settings: settings);

      case Routes.REGISTERDETAILPAGE:
        var registerParam = arguments as RegisterParams;
        return SharedAxisPageRoute(
            page: RegisterDetailPage(params: registerParam),
            transitionType: SharedAxisTransitionType.horizontal,
            settings: settings);

      case Routes.HOMEPAGE:
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                create: (context) =>
                    ReportCubit(Get.find<ReportRepository>())..getReports(),
                child: HomePage()),
            settings: settings);

      case Routes.DETAILREPORTPAGE:
        var idReport = arguments as int;
        return MaterialPageRoute(
            builder: (context) => DetailReportPage(
                  idReport: idReport,
                ),
            settings: settings);

      case Routes.DETAILREPORTSTATUSPAGE:
        var report = arguments as ReportDetail;
        return MaterialPageRoute(
            builder: (context) => DetailReportStatus(
                  report: report,
                ),
            settings: settings);

      case Routes.REPORTFORMPAGE:
        return MaterialPageRoute(
            fullscreenDialog: true,
            builder: (context) => BlocProvider(
                create: (context) =>
                    ReportFormCubit(Get.find<ReportRepository>())
                      ..getReportForm(),
                child: ReportFormPage()),
            settings: settings);

      case Routes.BIMASTFORMPAGE:
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                create: (context) => BimasFormCubit(Get.find<BimasRepository>())
                  ..getReportForm(),
                child: BimasFormPage()),
            settings: settings);

      case Routes.DETAILBIMASPAGE:
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                create: (context) =>
                    BimasCubit(repository: Get.find<BimasRepository>())
                      ..getDetailBimas(id: arguments as int),
                child: DetailBimasPage(
                  id: arguments as int,
                )),
            settings: settings);

      case Routes.PATROLFORMPAGE:
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                create: (context) =>
                    PatrolFormCubit(Get.find<PatrolRepository>())
                      ..getReportForm(),
                child: PatrolFormPage()),
            settings: settings);

      case Routes.DETAILPATROLPAGE:
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                create: (context) => PatrolCubit(Get.find<PatrolRepository>())
                  ..getDetailReport(id: arguments as int),
                child: DetailPatrolPage(
                  id: arguments as int,
                )),
            settings: settings);

      case Routes.PROFILEPAGE:
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                create: (context) =>
                    ProfileCubit(Get.find<ProfileRepository>())..getProfile(),
                child: ProfilePage()),
            settings: settings);

      case Routes.APPWEBVIEW:
        var url = arguments as String;
        return MaterialPageRoute(
            fullscreenDialog: true,
            builder: (context) => AppWebView(
                  url: url,
                ),
            settings: settings);

      default:
        return MaterialPageRoute(
            builder: (context) => LoginPage(), settings: settings);
    }
  }
}
