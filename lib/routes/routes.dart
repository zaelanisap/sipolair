class Routes {
  static const String INITIALPAGE = '/';
  static const String LOGINPAGE = '/login';
  static const String REGISTERPAGE = '/register';
  static const String REGISTERDETAILPAGE = '/register-detail';
  static const String HOMEPAGE = '/home';

  static const String BIMASTFORMPAGE = '/bimas-form';
  static const String DETAILBIMASPAGE = '/detail-bimas';

  static const String PATROLFORMPAGE = '/patrol-form';
  static const String DETAILPATROLPAGE = '/detail-patrol';

  static const String DETAILREPORTPAGE = '/detail-report';
  static const String DETAILREPORTSTATUSPAGE = '/detail-report/status';
  static const String REPORTFORMPAGE = '/add-report';

  static const String PROFILEPAGE = '/profile';
  static const String APPWEBVIEW = '/webview';
}
