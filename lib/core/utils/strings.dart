class Strings {
  static const String BASE_URL = 'http://122.50.5.62:8007/sippolair-web/';
  static const String logoApp = 'assets/images/logo.jpeg';
  static const String dummyAvatar =
      'https://images.unsplash.com/photo-1511367461989-f85a21fda167?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=200&q=80';
}
