import 'dart:io';

import 'package:dio/dio.dart' hide Headers;

class CacheException implements Exception {}

class AppException implements Exception {
  int? _errorCode;
  String _errorMessage = "";
  String _errorTitle = "";

  AppException.withError(DioError error) {
    _handleError(error);
  }

  AppException.withMessage(String title, String message, {required int code}) {
    this._errorCode = code;
    this._errorMessage = message;
    this._errorTitle = title;
  }

  int? get errorCode => _errorCode;
  String get errorTitle => _errorTitle;
  String get errorMessage => _errorMessage;

  void _handleError(DioError error) {
    _errorCode = error.response?.statusCode;

    if (error.error is SocketException) {
      _errorTitle = "No connection found";
      _errorMessage = "Please check your internet connection and try again.";
      return;
    }

    switch (error.type) {
      case DioErrorType.cancel:
        _errorTitle = "Oh no...";
        _errorMessage = "Request was cancelled";
        break;
      case DioErrorType.connectTimeout:
        _errorTitle = "Connection timeout...";
        _errorMessage = "Please check your internet connection and try again.";
        break;
      case DioErrorType.other:
        _errorTitle = "Oh no...";
        _errorMessage = "Internal Server Error";
        break;
      case DioErrorType.receiveTimeout:
        _errorTitle = "Oh no...";
        _errorMessage = "Receive timeout in connection";
        break;
      case DioErrorType.response:
        if (error.response?.data != null) {
          if (error.response?.statusCode == 500) {
            _errorTitle = "Oh no...";
            _errorMessage = 'Internal Server Error';
            break;
          }

          // if (error.response?.statusCode == 400) {
          //   _errorTitle = "Oh no...";
          //   _errorMessage = 'Bad request';
          //   break;
          // }

          if (error.response?.data['message'] == 'Unauthorized User') {
            _errorTitle = "Unauthorized User";
            _errorMessage =
                'Your session is ended. Please login again to use this App.';
            break;
          }
          var data = error.response?.data;

          if (data is String) {
            _errorTitle = "Oh no...";
            _errorMessage = 'Internal Server Error';
            return;
          }
          _errorTitle = "Failed";
          _errorMessage = '${data['message']}';
          break;
        }
        _errorTitle = "Something went wrong";
        _errorMessage = 'Try refreshing the app';
        break;
      case DioErrorType.sendTimeout:
        _errorTitle = "Oh no...";
        _errorMessage = "Receive timeout in send request";
        break;
    }
  }
}
