import 'dart:convert';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:sipolair/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PrefUtil {
  static PrefUtil instance = Get.find<PrefUtil>();
  SharedPreferences _pref = Get.find<SharedPreferences>();

  bool get showOnboarding => !(_pref.containsKey('onboarding'));

  void setOnboarding() => _pref.setBool('onboarding', false);

  bool get isFCMChannelAlreadyCreated => _pref.containsKey('fcm_channel');

  void setFCMChannel() => _pref.setBool('fcm_channel', true);

  void saveUserData(User user) async {
    String userJson = jsonEncode(user.toJson());
    _pref.setString('user', userJson);
  }

  void saveToken(String token) {
    _pref.setString('access_token', token);
  }

  User? getUserData() {
    User? user;
    String? userJson = _pref.getString('user');

    if (userJson != null) {
      Map<String, dynamic> map = jsonDecode(userJson);
      user = User.fromJson(map);
    }

    return user;
  }

  int? get idUser {
    var user = getUserData();
    return user?.id;
  }

  int? get idZone {
    var user = getUserData();
    return user?.idZone;
  }

  int? get idAgency {
    var user = getUserData();
    return user?.idAgency;
  }

  String? get avatar {
    var user = getUserData();
    return user?.account?.avatar ?? user?.avatar;
  }

  String? get token => _pref.getString('access_token');

  bool get isLogin => _pref.containsKey('user');

  void clearLogin() {
    _pref.clear();
    setOnboarding();
    Get.find<GetStorage>().erase();
  }
}
