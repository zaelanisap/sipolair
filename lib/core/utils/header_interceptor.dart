import 'package:dio/dio.dart';
import 'pref_utils.dart';

class HeaderInterceptor extends Interceptor {
  HeaderInterceptor();

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    final _pref = PrefUtil.instance;

    if (options.headers.containsKey('requirestoken')) {
      options.headers.remove('requirestoken');
      String? token = _pref.token;
      options.headers['authorization'] = 'Bearer $token';
    }

    if (options.queryParameters.containsKey('requiresuserid')) {
      options.queryParameters.remove('requiresuserid');
      print('idUSER = ${_pref.idUser}');

      options.queryParameters['id_user'] = _pref.idUser;
    }

    if (options.queryParameters.containsKey('requiresid')) {
      options.queryParameters.remove('requiresid');
      print('idUSER = ${_pref.idUser}');
      options.queryParameters['id'] = _pref.idUser;
    }

    return handler.next(options);

    // if (options.queryParameters.containsKey('requirescompanyid')) {
    //   options.queryParameters.remove('requirescompanyid');
    //   options.queryParameters['id_company'] = _pref.companyId;
    // }
  }
}
