import 'package:connectivity/connectivity.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

abstract class NetworkInfo {
  Future<bool> get isConnected;
}

class NetworkInfoImpl implements NetworkInfo {
  final Connectivity connectivity;

  NetworkInfoImpl(this.connectivity);

  @override
  Future<bool> get isConnected async {
    var result = await connectivity.checkConnectivity();

    if (result != ConnectivityResult.none) {
      return await internetChecker;
    }

    return false;
  }

  Future<bool> get internetChecker async {
    var result = await InternetConnectionChecker().hasConnection;

    return result;
  }
}
