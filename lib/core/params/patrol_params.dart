class PatrolParams {
  PatrolParams(
      {required this.idShip,
      required this.idPatrol,
      required this.contents,
      required this.location,
      required this.date,
      required this.startTime,
      required this.endTime,
      this.files});

  final int idShip;
  final int idPatrol;
  final String contents;
  final String location;
  final String date;
  final String startTime;
  final String endTime;
  final List<String>? files;

  factory PatrolParams.fromJson(Map<String, dynamic> json) => PatrolParams(
      idShip: json["id_ship"],
      idPatrol: json["id_patrol"],
      contents: json["contents"],
      location: json["location"],
      date: json["date"],
      startTime: json["start_time"],
      endTime: json["end_time"],
      files: json["files"]);

  Map<String, dynamic> toJson() => {
        "id_ship": idShip,
        "id_patrol": idPatrol,
        "contents": contents,
        "location": location,
        "date": date,
        "start_time": startTime,
        "end_time": endTime,
        "files": files
      };
}
