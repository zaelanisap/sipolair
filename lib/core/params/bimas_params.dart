class BimasParams {
  BimasParams(
      {required this.title,
      required this.contents,
      required this.pic,
      required this.location,
      required this.totalParticipant,
      required this.date,
      required this.time,
      required this.timezone,
      this.files});

  final String title;
  final String contents;
  final String pic;
  final String location;
  final int totalParticipant;
  final String date;
  final String time;
  final String timezone;
  final List<String>? files;

  factory BimasParams.fromJson(Map<String, dynamic> json) => BimasParams(
      title: json["title"],
      contents: json["contents"],
      pic: json["pic"],
      location: json["location"],
      totalParticipant: json["num_of_participants"],
      date: json["date"],
      time: json["time"],
      timezone: json["timezone"],
      files: json["files"]);

  Map<String, dynamic> toJson() => {
        "title": title,
        "contents": contents,
        "pic": pic,
        "location": location,
        "num_of_participants": totalParticipant,
        "date": date,
        "time": time,
        "timezone": timezone,
        "files": files
      };
}
