import 'package:sipolair/models/report_detail.dart';

class ReportParams {
  int? idUser;
  String? code;
  String? date;
  String? time;
  String? address;
  String? chronology;
  String? evidence;
  String? article;
  int? idCategory;
  int? idShip;
  String? officer;
  List<Detail>? suspected;
  List<String?>? attachments;
  String? element;
  ReportParams({
    this.idUser,
    this.code,
    this.date,
    this.time,
    this.address,
    this.idCategory,
    this.chronology,
    this.evidence,
    this.suspected,
    this.attachments,
    this.article,
    this.idShip,
    this.officer,
    this.element,
  });

  bool get isAttachmentEmpty => this.attachments?.isEmpty ?? true;
  bool get isSuspectedEmpty => this.suspected?.isEmpty ?? true;

  Map<String, dynamic> toJson() {
    return {
      'id_user': idUser,
      'code': code,
      'date': date,
      'time': time,
      'tkp': address,
      'chronology': chronology,
      'evidence': evidence,
      'article': article,
      'id_ship': idShip,
      'officer': officer,
      'element': element,
      if (idCategory != null) 'id_criminal_category': idCategory,
      if (suspected != null)
        'details': suspected!.map((x) => x.toJson()).toList(),
      if (attachments != null) 'attachments': attachments,
    };
  }

  factory ReportParams.fromJson(Map<String, dynamic> json) => ReportParams(
      idCategory: json['id_criminal_category'],
      code: json["code"],
      idUser: json["id_user"],
      idShip: json['id_ship'],
      officer: json['officer'],
      address: json["tkp"],
      chronology: json["chronology"],
      evidence: json["evidence"],
      article: json['article'],
      date: json["date"],
      time: json["time"],
      element: json["element"],
      suspected: json['details'] != null
          ? List<Detail>.from(json["details"].map((x) => Detail.fromJson(x)))
          : null,
      attachments: json['attachments'] != null
          ? List<String>.from(
              json['attachments'].map((e) => e as String).toList())
          : null);

  @override
  String toString() {
    return 'ReportParams(idUser: $idUser, code: $code, date: $date, time: $time, address: $address, chronology: $chronology, evidence: $evidence, article: $article, suspected: $suspected, attachments: $attachments)';
  }

  ReportParams copyWith(
      {int? idUser,
      String? code,
      String? date,
      String? time,
      String? address,
      String? chronology,
      String? evidence,
      String? article,
      int? idZone,
      int? idCategory,
      List<Detail>? suspected,
      List<String?>? attachments,
      int? idShip,
      String? officer,
      String? element}) {
    return ReportParams(
        idUser: idUser ?? this.idUser,
        code: code ?? this.code,
        date: date ?? this.date,
        time: time ?? this.time,
        address: address ?? this.address,
        idCategory: idCategory ?? this.idCategory,
        chronology: chronology ?? this.chronology,
        evidence: evidence ?? this.evidence,
        article: article ?? this.article,
        suspected: suspected ?? this.suspected,
        attachments: attachments ?? this.attachments,
        idShip: idShip ?? this.idShip,
        officer: officer ?? this.officer,
        element: element ?? this.element);
  }
}
