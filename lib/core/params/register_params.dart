class RegisterParams {
  String? username;
  String? email;
  String? name;
  String? password;
  String? passwordConfirmation;
  String? phone;
  String? identityNo;
  String? address;
  int? idGender;
  RegisterParams(
      {this.username,
      this.email,
      this.name,
      this.passwordConfirmation,
      this.idGender,
      this.password,
      this.phone,
      this.identityNo,
      this.address});

  bool get isAlreadyFilled {
    return this.username != null ||
        this.email != null ||
        this.name != null ||
        this.password != null ||
        this.phone != null ||
        this.passwordConfirmation != null ||
        this.idGender != null ||
        this.identityNo != null ||
        this.address != null;
  }

  RegisterParams copyWith(
      {String? username,
      String? email,
      String? name,
      String? password,
      String? noHp,
      String? identityNo,
      String? address,
      String? passwordConfirmation,
      int? idGender}) {
    return RegisterParams(
        username: username ?? this.username,
        email: email ?? this.email,
        name: name ?? this.name,
        password: password ?? this.password,
        passwordConfirmation: passwordConfirmation ?? this.passwordConfirmation,
        phone: noHp ?? this.phone,
        identityNo: identityNo ?? this.identityNo,
        address: address ?? this.address,
        idGender: idGender ?? this.idGender);
  }

  Map<String, dynamic> toJson() {
    return {
      'username': username,
      'email': email,
      'name': name,
      'password': password,
      'password_confirmation': passwordConfirmation,
      'phone': phone,
      'identity_num': identityNo,
      'address': address,
      'id_gender': idGender
    };
  }

  factory RegisterParams.fromJson(Map<String, dynamic> map) {
    return RegisterParams(
      username: map['username'],
      email: map['email'],
      name: map['name'],
      password: map['password'],
      phone: map['phone'],
      identityNo: map['identityNo'],
      address: map['address'],
    );
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is RegisterParams &&
        other.username == username &&
        other.email == email &&
        other.name == name &&
        other.password == password &&
        other.phone == phone &&
        other.identityNo == identityNo &&
        other.passwordConfirmation == passwordConfirmation &&
        other.idGender == idGender &&
        other.address == address;
  }

  @override
  int get hashCode {
    return username.hashCode ^
        email.hashCode ^
        name.hashCode ^
        password.hashCode ^
        phone.hashCode ^
        identityNo.hashCode ^
        passwordConfirmation.hashCode ^
        idGender.hashCode ^
        address.hashCode;
  }

  @override
  String toString() {
    return 'RegisterParams(username: $username, email: $email, name: $name, password: $password, passwordConfirmation: $passwordConfirmation, phone: $phone, identityNo: $identityNo, address: $address, idGender: $idGender)';
  }
}
