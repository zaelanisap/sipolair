class LoginParams {
  String email;
  String password;
  LoginParams({
    required this.email,
    required this.password,
  });

  Map<String, dynamic> toJson() {
    return {
      'email': email,
      'password': password,
    };
  }

  factory LoginParams.fromJson(Map<String, dynamic> map) {
    return LoginParams(
      email: map['email'],
      password: map['password'],
    );
  }

  @override
  String toString() => 'LoginParams(email: $email, password: $password)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is LoginParams &&
        other.email == email &&
        other.password == password;
  }

  @override
  int get hashCode => email.hashCode ^ password.hashCode;
}
