import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ColorUtil {
  static const Color darkBlue = Color(0xFF0C7AB0);
  static const Color blue = Color(0xFF229BD8);
  static const Color lightGrey = Color(0xFFF5F6FA);
  static const Color tosca = Color(0xFF1DC6D1);
  static const Color darkGreen = Color(0xFF219653);
  static const Color green = Color(0xFF16A085);
  static const Color amber = Color(0xFFF39C12);
  static const Color lightGrey2 = Color(0xFFF6F8FA);
  static const Color orange = Color(0xFFF2994A);
  static const Color lightOrange = Color(0xFFFFEEC1);
}

class DateHelper {
  static String formatDateTime(DateTime dateTime, [String? pattern]) {
    DateFormat dateFormat = DateFormat(pattern ?? 'dd MMMM yyyy');
    return dateFormat.format(dateTime);
  }

  static String formatDateTimeFromString(String dateTime, [String? pattern]) {
    DateTime? time = DateTime.tryParse(dateTime);
    if (time == null) return '-';
    DateFormat dateFormat = DateFormat(pattern ?? 'dd MMMM yyyy');
    return dateFormat.format(time.toLocal());
  }

  static String mappingDateTime(String value) {
    var now = DateTime.now();
    var date = DateTime.parse(value).toLocal();

    var diff = DateTime(now.year, now.month, now.day)
        .difference(DateTime(date.year, date.month, date.day));

    if (diff.inDays == 0) {
      return 'Hari ini';
    } else if (diff.inDays == 1) {
      return 'Kemarin';
    }
    return formatDateTimeFromString(value, 'dd-MM-yyyy');
  }
}
