import 'package:animations/animations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sipolair/core/utils/pref_utils.dart';
import 'package:sipolair/routes/routes.dart';
import 'package:sipolair/screen/helpers/util.dart';
import 'package:sipolair/screen/widgets/custom_button.dart';

class ModalManager {
  static void showLoadingIndicator(BuildContext context) {
    showModal(
      context: context,
      builder: (context) {
        return Center(
          child: Container(
            height: 72,
            width: 72,
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(8)),
            padding: EdgeInsets.all(16),
            child: Center(
              child: CircularProgressIndicator(),
            ),
          ),
        );
      },
    );
  }

  static void showUnauthorizedSheet(BuildContext context) {
    showBottomSheet(
      context,
      assetImage: 'assets/images/illustrator1.png',
      labelButton: 'Keluar',
      title: 'User tidak dikenal',
      content:
          'Silahkan login kembali untuk dapat kembali menggunakan aplikasi',
      onPressed: () {
        PrefUtil.instance.clearLogin();
        Navigator.of(context)
            .pushNamedAndRemoveUntil(Routes.LOGINPAGE, (route) => false);
      },
    );
  }

  static Future<T?> showBottomSheet<T>(BuildContext context,
      {String? title,
      String? content,
      ImageProvider? image,
      String? assetImage,
      String? labelButton,
      Widget? secondButton,
      VoidCallback? onClosed,
      VoidCallback? onPressed}) async {
    assert(image != null || assetImage != null);
    return await showModalBottomSheet<T>(
      isDismissible: false,
      context: context,
      backgroundColor: Colors.white,
      enableDrag: false,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(8),
        ),
      ),
      builder: (context) => _BodyBottomSheet(
        title: title,
        content: content,
        image: image ?? AssetImage(assetImage!),
        labelButton: labelButton,
        onPressed: onPressed,
        onClosed: onClosed,
        secondButton: secondButton,
      ),
    );
  }
}

class _BodyBottomSheet extends StatelessWidget {
  final String? title;
  final String? content;
  final ImageProvider image;
  final String? labelButton;
  final VoidCallback? onPressed;
  final VoidCallback? onClosed;
  final Widget? secondButton;

  _BodyBottomSheet(
      {Key? key,
      this.title,
      this.content,
      required this.image,
      this.labelButton,
      this.secondButton,
      this.onClosed,
      this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1),
      child: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                AspectRatio(aspectRatio: 1.3, child: Image(image: image)),
                SizedBox(
                  height: 24,
                ),
                Text(
                  title ?? 'Judul',
                  maxLines: 1,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  content ?? 'Isi konten',
                  style: TextStyle(),
                ),
                SizedBox(
                  height: 24,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                        child: buildButton(
                            isOutlinedButton: secondButton != null)),
                    if (secondButton != null) SizedBox(width: 16),
                    if (secondButton != null) Expanded(child: secondButton!),
                  ],
                )
              ],
            ),
          ),
          Positioned(
            top: 8,
            right: 8,
            child: CustomButton(
                child: Icon(Icons.close),
                onTap: () {
                  onClosed?.call();
                  Navigator.of(context).pop();
                }),
          )
        ],
      ),
    );
  }

  Widget buildButton({bool isOutlinedButton = false}) {
    return isOutlinedButton
        ? OutlinedButton(
            style: OutlinedButton.styleFrom(
                primary: Colors.white,
                padding: EdgeInsets.all(12),
                side: BorderSide(color: ColorUtil.darkGreen, width: 1),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(6))),
            child: Text(
              labelButton ?? 'OK',
              style: TextStyle(fontSize: 16, color: ColorUtil.green),
            ),
            onPressed: onPressed ?? () {})
        : ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: ColorUtil.darkGreen,
                padding: EdgeInsets.all(12),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(6))),
            child: Text(
              labelButton ?? 'OK',
              style: TextStyle(fontSize: 16),
            ),
            onPressed: onPressed ?? () {});
  }
}
