import 'package:flutter/material.dart';

class SnackBarManager {
  static void showSnackBar(
      {required BuildContext context,
      required String message,
      Color? backgroundColor,
      Duration? duration,
      TextStyle? textStyle,
      Widget? leading,
      SnackBarAction? action}) {
    ScaffoldMessenger.of(context).showSnackBar(_snackBar(
        message: message,
        action: action,
        backgroundColor: backgroundColor,
        duration: duration,
        textStyle: textStyle,
        leading: leading));
  }

  static SnackBar _snackBar(
      {required String message,
      Duration? duration,
      Widget? leading,
      TextStyle? textStyle,
      Color? backgroundColor,
      SnackBarAction? action}) {
    return SnackBar(
      duration: duration ?? Duration(milliseconds: 1500),
      action: action,
      content: Row(
        children: [
          if (leading != null)
            Padding(padding: EdgeInsets.only(right: 16), child: leading),
          Expanded(
            child: Text(
              message,
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
              style: textStyle,
            ),
          ),
        ],
      ),
      backgroundColor: backgroundColor ?? Color(0xFF16A085),
      margin: EdgeInsets.fromLTRB(16, 0, 16, 16),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      behavior: SnackBarBehavior.floating,
    );
  }
}
