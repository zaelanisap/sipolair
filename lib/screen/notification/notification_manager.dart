import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:sipolair/routes/route_generator.dart';

class NotificationManager {
  final FlutterLocalNotificationsPlugin localNotificationsPlugin;
  final AndroidNotificationChannel channel;

  NotificationManager(
      {required this.localNotificationsPlugin, required this.channel});

  static Future<void> initializing() async {
    FlutterLocalNotificationsPlugin localNotificationsPlugin = Get.find();

    AndroidInitializationSettings androidInitializationSettings =
        AndroidInitializationSettings('app_icon');

    IOSInitializationSettings iosInitializationSettings =
        IOSInitializationSettings(
            requestAlertPermission: false,
            requestBadgePermission: false,
            requestSoundPermission: false,
            onDidReceiveLocalNotification: onDidReceiveLocalNotification);

    InitializationSettings initializationSettings = InitializationSettings(
        android: androidInitializationSettings, iOS: iosInitializationSettings);

    await localNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (String? payload) async {});
  }

  Future<void> createNotificationChannel() async {
    await localNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);
  }

  Future<void> notification(int id, String? title, String? body,
      {String? payload}) async {
    String _title = title ?? 'Title Notification';
    String _body = body ?? 'Body Notification';
    FlutterLocalNotificationsPlugin localNotificationsPlugin =
        Get.find<FlutterLocalNotificationsPlugin>();

    AndroidNotificationDetails androidNotificationDetails =
        AndroidNotificationDetails(
            channel.id, channel.name, channel.description,
            priority: Priority.high,
            importance: Importance.max,
            enableVibration: true,
            largeIcon: DrawableResourceAndroidBitmap('@mipmap/ic_launcher'),
            playSound: true,
            enableLights: true,
            ledOnMs: 1000,
            ledOffMs: 300,
            ledColor: Colors.green,
            ticker: 'HRKu');

    IOSNotificationDetails iosNotificationDetails = IOSNotificationDetails();

    NotificationDetails notificationDetails = NotificationDetails(
        android: androidNotificationDetails, iOS: iosNotificationDetails);

    await localNotificationsPlugin.show(id, _title, _body, notificationDetails,
        payload: payload);
  }

  static Future<void> onDidReceiveLocalNotification(
      int id, String? title, String? body, String? payload) async {
    showDialog(
      context: RouteGenerator.navigatorKey.currentState!.overlay!.context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text(title ?? 'Title Notification'),
          content: Text(body ?? 'Body Notification'),
          actions: <Widget>[
            CupertinoDialogAction(
              isDefaultAction: true,
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text("Okay"),
            ),
          ],
        );
      },
    );
  }
}
