import 'package:flutter/material.dart';
import 'package:sipolair/screen/helpers/util.dart';

import 'form_text_field.dart';

typedef DateTimeCallback = void Function(DateTime dateTime);

class DatePicker extends StatelessWidget {
  DatePicker(
      {Key? key,
      required this.controller,
      this.readOnly = false,
      this.onDateChanged})
      : super(key: key);
  final TextEditingController controller;
  final bool readOnly;
  final DateTimeCallback? onDateChanged;

  final now = DateTime.now();
  @override
  Widget build(BuildContext context) {
    return FormTextField(
      controller: controller,
      label: 'Tanggal',
      hintText: 'Pilih tanggal',
      suffixIcon: Icon(Icons.date_range),
      isDense: true,
      readOnly: true,
      validator: (value) =>
          value?.isEmpty ?? false ? 'Tanggal tidak boleh kosong' : null,
      onTap: readOnly
          ? null
          : () async {
              var date = await showDatePicker(
                  context: context,
                  initialDate: now,
                  firstDate: DateTime(now.year - 20),
                  lastDate: now,
                  helpText: 'Tanggal');

              if (date != null) {
                onDateChanged?.call(date);
                controller.text = DateHelper.formatDateTime(date);
              }
            },
    );
  }
}
