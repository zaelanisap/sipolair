import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:sipolair/core/utils/pref_utils.dart';
import 'package:sipolair/core/utils/strings.dart';
import 'package:sipolair/cubit/authentication/cubit.dart';
import 'package:sipolair/repositories/authentication_repository.dart';
import 'package:sipolair/routes/routes.dart';
import 'package:sipolair/screen/helpers/modal_manager.dart';
import 'package:sipolair/screen/helpers/snackbar_manager.dart';
import 'package:sipolair/screen/helpers/util.dart';

class HomeAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: ColorUtil.darkBlue,
      title: Text('SIPPOLAIR'),
      actions: [
        BlocProvider(
          create: (_) =>
              AuthenticationCubit(Get.find<AuthenticationRepository>()),
          child: Builder(
            builder: (context) =>
                BlocListener<AuthenticationCubit, AuthenticationState>(
              listener: (context, state) {
                if (state is AuthenticationLoading) {
                  ModalManager.showLoadingIndicator(context);
                } else if (state is AuthenticationFailure) {
                  Navigator.of(context).pop();
                  SnackBarManager.showSnackBar(
                      context: context,
                      message: state.message,
                      backgroundColor: Colors.red);
                } else if (state is LogoutSuccess) {
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      Routes.LOGINPAGE, (route) => false);
                }
              },
              child: PopupMenuButton(
                padding: EdgeInsets.zero,
                iconSize: 20,
                child: Container(
                  margin: EdgeInsets.only(right: 8),
                  height: 36,
                  width: 36,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                    border: Border.all(color: Colors.white, width: 0.4),
                    image: DecorationImage(
                      fit: BoxFit.contain,
                      image: NetworkImage(
                          PrefUtil.instance.avatar ?? Strings.dummyAvatar),
                    ),
                  ),
                ),
                onSelected: (value) async {
                  switch (value) {
                    case 1:
                      Navigator.of(context).pushNamed(Routes.PROFILEPAGE);
                      break;
                    case 2:
                      context.read<AuthenticationCubit>().logout();
                      break;
                  }
                },
                itemBuilder: (context) {
                  return [
                    PopupMenuItem(
                      child: Text('Lihat Profil'),
                      value: 1,
                      textStyle: TextStyle(color: Colors.black),
                    ),
                    PopupMenuItem(
                      child: Text('Logout'),
                      value: 2,
                      textStyle: TextStyle(color: Colors.black),
                    ),
                  ];
                },
              ),
            ),
          ),
        ),
      ],
    );
  }
}
