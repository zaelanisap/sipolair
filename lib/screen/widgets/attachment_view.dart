import 'package:flutter/material.dart';

import 'package:sipolair/core/utils/strings.dart';
import 'package:sipolair/models/report_detail.dart';
import 'package:sipolair/routes/routes.dart';

class AttachmentView extends StatelessWidget {
  const AttachmentView({
    Key? key,
    required this.files,
  }) : super(key: key);
  final List<FileReport> files;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Foto/Video/Dokumen',
          style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
        ),
        SizedBox(height: 12),
        if (files.isNotEmpty)
          Wrap(
            spacing: 16,
            runSpacing: 16,
            children: List.generate(
              files.length,
              (index) {
                var file = files[index];

                return InputChip(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8)),
                  avatar: Icon(
                    Icons.attach_file,
                    size: 20,
                  ),
                  label: Text(file.fileName),
                  onPressed: () {
                    String url = Strings.BASE_URL +
                        'public/' +
                        file.path +
                        file.fileName;
                    Navigator.of(context)
                        .pushNamed(Routes.APPWEBVIEW, arguments: url);
                  },
                );
              },
            ),
          )
        else
          Text('Tidak ada file')
      ],
    );
  }
}
