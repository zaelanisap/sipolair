import 'package:flutter/material.dart';
import 'package:sipolair/datasources/device_source.dart';
import 'package:sipolair/screen/helpers/util.dart';

class AttachmentPicker extends StatefulWidget {
  final void Function(List<String?> paths) onFileSelected;
  const AttachmentPicker({Key? key, required this.onFileSelected})
      : super(key: key);

  @override
  _AttachmentPickerState createState() => _AttachmentPickerState();
}

class _AttachmentPickerState extends State<AttachmentPicker> {
  List<String?> files = [];

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (files.isNotEmpty)
          Padding(
            padding: const EdgeInsets.only(top: 8, bottom: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '${files.length} terpilih',
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
                ),
                Wrap(
                  children: List.generate(
                    files.length,
                    (index) => InputChip(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)),
                      avatar: Icon(
                        Icons.attach_file,
                        size: 20,
                      ),
                      label: Text(files[index]?.split('/').last ?? ''),
                      onDeleted: () {
                        setState(() {
                          files.removeAt(index);
                          widget.onFileSelected.call(files);
                        });
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        Container(
          padding: EdgeInsets.fromLTRB(24, 16, 24, 16),
          decoration: BoxDecoration(
            color: ColorUtil.lightGrey,
            borderRadius: BorderRadius.circular(8),
          ),
          child: InkWell(
            onTap: () async {
              var picker = await DeviceSource().getMultipleFile();

              files.addAll(picker);
              widget.onFileSelected.call(files);
              setState(() {});
            },
            child: Row(
              children: [
                Icon(
                  Icons.upload_file,
                  color: ColorUtil.darkBlue,
                ),
                SizedBox(width: 16),
                Text(
                  'Unggah Berkas',
                  style: TextStyle(
                      color: ColorUtil.darkBlue, fontWeight: FontWeight.w500),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
