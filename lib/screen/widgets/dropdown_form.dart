import 'package:dropdown_sheet/dropdown_sheet.dart';
import 'package:flutter/material.dart';

class FormDropDown<T> extends StatelessWidget {
  final String label;
  final String hintText;
  final bool enabled = true;
  final List<SimpleDialogItem<T>> items;
  final T? selectedItem;
  final Widget? prefixIcon;
  final void Function(T value) onSelected;
  final String? Function(String?)? validator;
  final Color? backgroundColor;
  const FormDropDown(
      {Key? key,
      required this.label,
      required this.hintText,
      required this.items,
      required this.selectedItem,
      required this.onSelected,
      this.validator,
      this.backgroundColor,
      this.prefixIcon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
        ),
        SizedBox(height: 4),
        DropdownSheet<T>(
          backgroundColor: backgroundColor,
          title: label,
          prefixIcon: prefixIcon,
          validator: validator,
          hintText: hintText,
          items: items,
          onSelected: onSelected,
          selectedItem: selectedItem,
          isDense: true,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: BorderSide(color: Colors.transparent),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: BorderSide(color: Colors.transparent),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: BorderSide(color: Colors.blue),
          ),
        ),
      ],
    );
  }
}
