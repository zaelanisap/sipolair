import 'package:flutter/material.dart';
import 'package:sipolair/screen/helpers/util.dart';

import 'custom_button.dart';

typedef OnDateRangeSelected = void Function(DateTimeRange? timeRange);

class FilterDatePicker extends StatefulWidget {
  FilterDatePicker({
    Key? key,
    required this.onSelected,
  }) : super(key: key);

  final OnDateRangeSelected onSelected;

  @override
  _FilterDatePickerState createState() => _FilterDatePickerState();
}

class _FilterDatePickerState extends State<FilterDatePicker> {
  DateTimeRange? timeRange;

  String dateRange(DateTime start, DateTime end) {
    String from = DateHelper.formatDateTime(start, 'dd/MM/yyy');
    String until = DateHelper.formatDateTime(end, 'dd/MM/yyy');
    return '$from - $until';
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Colors.grey, width: 0.4),
              borderRadius: BorderRadius.circular(4)),
          child: InkWell(
            onTap: () async {
              var now = DateTime.now();
              timeRange = await showDateRangePicker(
                context: context,
                firstDate: DateTime(now.year - 1),
                initialDateRange: DateTimeRange(start: now, end: now),
                lastDate: now,
              );
              setState(() {});
              widget.onSelected(timeRange);
            },
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(
                  Icons.date_range_outlined,
                  size: 20,
                  color: Colors.black54,
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  timeRange != null
                      ? dateRange(timeRange!.start, timeRange!.end)
                      : 'Pilih Tanggal',
                  textScaleFactor: 1,
                  style: TextStyle(fontSize: timeRange != null ? 10 : 12),
                )
              ],
            ),
          ),
        ),
        if (timeRange != null)
          CustomButton(
              child: Icon(
                Icons.close,
              ),
              onTap: () {
                setState(() {
                  timeRange = null;
                });
                widget.onSelected(timeRange);
              })
      ],
    );
  }
}
