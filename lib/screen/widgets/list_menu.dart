import 'package:flutter/material.dart';
import 'package:sipolair/screen/helpers/util.dart';

typedef IntCallback = void Function(int value);

class ListMenu extends StatefulWidget {
  const ListMenu({
    Key? key,
    required this.onMenuChanged,
  }) : super(key: key);
  final IntCallback onMenuChanged;

  @override
  _ListMenuState createState() => _ListMenuState();
}

class _ListMenuState extends State<ListMenu> {
  final List<GiatRutin> listMenu = [
    GiatRutin(id: 1, name: 'Bimas'),
    GiatRutin(id: 2, name: 'Patroli'),
    GiatRutin(id: 3, name: 'Penegakkan Hukum'),
  ];

  int id = 1;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      child: ListView.separated(
        padding: EdgeInsets.symmetric(horizontal: 8),
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          final menu = listMenu[index];
          return ChoiceChip(
            selected: menu.id == id,
            labelPadding: EdgeInsets.symmetric(horizontal: 16),
            elevation: 0,
            side: BorderSide(
                color: menu.id == id ? Colors.blue : Colors.black54,
                width: 0.4),
            selectedColor: Colors.blue,
            backgroundColor: Colors.white,
            pressElevation: 0,
            label: Text(
              menu.name,
              style: TextStyle(
                  color: menu.id == id ? Colors.white : ColorUtil.darkBlue),
            ),
            onSelected: (_) {
              setState(() => id = menu.id);
              widget.onMenuChanged.call(menu.id);
            },
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          );
        },
        separatorBuilder: (context, index) => SizedBox(width: 12),
        itemCount: listMenu.length,
      ),
    );
  }
}

class GiatRutin {
  final int id;
  final String name;
  GiatRutin({
    required this.id,
    required this.name,
  });
}
