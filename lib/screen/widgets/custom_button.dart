import 'package:flutter/material.dart';

class CustomButton extends StatefulWidget {
  final Widget child;
  final EdgeInsetsGeometry? padding;
  final VoidCallback? onTap;
  final HitTestBehavior? behavior;

  const CustomButton(
      {Key? key,
      required this.onTap,
      required this.child,
      this.padding,
      this.behavior})
      : super(key: key);

  @override
  _CustomButtonState createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  double? kOpacity;
  double? _opacity;

  @override
  void initState() {
    super.initState();
    kOpacity = widget.onTap != null ? 1 : 0.4;
    if (mounted) {
      _opacity = kOpacity;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: widget.behavior,
      onTap: widget.onTap,
      onTapDown: (details) {
        setState(() {
          _opacity = 0.4;
        });
      },
      onTapUp: (details) {
        setState(() {
          _opacity = kOpacity;
        });
      },
      onTapCancel: () {
        setState(() {
          _opacity = kOpacity;
        });
      },
      child: Padding(
        padding: widget.padding ?? EdgeInsets.all(4.0),
        child: AnimatedOpacity(
            duration: Duration(milliseconds: 150),
            opacity: _opacity ?? 1,
            child: widget.child),
      ),
    );
  }
}
