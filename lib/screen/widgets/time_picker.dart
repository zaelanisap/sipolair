import 'package:flutter/material.dart';

import 'form_text_field.dart';

typedef TimeCallback = void Function(TimeOfDay timeOfDay);

class TimePicker extends StatelessWidget {
  const TimePicker(
      {Key? key,
      required this.controller,
      this.readOnly = false,
      this.onTimeChanged,
      this.label})
      : super(key: key);
  final String? label;
  final TextEditingController controller;
  final bool readOnly;
  final TimeCallback? onTimeChanged;

  @override
  Widget build(BuildContext context) {
    return FormTextField(
      controller: controller,
      label: label ?? 'Waktu',
      hintText: '--:--',
      suffixIcon: Icon(Icons.access_time),
      validator: (value) =>
          value?.isEmpty ?? false ? 'Waktu tidak boleh kosong' : null,
      isDense: true,
      readOnly: true,
      onTap: readOnly
          ? null
          : () async {
              var time = await showTimePicker(
                  context: context,
                  initialTime: TimeOfDay.now(),
                  helpText: 'Waktu');

              if (time != null) {
                onTimeChanged?.call(time);
                controller.text = time.format(context);
              }
            },
    );
  }
}
