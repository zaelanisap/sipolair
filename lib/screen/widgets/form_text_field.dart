import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sipolair/screen/helpers/util.dart';

class FormTextField extends StatelessWidget {
  final TextEditingController? controller;
  final String? initialValue;
  final String label;
  final String? hintText;
  final String? helperText;
  final bool isObscure;
  final bool isDense;
  final bool readOnly;
  final int? maxLength;
  final int? minLines;
  final int? maxLines;
  final InputBorder? enabledBorder;
  final TextInputAction? inputAction;
  final TextInputType? inputType;
  final FocusNode? focusNode;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final VoidCallback? onTap;
  final void Function(String value)? onFieldSubmitted;
  final void Function(String value)? onChanged;
  final String? Function(String? value)? validator;
  final TextCapitalization textCapitalization;

  const FormTextField(
      {Key? key,
      required this.label,
      this.controller,
      this.initialValue,
      this.hintText,
      this.helperText,
      this.maxLength,
      this.maxLines = 1,
      this.minLines,
      this.isDense = false,
      this.isObscure = false,
      this.readOnly = false,
      this.inputAction,
      this.inputType,
      this.textCapitalization = TextCapitalization.sentences,
      this.focusNode,
      this.prefixIcon,
      this.suffixIcon,
      this.enabledBorder,
      this.onTap,
      this.onChanged,
      this.onFieldSubmitted,
      this.validator})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
        ),
        SizedBox(height: 4),
        TextFormField(
          controller: controller,
          initialValue: initialValue,
          keyboardType: inputType,
          textInputAction: inputAction,
          textCapitalization: textCapitalization,
          autofocus: false,
          readOnly: readOnly,
          obscureText: isObscure,
          maxLines: maxLines,
          minLines: minLines,
          validator: validator,
          onChanged: onChanged,
          onFieldSubmitted: onFieldSubmitted,
          onTap: onTap,
          inputFormatters: [LengthLimitingTextInputFormatter(maxLength)],
          decoration: InputDecoration(
            helperText: helperText,
            prefixIcon: prefixIcon,
            suffixIcon: suffixIcon,
            hintText: hintText ?? '',
            hintStyle: TextStyle(fontSize: 14),
            // contentPadding: EdgeInsets.fromLTRB(25.0, 20.0, 0, 16.0),
            isDense: isDense,
            filled: true,
            fillColor: ColorUtil.lightGrey,
            enabled: true,
            border: OutlineInputBorder(
                borderSide: BorderSide(
                  width: 0,
                  color: Colors.transparent,
                ),
                borderRadius: BorderRadius.circular(8)),
            enabledBorder: enabledBorder ??
                OutlineInputBorder(
                    borderSide: BorderSide(
                      width: 0,
                      color: Colors.transparent,
                    ),
                    borderRadius: BorderRadius.circular(8)),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(color: ColorUtil.blue, width: 0.8)),
          ),
        ),
      ],
    );
  }
}

class CustomTextField extends StatelessWidget {
  final TextEditingController? controller;
  final String? initialValue;
  final String? hintText;
  final String? helperText;
  final bool isObscure;
  final bool isDense;
  final bool readOnly;
  final int? maxLength;
  final int? minLines;
  final int? maxLines;
  final TextInputAction? inputAction;
  final TextInputType? inputType;
  final FocusNode? focusNode;
  final Color? backgroundColor;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final VoidCallback? onTap;
  final void Function(String value)? onFieldSubmitted;
  final void Function(String value)? onChanged;
  final String? Function(String? value)? validator;

  const CustomTextField(
      {Key? key,
      this.controller,
      this.initialValue,
      this.hintText,
      this.helperText,
      this.maxLength,
      this.maxLines = 1,
      this.minLines,
      this.isDense = false,
      this.isObscure = false,
      this.readOnly = false,
      this.inputAction,
      this.inputType,
      this.focusNode,
      this.backgroundColor,
      this.prefixIcon,
      this.suffixIcon,
      this.onTap,
      this.onChanged,
      this.onFieldSubmitted,
      this.validator})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      initialValue: initialValue,
      keyboardType: inputType,
      textInputAction: inputAction,
      autofocus: false,
      readOnly: readOnly,
      obscureText: isObscure,
      maxLines: maxLines,
      minLines: minLines,
      validator: validator,
      onChanged: onChanged,
      onFieldSubmitted: onFieldSubmitted,
      onTap: onTap,
      inputFormatters: [LengthLimitingTextInputFormatter(maxLength)],
      decoration: InputDecoration(
        helperText: helperText,
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon,
        hintText: hintText ?? '',
        hintStyle: TextStyle(fontSize: 14),
        // contentPadding: EdgeInsets.fromLTRB(25.0, 20.0, 0, 16.0),
        isDense: isDense,
        filled: true,
        fillColor: backgroundColor ?? ColorUtil.lightGrey,
        enabled: true,
        border: OutlineInputBorder(
            borderSide: BorderSide(
              width: 0,
              color: Colors.transparent,
            ),
            borderRadius: BorderRadius.circular(8)),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              width: 0,
              color: Colors.transparent,
            ),
            borderRadius: BorderRadius.circular(8)),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: BorderSide(color: ColorUtil.blue, width: 0.8)),
      ),
    );
  }
}
