import 'package:flutter/material.dart';

import 'package:sipolair/screen/helpers/util.dart';

class SubmitButton extends StatelessWidget {
  const SubmitButton({
    Key? key,
    required this.onPressed,
  }) : super(key: key);
  final VoidCallback onPressed;
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
          elevation: 0,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          padding: EdgeInsets.all(14),
          primary: ColorUtil.darkBlue),
      child: Text(
        'SUBMIT',
        style: TextStyle(fontSize: 14),
      ),
    );
  }
}
