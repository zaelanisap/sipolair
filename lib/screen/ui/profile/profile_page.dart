import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sipolair/core/utils/strings.dart';
import 'package:sipolair/cubit/profile/cubit.dart';
import 'package:sipolair/screen/helpers/snackbar_manager.dart';
import 'package:sipolair/screen/helpers/util.dart';
import 'package:sipolair/screen/widgets/form_text_field.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          'Profil',
          style: TextStyle(color: Colors.black87),
        ),
        iconTheme: IconThemeData(color: Colors.black87),
      ),
      body: BlocConsumer<ProfileCubit, ProfileState>(
        listener: (context, state) {
          if (state is ProfileFailure) {
            SnackBarManager.showSnackBar(
                context: context,
                message: state.message,
                backgroundColor: Colors.red);
          }
        },
        buildWhen: (previous, current) => current is ProfileLoaded,
        builder: (context, state) {
          if (state is ProfileLoaded) {
            var user = state.user;
            return RefreshIndicator(
              onRefresh: () async {
                context.read<ProfileCubit>().getProfile();
              },
              child: SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(),
                padding: EdgeInsets.fromLTRB(16, 24, 16, 24),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        height: 80,
                        width: 80,
                        clipBehavior: Clip.antiAlias,
                        decoration: BoxDecoration(
                            color: ColorUtil.lightGrey,
                            borderRadius: BorderRadius.circular(8)),
                        child: Image.network(
                          user.account?.avatar ??
                              user.avatar ??
                              Strings.dummyAvatar,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    SizedBox(height: 16),
                    FormTextField(
                      label: 'Nama Lengkap',
                      initialValue: user.account?.name,
                      readOnly: true,
                      isDense: true,
                      hintText: '',
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 0.6),
                        borderRadius: BorderRadius.circular(8),
                      ),
                    ),
                    SizedBox(height: 16),
                    FormTextField(
                      label: 'Email',
                      initialValue: user.email,
                      readOnly: true,
                      isDense: true,
                      hintText: '',
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 0.6),
                        borderRadius: BorderRadius.circular(8),
                      ),
                    ),
                    SizedBox(height: 16),
                    FormTextField(
                      label: 'No. Identitas',
                      initialValue: user.account?.identityNum,
                      readOnly: true,
                      isDense: true,
                      hintText: '',
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 0.6),
                        borderRadius: BorderRadius.circular(8),
                      ),
                    ),
                    SizedBox(height: 16),
                    FormTextField(
                      label: 'No. Handphone',
                      initialValue: user.account?.phone,
                      readOnly: true,
                      isDense: true,
                      hintText: '',
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 0.6),
                        borderRadius: BorderRadius.circular(8),
                      ),
                    ),
                    SizedBox(height: 16),
                    FormTextField(
                      label: 'Alamat',
                      initialValue: user.account?.address,
                      readOnly: true,
                      isDense: true,
                      hintText: '',
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 0.6),
                        borderRadius: BorderRadius.circular(8),
                      ),
                    ),
                    SizedBox(height: 16),
                    FormTextField(
                      label: 'Jenis Kelamin',
                      initialValue: user.account?.gender.gender,
                      readOnly: true,
                      isDense: true,
                      hintText: '',
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 0.6),
                        borderRadius: BorderRadius.circular(8),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
