import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:sipolair/core/utils/strings.dart';
import 'package:sipolair/cubit/authentication/cubit.dart';
import 'package:sipolair/repositories/authentication_repository.dart';
import 'package:sipolair/routes/routes.dart';
import 'package:sipolair/screen/helpers/modal_manager.dart';
import 'package:sipolair/screen/helpers/snackbar_manager.dart';
import 'package:sipolair/screen/helpers/util.dart';
import 'package:sipolair/screen/widgets/form_text_field.dart';
import 'package:sipolair/screen/widgets/hide_focus.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  FocusNode emailFocus = FocusNode();
  FocusNode passFocus = FocusNode();

  bool isValidate = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return HideFocus(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.white,
        body: Container(
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: [
              Spacer(),
              Expanded(
                flex: 5,
                child: SingleChildScrollView(
                  primary: false,
                  child: Form(
                    key: _formKey,
                    autovalidateMode: isValidate
                        ? AutovalidateMode.always
                        : AutovalidateMode.disabled,
                    child: Padding(
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Align(
                            alignment: Alignment.center,
                            child: Image.asset(Strings.logoApp, width: 100),
                          ),
                          SizedBox(height: 48),
                          Container(
                            child: Text(
                              'Selamat Datang',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.w500),
                            ),
                          ),
                          SizedBox(height: 4),
                          Container(
                            child: Text(
                              'Masuk dengan email Anda',
                              style: TextStyle(
                                  fontSize: 12, color: Colors.black54),
                            ),
                          ),
                          SizedBox(height: 32),
                          CustomTextField(
                            controller: emailController,
                            focusNode: emailFocus,
                            hintText: 'Email',
                            inputType: TextInputType.emailAddress,
                            inputAction: TextInputAction.next,
                            validator: (value) {
                              if (value?.isEmpty ?? false) {
                                return 'Mohon masukkan email Anda.';
                              } else if (!(value?.isEmail ?? true)) {
                                return 'Mohon masukkan Email yang valid';
                              }
                              return null;
                            },
                          ),
                          SizedBox(height: 10.0),
                          CustomTextField(
                            controller: passwordController,
                            focusNode: passFocus,
                            isObscure: true,
                            maxLines: 1,
                            hintText: 'Password',
                            inputAction: TextInputAction.done,
                            validator: (value) => value?.isEmpty ?? false
                                ? 'Mohon masukkan password Anda.'
                                : null,
                          ),
                          SizedBox(height: 32.0),
                          buildLoginButton(),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                              margin: EdgeInsets.only(top: 32, bottom: 24),
                              child: Text.rich(
                                TextSpan(
                                  text: "Belum Punya akun? ",
                                  children: [
                                    TextSpan(
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () => Navigator.of(context)
                                            .pushReplacementNamed(
                                                Routes.REGISTERPAGE),
                                      text: 'Daftar',
                                      style: TextStyle(
                                          color: ColorUtil.darkBlue,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ],
                                ),
                                style: TextStyle(
                                  fontSize: 12,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildLoginButton() {
    return BlocProvider(
      create: (_) => AuthenticationCubit(Get.find<AuthenticationRepository>()),
      child: Builder(
        builder: (context) => Container(
          margin: EdgeInsets.only(top: 24),
          width: double.infinity,
          child: BlocListener<AuthenticationCubit, AuthenticationState>(
            listener: (context, state) {
              if (state is AuthenticationLoading) {
                ModalManager.showLoadingIndicator(context);
              } else if (state is AuthenticationFailure) {
                Navigator.of(context).pop();
                SnackBarManager.showSnackBar(
                    context: context,
                    message: state.message,
                    backgroundColor: Colors.black87);
              } else if (state is LoginSuccess) {
                Navigator.of(context)
                    .pushNamedAndRemoveUntil(Routes.HOMEPAGE, (route) => false);
                SnackBarManager.showSnackBar(
                    context: context,
                    message: 'Berhasil login',
                    backgroundColor: ColorUtil.green);
              }
            },
            child: ElevatedButton(
              onPressed: () {
                if (_formKey.currentState?.validate() ?? false) {
                  context.read<AuthenticationCubit>().login(
                      email: emailController.text,
                      password: passwordController.text);
                }

                setState(() {
                  isValidate = true;
                });
              },
              child: Text('Login'),
              style: ElevatedButton.styleFrom(
                primary: ColorUtil.darkBlue,
                padding: EdgeInsets.all(16),
                textStyle:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.w500),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
