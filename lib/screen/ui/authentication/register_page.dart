import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sipolair/core/params/register_params.dart';
import 'package:sipolair/routes/routes.dart';
import 'package:sipolair/screen/helpers/util.dart';
import 'package:sipolair/screen/widgets/custom_button.dart';
import 'package:sipolair/screen/widgets/form_text_field.dart';
import 'package:sipolair/screen/widgets/hide_focus.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late TextEditingController _unameController;
  late TextEditingController _nameController;
  late TextEditingController _emailController;
  late TextEditingController _passController;
  late TextEditingController _passConfirmController;

  bool _isValidate = false;
  bool _isObscure = true;

  @override
  void initState() {
    super.initState();
    _unameController = TextEditingController();
    _nameController = TextEditingController();
    _emailController = TextEditingController();
    _passController = TextEditingController();
    _passConfirmController = TextEditingController();
  }

  @override
  void dispose() {
    _unameController.dispose();
    _nameController.dispose();
    _emailController.dispose();
    _passController.dispose();
    _passConfirmController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double paddingTop = MediaQuery.of(context).padding.top;
    return HideFocus(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(24, paddingTop + 40, 24, 24),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Text(
                  'Daftar Pengguna Baru',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                ),
              ),
              SizedBox(height: 4),
              Container(
                child: Text(
                  'Masuk dengan email Anda',
                  style: TextStyle(fontSize: 12, color: Colors.black54),
                ),
              ),
              SizedBox(height: 24),
              Form(
                key: _formKey,
                autovalidateMode: _isValidate
                    ? AutovalidateMode.always
                    : AutovalidateMode.disabled,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomTextField(
                      controller: _unameController,
                      hintText: 'Username',
                      inputAction: TextInputAction.next,
                      validator: (value) => (value?.isEmpty ?? false)
                          ? 'Mohon masukkan username Anda'
                          : null,
                    ),
                    SizedBox(height: 16),
                    CustomTextField(
                      controller: _nameController,
                      hintText: 'Nama Lengkap',
                      inputAction: TextInputAction.next,
                      validator: (value) => (value?.isEmpty ?? false)
                          ? 'Mohon masukkan nama lengkap Anda'
                          : null,
                    ),
                    SizedBox(height: 16),
                    CustomTextField(
                      controller: _emailController,
                      hintText: 'Email',
                      inputType: TextInputType.emailAddress,
                      inputAction: TextInputAction.next,
                      validator: (value) {
                        if (value?.isEmpty ?? false) {
                          return 'Mohon masukkan email Anda';
                        } else if (!(value?.isEmail ?? true)) {
                          return 'Mohon masukkan email yang valid';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 16),
                    CustomTextField(
                      controller: _passController,
                      hintText: 'Password',
                      isObscure: _isObscure,
                      maxLines: 1,
                      suffixIcon: CustomButton(
                        onTap: () {
                          setState(() {
                            _isObscure = !_isObscure;
                          });
                        },
                        child: Icon(_isObscure
                            ? Icons.visibility_outlined
                            : Icons.visibility_off_outlined),
                      ),
                      inputAction: TextInputAction.next,
                      helperText: 'Password terdiri dari 6-20 karakter',
                      validator: (value) {
                        if (value?.isEmpty ?? false) {
                          return 'Mohon masukkan password Anda';
                        } else if ((value?.length ?? 0) < 6 ||
                            (value?.length ?? 0) > 20) {
                          return 'Pastikan panjang password antara 6-20 karakter';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 16),
                    CustomTextField(
                      controller: _passConfirmController,
                      hintText: 'Konfirmasi Password',
                      isObscure: _isObscure,
                      maxLines: 1,
                      suffixIcon: CustomButton(
                        onTap: () {
                          setState(() {
                            _isObscure = !_isObscure;
                          });
                        },
                        child: Icon(_isObscure
                            ? Icons.visibility_outlined
                            : Icons.visibility_off_outlined),
                      ),
                      inputAction: TextInputAction.next,
                      validator: (value) {
                        if (value?.isEmpty ?? false) {
                          return 'Mohon masukkan password Anda';
                        } else if (value != _passController.text) {
                          return 'Password tidak sama';
                        }
                        return null;
                      },
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 24),
                      width: double.infinity,
                      child: ElevatedButton(
                        onPressed: () {
                          if (_formKey.currentState?.validate() ?? false) {
                            Navigator.of(context).pushNamed(
                              Routes.REGISTERDETAILPAGE,
                              arguments: RegisterParams(
                                  username: _unameController.text,
                                  passwordConfirmation:
                                      _passConfirmController.text,
                                  name: _nameController.text,
                                  email: _emailController.text,
                                  password: _passController.text),
                            );
                            return;
                          }
                          setState(() {
                            _isValidate = true;
                          });
                        },
                        child: Text('Daftar'),
                        style: ElevatedButton.styleFrom(
                            primary: ColorUtil.darkBlue,
                            padding: EdgeInsets.all(16),
                            textStyle: TextStyle(color: Colors.white)),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 32),
              Align(
                alignment: Alignment.bottomCenter,
                child: Text.rich(
                  TextSpan(
                    text: "Sudah punya akun? ",
                    children: [
                      TextSpan(
                        recognizer: TapGestureRecognizer()
                          ..onTap = () => Navigator.of(context)
                              .pushReplacementNamed(Routes.LOGINPAGE),
                        text: 'Masuk',
                        style: TextStyle(
                            color: ColorUtil.darkBlue,
                            fontWeight: FontWeight.w600),
                      ),
                    ],
                  ),
                  style: TextStyle(
                    fontSize: 12,
                    color: Colors.black,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
