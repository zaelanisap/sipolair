import 'package:dropdown_sheet/dropdown_sheet.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:sipolair/core/params/register_params.dart';
import 'package:sipolair/cubit/authentication/cubit.dart';
import 'package:sipolair/models/gender.dart';
import 'package:sipolair/repositories/authentication_repository.dart';
import 'package:sipolair/routes/routes.dart';
import 'package:sipolair/screen/helpers/modal_manager.dart';
import 'package:sipolair/screen/helpers/snackbar_manager.dart';
import 'package:sipolair/screen/helpers/util.dart';
import 'package:sipolair/screen/widgets/form_text_field.dart';
import 'package:sipolair/screen/widgets/hide_focus.dart';

class RegisterDetailPage extends StatefulWidget {
  final RegisterParams params;
  const RegisterDetailPage({
    Key? key,
    required this.params,
  }) : super(key: key);
  @override
  _RegisterDetailPageState createState() => _RegisterDetailPageState();
}

class _RegisterDetailPageState extends State<RegisterDetailPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  late TextEditingController _phoneController;
  late TextEditingController _noController;
  late TextEditingController _addressController;

  bool _isValidate = false;

  Gender? gender;

  List<Gender> genders = [
    Gender(idGender: 1, gender: 'Laki-laki'),
    Gender(idGender: 2, gender: 'Perempuan'),
  ];

  @override
  void initState() {
    super.initState();

    _phoneController = TextEditingController();
    _noController = TextEditingController();
    _addressController = TextEditingController();
  }

  @override
  void dispose() {
    _phoneController.dispose();
    _noController.dispose();
    _addressController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return HideFocus(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          iconTheme: IconThemeData(color: Colors.black87),
        ),
        backgroundColor: Colors.white,
        body: Container(
          height: MediaQuery.of(context).size.height -
              MediaQuery.of(context).padding.top -
              kToolbarHeight,
          child: SingleChildScrollView(
            padding: const EdgeInsets.fromLTRB(24, 0, 24, 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 48),
                  child: Text(
                    'Isi Data Pengguna Baru',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                  ),
                ),
                SizedBox(height: 24),
                Form(
                  key: _formKey,
                  autovalidateMode: _isValidate
                      ? AutovalidateMode.always
                      : AutovalidateMode.disabled,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomTextField(
                        controller: _noController,
                        hintText: 'Nomer Identitas',
                        helperText: 'KTP/SIM/PASPOR',
                        inputAction: TextInputAction.next,
                        validator: (value) => (value?.isEmpty ?? false)
                            ? 'Mohon masukkan nomer identitas Anda'
                            : null,
                      ),
                      SizedBox(height: 16),
                      CustomTextField(
                        controller: _phoneController,
                        hintText: 'No. Handphone',
                        helperText: 'Contoh: 08871227772',
                        inputType: TextInputType.number,
                        maxLength: 13,
                        inputAction: TextInputAction.done,
                        validator: (value) => (value?.isEmpty ?? false)
                            ? 'Mohon masukkan No. Handphone Anda'
                            : null,
                      ),
                      SizedBox(height: 16),
                      DropdownSheet<Gender>(
                        title: 'Jenis Kelamin',
                        hintText: 'Pilih Jenis Kelamin',
                        validator: (value) => (value?.isEmpty ?? false)
                            ? 'Jenis kelamin tidak boleh kosong'
                            : null,
                        items: genders
                            .map((e) =>
                                SimpleDialogItem(title: e.gender, value: e))
                            .toList(),
                        selectedItem: gender,
                        onSelected: (Gender value) {
                          setState(() {
                            gender = value;
                          });
                        },
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Colors.transparent),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(color: Colors.blue),
                        ),
                      ),
                      SizedBox(height: 16),
                      CustomTextField(
                        controller: _addressController,
                        hintText: 'Alamat',
                        minLines: 1,
                        maxLines: 4,
                        inputAction: TextInputAction.next,
                        validator: (value) => (value?.isEmpty ?? false)
                            ? 'Mohon masukkan alamat Anda'
                            : null,
                      ),
                      SizedBox(height: 16),
                      BlocProvider(
                        create: (_) => AuthenticationCubit(
                            Get.find<AuthenticationRepository>()),
                        child: Builder(
                          builder: (context) => BlocListener<
                              AuthenticationCubit, AuthenticationState>(
                            listener: (context, state) {
                              if (state is AuthenticationLoading) {
                                ModalManager.showLoadingIndicator(context);
                              } else if (state is AuthenticationFailure) {
                                Navigator.of(context).pop();
                                SnackBarManager.showSnackBar(
                                    context: context, message: state.message);
                              } else if (state is RegisterSuccess) {
                                Navigator.of(context).pushNamedAndRemoveUntil(
                                    Routes.LOGINPAGE, (route) => false);
                                SnackBarManager.showSnackBar(
                                    context: context,
                                    message:
                                        'Berhasil Daftar. Silahkan login untuk menggunakan aplikasi.',
                                    backgroundColor: ColorUtil.green);
                              }
                            },
                            child: Container(
                              margin: EdgeInsets.only(top: 24),
                              width: double.infinity,
                              child: ElevatedButton(
                                onPressed: () {
                                  if (_formKey.currentState?.validate() ??
                                      false) {
                                    RegisterParams params = widget.params
                                        .copyWith(
                                            idGender: gender?.idGender,
                                            identityNo: _noController.text,
                                            noHp: _phoneController.text,
                                            address: _addressController.text);
                                    if (params.isAlreadyFilled)
                                      params.toString();
                                    context
                                        .read<AuthenticationCubit>()
                                        .register(params: params);
                                    return;
                                  }
                                  setState(() {
                                    _isValidate = true;
                                  });
                                },
                                child: Text('Daftar'),
                                style: ElevatedButton.styleFrom(
                                    primary: ColorUtil.darkBlue,
                                    padding: EdgeInsets.all(16),
                                    textStyle: TextStyle(color: Colors.white)),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 32),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Text.rich(
                    TextSpan(
                      text: "Sudah punya akun? ",
                      children: [
                        TextSpan(
                          recognizer: TapGestureRecognizer()
                            ..onTap = () => Navigator.of(context)
                                .pushReplacementNamed(Routes.LOGINPAGE),
                          text: 'Masuk',
                          style: TextStyle(
                              color: ColorUtil.darkBlue,
                              fontWeight: FontWeight.w600),
                        ),
                      ],
                    ),
                    style: TextStyle(
                      fontSize: 12,
                      color: Colors.black,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
