import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:sipolair/core/utils/pref_utils.dart';
import 'package:sipolair/cubit/bimas/bimas_cubit.dart';
import 'package:sipolair/cubit/patrol/patrol_cubit.dart';
import 'package:sipolair/cubit/report/cubit.dart';
import 'package:sipolair/repositories/bimas_repository.dart';
import 'package:sipolair/repositories/patrol_repository.dart';
import 'package:sipolair/routes/routes.dart';
import 'package:sipolair/screen/helpers/util.dart';
import 'package:sipolair/screen/ui/bimas/bimas_list_page.dart';
import 'package:sipolair/screen/ui/patrol/patrol_list_page.dart';
import 'package:sipolair/screen/ui/report/report_list_page.dart';
import 'package:sipolair/screen/widgets/hide_focus.dart';
import 'package:sipolair/screen/widgets/home_appbar.dart';
import 'package:sipolair/screen/notification/notification_manager.dart';
import 'package:sipolair/screen/widgets/list_menu.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late FirebaseMessaging firebaseMessaging;
  late NotificationManager notifManager;

  int menu = 1;

  @override
  void initState() {
    super.initState();

    firebaseMessaging = Get.find<FirebaseMessaging>();
    notifManager = Get.find<NotificationManager>();

    if (PrefUtil.instance.isFCMChannelAlreadyCreated) {
      createNotificationChannel();
      PrefUtil.instance.setFCMChannel();
    }

    initFirebase();
  }

  void createNotificationChannel() async {
    if (Platform.isAndroid) {
      await notifManager.createNotificationChannel();
      print('create notification channel');
    }
  }

  static Future<void> onBackgroundMessage(RemoteMessage message) async {
    debugPrint('onBackgroundMessage: $message');
  }

  void initFirebase() async {
    firebaseMessaging.requestPermission(
        alert: true, badge: true, sound: true, provisional: false);

    if (Platform.isIOS) {
      firebaseMessaging.setForegroundNotificationPresentationOptions(
          alert: true, badge: true, sound: true);
    }

    RemoteMessage? initialMessage = await firebaseMessaging.getInitialMessage();

    if (initialMessage?.notification != null) {
      onBackgroundMessage(initialMessage!);
    }

    FirebaseMessaging.onBackgroundMessage(onBackgroundMessage);

    FirebaseMessaging.onMessage.listen((message) {
      print(
          'onMessage:\n notification: ${message.notification?.body}\n data: ${message.data}');
      context.read<ReportCubit>().getReports();
      onNotification(message);
    });
  }

  void onNotification(RemoteMessage message, {bool isForeground = false}) {
    debugPrint('getDataFcm: name: $message');

    String? title = '';
    String? body = '';

    title = message.notification?.title ?? message.data['title'];

    body = message.notification?.body ?? message.data['body'];

    notifManager.notification(0, title, body);
  }

  @override
  Widget build(BuildContext context) {
    return HideFocus(
      child: Scaffold(
        backgroundColor: ColorUtil.lightGrey,
        appBar: PreferredSize(
            child: HomeAppBar(),
            preferredSize: Size.fromHeight(kToolbarHeight)),
        floatingActionButton: floatingButton,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListMenu(
              onMenuChanged: (value) {
                setState(() {
                  menu = value;
                });
              },
            ),
            Expanded(child: body())
          ],
        ),
      ),
    );
  }

  Widget body() {
    switch (menu) {
      case 1:
        return BlocProvider(
          create: (context) =>
              BimasCubit(repository: Get.find<BimasRepository>()),
          child: BimasListPage(),
        );
      case 2:
        return BlocProvider(
          create: (context) => PatrolCubit(Get.find<PatrolRepository>()),
          child: PatrolListPage(),
        );
      case 3:
        return ReportListPage();
      default:
        return Container();
    }
  }

  Widget get floatingButton {
    return Builder(
      builder: (context) => FloatingActionButton.extended(
        onPressed: () async {
          showModalBottomSheet(
            useRootNavigator: true,
            context: context,
            clipBehavior: Clip.antiAlias,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.vertical(top: Radius.circular(8))),
            builder: (context) {
              return Container(
                padding: EdgeInsets.only(top: 16),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      'Giat Rutin',
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                    ),
                    Divider(
                      height: 32,
                      thickness: 1,
                    ),
                    ListTile(
                      title: Text('Bimbingan Masyarakat'),
                      onTap: () {
                        Navigator.of(context)
                            .pushReplacementNamed(Routes.BIMASTFORMPAGE);
                      },
                    ),
                    Divider(height: 0),
                    ListTile(
                      title: Text('Patroli'),
                      onTap: () {
                        Navigator.of(context)
                            .pushReplacementNamed(Routes.PATROLFORMPAGE);
                      },
                    ),
                    Divider(height: 0),
                    ListTile(
                      title: Text('Penegakkan Hukum'),
                      onTap: () {
                        Navigator.of(context)
                            .pushReplacementNamed(Routes.REPORTFORMPAGE);
                      },
                    ),
                  ],
                ),
              );
            },
          );
        },
        label: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(Icons.add),
            SizedBox(width: 4),
            Text('Buat Laporan'),
          ],
        ),
        tooltip: 'Buat laporan baru',
        backgroundColor: ColorUtil.darkBlue,
      ),
    );
  }
}
