import 'package:dropdown_sheet/dropdown_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sipolair/core/params/patrol_params.dart';
import 'package:sipolair/cubit/patrol_form/cubit.dart';
import 'package:sipolair/models/patrol.dart';
import 'package:sipolair/models/ship.dart';
import 'package:sipolair/screen/helpers/modal_manager.dart';
import 'package:sipolair/screen/helpers/snackbar_manager.dart';
import 'package:sipolair/screen/helpers/util.dart';
import 'package:sipolair/screen/widgets/attachment_picker.dart';
import 'package:sipolair/screen/widgets/date_picker.dart';
import 'package:sipolair/screen/widgets/dropdown_form.dart';
import 'package:sipolair/screen/widgets/empty_widget.dart';
import 'package:sipolair/screen/widgets/form_text_field.dart';
import 'package:sipolair/screen/widgets/hide_focus.dart';
import 'package:sipolair/screen/widgets/submit_button.dart';
import 'package:sipolair/screen/widgets/time_picker.dart';

class PatrolFormPage extends StatefulWidget {
  const PatrolFormPage({Key? key}) : super(key: key);

  @override
  _PatrolFormPageState createState() => _PatrolFormPageState();
}

class _PatrolFormPageState extends State<PatrolFormPage> {
  final _formKey = GlobalKey<FormState>();

  late TextEditingController dateController,
      startTimeController,
      endTimeController,
      locationController,
      contentController;

  bool isValidate = false;

  String? date, startTime, endTime;
  List<String> files = [];

  Patrol? patrol;
  Ship? ship;

  @override
  void initState() {
    super.initState();
    startTimeController = TextEditingController();
    endTimeController = TextEditingController();
    dateController = TextEditingController();
    locationController = TextEditingController();
    contentController = TextEditingController();
  }

  @override
  void dispose() {
    startTimeController.dispose();
    endTimeController.dispose();
    dateController.dispose();
    locationController.dispose();
    contentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return HideFocus(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: ColorUtil.darkBlue,
          title: Text('Laporan Patroli'),
        ),
        body: BlocConsumer<PatrolFormCubit, PatrolFormState>(
          listener: (context, state) {
            if (state is PatrolSubmitLoading) {
              ModalManager.showLoadingIndicator(context);
            } else if (state is PatrolSubmitFailure) {
              Navigator.of(context).pop();
              SnackBarManager.showSnackBar(
                  context: context,
                  message: state.message,
                  backgroundColor: Colors.red);
            } else if (state is PatrolFormFailure) {
              SnackBarManager.showSnackBar(
                  context: context,
                  message: state.message,
                  backgroundColor: Colors.red);
            } else if (state is PatrolFormSubmitted) {
              Navigator.of(context).pop();
              Navigator.of(context).pop(1);
              SnackBarManager.showSnackBar(
                  context: context,
                  message: state.isSync
                      ? 'Laporan berhasil dikirim'
                      : 'Laporan disimpan di penyimpanan lokal',
                  backgroundColor: ColorUtil.green);
            }
          },
          buildWhen: (previous, current) =>
              current is PatrolFormLoaded || current is PatrolFormFailure,
          builder: (context, state) {
            if (state is PatrolFormFailure) {
              return Center(child: EmptyWidget(message: state.message));
            }
            List<Patrol> patrols = [];
            List<Ship> ships = [];

            if (state is PatrolFormLoaded) {
              patrols = state.patrols;
              ships = state.ships;
            }

            return SingleChildScrollView(
              child: Form(
                key: _formKey,
                autovalidateMode: isValidate
                    ? AutovalidateMode.always
                    : AutovalidateMode.disabled,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(16, 16, 16, 16),
                  child: Column(
                    children: [
                      FormDropDown<Patrol>(
                        label: 'Patroli',
                        hintText: 'Pilih Patroli',
                        items: patrols
                            .map((e) =>
                                SimpleDialogItem(title: e.name, value: e))
                            .toList(),
                        selectedItem: patrol,
                        validator: (value) => value?.isEmpty ?? true
                            ? 'Patroli tidak boleh kosong'
                            : null,
                        onSelected: (value) {
                          setState(() {
                            patrol = value;
                          });
                        },
                      ),
                      SizedBox(height: 14),
                      FormDropDown<Ship>(
                        label: 'Kapal',
                        hintText: 'Pilih Kapal',
                        items: ships
                            .map((e) =>
                                SimpleDialogItem(title: e.name, value: e))
                            .toList(),
                        selectedItem: ship,
                        validator: (value) => value?.isEmpty ?? true
                            ? 'Kapal tidak boleh kosong'
                            : null,
                        onSelected: (value) {
                          setState(() {
                            ship = value;
                          });
                        },
                      ),
                      SizedBox(height: 14),
                      FormTextField(
                        controller: locationController,
                        label: 'Lokasi',
                        hintText: 'Lokasi Kegiatan',
                        isDense: true,
                        minLines: 1,
                        maxLines: 3,
                        inputAction: TextInputAction.next,
                        validator: (value) => value?.isEmpty ?? false
                            ? 'Lokasi Kegiatan tidak boleh kosong'
                            : null,
                      ),
                      SizedBox(height: 14),
                      DatePicker(
                        controller: dateController,
                        onDateChanged: (value) {
                          date = DateHelper.formatDateTime(value, 'yyyy-MM-dd');
                        },
                      ),
                      SizedBox(height: 14),
                      Row(
                        children: [
                          Expanded(
                              child: TimePicker(
                            controller: startTimeController,
                            label: 'Waktu Mulai',
                            onTimeChanged: (value) {
                              startTime = '${value.hour}:${value.minute}:00';
                            },
                          )),
                          SizedBox(width: 8),
                          Expanded(
                            child: TimePicker(
                              controller: endTimeController,
                              label: 'Waktu Akhir',
                              onTimeChanged: (value) {
                                endTime = '${value.hour}:${value.minute}:00';
                              },
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 14),
                      FormTextField(
                        controller: contentController,
                        label: 'Narasi Laporan',
                        hintText: 'Jelaskan Narasi Laporan',
                        isDense: true,
                        minLines: 3,
                        maxLines: 6,
                        validator: (value) => value?.isEmpty ?? false
                            ? 'Narasi Laporan tidak boleh kosong'
                            : null,
                      ),
                      SizedBox(height: 14),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Dokumen',
                            style: TextStyle(
                                fontSize: 12, fontWeight: FontWeight.w500),
                          ),
                          SizedBox(height: 6),
                          AttachmentPicker(onFileSelected: (value) {
                            setState(() {
                              files = value.map((e) => e!).toList();
                            });
                          }),
                        ],
                      ),
                      SizedBox(height: 24),
                      SizedBox(
                        width: double.infinity,
                        child: SubmitButton(onPressed: () {
                          if (_formKey.currentState?.validate() ?? false) {
                            context.read<PatrolFormCubit>().submitReport(
                                params: PatrolParams(
                                    idShip: ship!.id,
                                    idPatrol: patrol!.id,
                                    contents: contentController.text,
                                    location: locationController.text,
                                    date: date!,
                                    startTime: startTime!,
                                    endTime: endTime!,
                                    files: files));
                            return;
                          }
                          setState(() {
                            isValidate = true;
                          });
                        }),
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
