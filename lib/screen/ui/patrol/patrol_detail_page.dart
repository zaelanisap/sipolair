import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sipolair/cubit/patrol/cubit.dart';
import 'package:sipolair/screen/helpers/snackbar_manager.dart';
import 'package:sipolair/screen/helpers/util.dart';
import 'package:sipolair/screen/widgets/attachment_view.dart';
import 'package:sipolair/screen/widgets/form_text_field.dart';
import 'package:sipolair/screen/widgets/hide_focus.dart';

class DetailPatrolPage extends StatelessWidget {
  final int id;
  const DetailPatrolPage({
    Key? key,
    required this.id,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return HideFocus(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: ColorUtil.darkBlue,
          title: Text('Detail Laporan Patroli'),
        ),
        body: BlocConsumer<PatrolCubit, PatrolState>(
          listener: (context, state) {
            if (state is PatrolFailure) {
              SnackBarManager.showSnackBar(
                  context: context,
                  message: state.message,
                  backgroundColor: Colors.red);
            }
          },
          builder: (context, state) {
            if (state is PatrolDetailLoaded) {
              var report = state.report;

              return RefreshIndicator(
                onRefresh: () async {
                  context.read<PatrolCubit>().getDetailReport(id: id);
                },
                child: SingleChildScrollView(
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      FormTextField(
                        initialValue: report.patrol.name,
                        readOnly: true,
                        label: 'Patroli',
                        hintText: 'Patroli',
                        isDense: true,
                      ),
                      SizedBox(height: 14),
                      FormTextField(
                        initialValue: report.ship.name,
                        readOnly: true,
                        label: 'Kapal',
                        hintText: 'Kapal Patroli',
                        isDense: true,
                      ),
                      SizedBox(height: 14),
                      FormTextField(
                        initialValue: report.location,
                        readOnly: true,
                        label: 'Lokasi',
                        hintText: 'Lokasi Patroli',
                        isDense: true,
                      ),
                      SizedBox(height: 14),
                      FormTextField(
                        initialValue:
                            DateHelper.formatDateTimeFromString(report.date),
                        readOnly: true,
                        label: 'Tanggal',
                        hintText: 'Tanggal Patroli',
                        isDense: true,
                      ),
                      SizedBox(height: 14),
                      Row(
                        children: [
                          Expanded(
                            child: FormTextField(
                              initialValue: DateHelper.formatDateTimeFromString(
                                  report.startTime, 'HH:mm'),
                              readOnly: true,
                              label: 'Waktu Mulai',
                              hintText: 'Waktu Mulai',
                              isDense: true,
                            ),
                          ),
                          SizedBox(width: 14),
                          Expanded(
                            child: FormTextField(
                              initialValue: DateHelper.formatDateTimeFromString(
                                  report.endTime, 'HH:mm'),
                              readOnly: true,
                              label: 'Waktu Akhir',
                              hintText: 'Waktu Akhir',
                              isDense: true,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 14),
                      FormTextField(
                        initialValue: report.zone?.name ?? '-',
                        readOnly: true,
                        label: 'Zona',
                        hintText: 'Zona',
                        isDense: true,
                        minLines: 1,
                        maxLines: 3,
                      ),
                      SizedBox(height: 14),
                      FormTextField(
                        initialValue: report.contents,
                        readOnly: true,
                        label: 'Narasi Laporan',
                        hintText: 'Narasi Laporan',
                        isDense: true,
                        minLines: 3,
                        maxLines: 6,
                      ),
                      SizedBox(height: 14),
                      AttachmentView(files: report.files!)
                    ],
                  ),
                ),
              );
            }
            return Center(child: CircularProgressIndicator());
          },
        ),
      ),
    );
  }
}
