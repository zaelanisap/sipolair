import 'package:dropdown_sheet/dropdown_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:sipolair/core/utils/pref_utils.dart';
import 'package:sipolair/cubit/patrol/cubit.dart';
import 'package:sipolair/cubit/patrol/patrol_cubit.dart';
import 'package:sipolair/cubit/patrol_form/cubit.dart';
import 'package:sipolair/cubit/report/cubit.dart';
import 'package:sipolair/cubit/report_form/cubit.dart';
import 'package:sipolair/models/patrol.dart';
import 'package:sipolair/models/patrol_report.dart';
import 'package:sipolair/models/zone.dart';
import 'package:sipolair/routes/routes.dart';
import 'package:sipolair/screen/helpers/modal_manager.dart';
import 'package:sipolair/screen/helpers/snackbar_manager.dart';
import 'package:sipolair/screen/helpers/util.dart';
import 'package:sipolair/screen/widgets/custom_button.dart';
import 'package:sipolair/screen/widgets/dropdown_form.dart';
import 'package:sipolair/screen/widgets/empty_widget.dart';
import 'package:sipolair/screen/widgets/filtered_datepicker.dart';

class PatrolListPage extends StatefulWidget {
  const PatrolListPage({Key? key}) : super(key: key);

  @override
  _PatrolListPageState createState() => _PatrolListPageState();
}

class _PatrolListPageState extends State<PatrolListPage> {
  String? _startDate, _endDate;
  Zone? zone;
  Patrol? patrol;

  @override
  void initState() {
    super.initState();
    context.read<PatrolCubit>().getListReport(
        idPatrol: patrol?.id,
        idZone: zone?.id,
        startDate: _startDate,
        endDate: _endDate);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        context.read<PatrolCubit>().getListReport(
            idPatrol: patrol?.id,
            idZone: zone?.id,
            startDate: _startDate,
            endDate: _endDate);
      },
      child: BlocBuilder<PatrolCubit, PatrolState>(
        buildWhen: (previous, current) => current is PatrolListLoaded,
        builder: (context, state) {
          if (state is PatrolListLoaded) {
            var reports = state.listPatrol;
            var localReports = [];
            bool isEmpty = reports.isEmpty && localReports.isEmpty;
            return CustomScrollView(
              slivers: [
                if (localReports.isNotEmpty)
                  SliverToBoxAdapter(
                    child: Container(
                      color: ColorUtil.lightOrange,
                      padding: EdgeInsets.fromLTRB(8, 12, 8, 12),
                      child: Row(
                        children: [
                          CustomButton(
                            onTap: () {},
                            child: Icon(Icons.info_outline, size: 18),
                          ),
                          SizedBox(width: 4),
                          Text('Beberapa laporan patroli perlu disinkronisasi'),
                        ],
                      ),
                    ),
                  ),
                SliverToBoxAdapter(
                  child: ExpansionTile(
                    maintainState: true,
                    title: Text('Filter'),
                    trailing: Icon(Icons.filter_list),
                    children: [dropdownFilters(), filterDate()],
                  ),
                ),
                if (isEmpty)
                  SliverFillRemaining(
                      child: EmptyWidget(message: 'Tidak ada laporan')),
                if (localReports.isNotEmpty) ...[
                  SliverToBoxAdapter(
                    child: Container(
                      padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Perlu Sinkronisasi',
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w500),
                          ),
                          BlocProvider(
                            create: (_) => ReportFormCubit(Get.find()),
                            child: Builder(
                              builder: (ctx) => BlocListener<ReportFormCubit,
                                  ReportFormState>(
                                listener: (ctx, state) {
                                  if (state is ReportFormSubmitted) {
                                    Navigator.of(context).pop();
                                    context.read<ReportCubit>().getReports();
                                  } else if (state is ReportSubmitFailure) {
                                    Navigator.of(context).pop();
                                    SnackBarManager.showSnackBar(
                                        context: context,
                                        message: state.message,
                                        backgroundColor: Colors.red);
                                  } else if (state is ReportSubmitLoading) {
                                    ModalManager.showLoadingIndicator(context);
                                  }
                                },
                                child: CustomButton(
                                  onTap: () {
                                    ctx.read<ReportFormCubit>().syncReports();
                                  },
                                  child: Text(
                                    'Perbarui',
                                    style: TextStyle(
                                        fontSize: 13,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  SliverPadding(
                    padding: EdgeInsets.fromLTRB(8, 8, 8, 16),
                    sliver: SliverList(
                        delegate: SliverChildBuilderDelegate(
                      (context, index) {
                        var report = localReports[index];
                        return Padding(
                          padding: const EdgeInsets.only(bottom: 12),
                          child: itemReport(report),
                        );
                      },
                      childCount: localReports.length,
                    )),
                  ),
                ],
                if (reports.isNotEmpty)
                  SliverPadding(
                    padding: EdgeInsets.fromLTRB(8, 16, 8, 16),
                    sliver: SliverList(
                        delegate: SliverChildBuilderDelegate(
                      (context, index) {
                        var report = reports[index];
                        return Padding(
                            padding: const EdgeInsets.only(bottom: 12),
                            child: itemReport(report));
                      },
                      childCount: reports.length,
                    )),
                  ),
              ],
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  Widget dropdownFilters() {
    return BlocProvider(
      create: (_) => PatrolFormCubit(Get.find())..getReportForm(),
      child: BlocBuilder<PatrolFormCubit, PatrolFormState>(
        builder: (context, state) {
          List<Patrol> patrols = [];
          List<Zone> zones = [];
          if (state is PatrolFormLoaded) {
            zones = state.zones;
            patrols = state.patrols;
          }

          return Padding(
            padding: const EdgeInsets.fromLTRB(8, 0, 8, 16),
            child: Column(
              children: [
                FormDropDown<Patrol>(
                  backgroundColor: Colors.white,
                  label: 'Patrol',
                  hintText: 'Pilih Patrol',
                  items: patrols
                      .map((e) => SimpleDialogItem(title: e.name, value: e))
                      .toList(),
                  selectedItem: patrol,
                  validator: (value) => value?.isEmpty ?? true
                      ? 'Patrol tidak boleh kosong'
                      : null,
                  onSelected: (value) {
                    setState(() {
                      patrol = value;
                    });
                    context.read<PatrolCubit>().getListReport(
                        idPatrol: patrol?.id,
                        idZone: zone?.id,
                        startDate: _startDate,
                        endDate: _endDate);
                  },
                ),
                if (PrefUtil.instance.idAgency != 6)
                  Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: FormDropDown<Zone>(
                      backgroundColor: Colors.white,
                      label: 'Zona',
                      hintText: 'Pilih Zona',
                      items: zones
                          .map((e) => SimpleDialogItem(title: e.name, value: e))
                          .toList(),
                      selectedItem: zone,
                      validator: (value) => value?.isEmpty ?? true
                          ? 'Zona tidak boleh kosong'
                          : null,
                      onSelected: (value) {
                        setState(() {
                          zone = value;
                        });
                        context.read<PatrolCubit>().getListReport(
                            idPatrol: patrol?.id,
                            idZone: zone?.id,
                            startDate: _startDate,
                            endDate: _endDate);
                      },
                    ),
                  ),
              ],
            ),
          );
        },
      ),
    );
  }

  Widget filterDate() {
    return Builder(
      builder: (context) => Align(
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: EdgeInsets.only(left: 8, bottom: 16),
          child: FilterDatePicker(onSelected: (dateTimeRange) {
            setState(() {
              _startDate = dateTimeRange != null
                  ? DateHelper.formatDateTime(dateTimeRange.start, 'yyyy-MM-dd')
                  : null;
              _endDate = dateTimeRange != null
                  ? DateHelper.formatDateTime(dateTimeRange.end, 'yyyy-MM-dd')
                  : null;
            });

            context.read<PatrolCubit>().getListReport(
                idPatrol: patrol?.id,
                idZone: zone?.id,
                startDate: _startDate,
                endDate: _endDate);
          }),
        ),
      ),
    );
  }

  Widget itemReport(PatrolReport report) {
    return GestureDetector(
      onTap: () {
        // if (report.isSync)
        Navigator.of(context)
            .pushNamed(Routes.DETAILPATROLPAGE, arguments: report.id);
      },
      child: Container(
        padding: EdgeInsets.all(12),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
        ),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 40),
                    child: Text(
                      report.patrol.name,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 13,
                      ),
                    ),
                  ),
                  SizedBox(height: 6),
                  Text("Zona : ${report.zone?.name ?? '-'}"),
                  SizedBox(height: 2),
                  Text('Lokasi : ${report.location}'),
                  SizedBox(height: 16),
                  Text(
                      '${DateHelper.mappingDateTime(report.date)} ${DateHelper.formatDateTimeFromString(report.startTime, 'HH:mm')}'),
                ],
              ),
            ),
            SizedBox(width: 14),
            Container(
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  border: Border.all(color: Colors.black54),
                  shape: BoxShape.rectangle),
              child: Icon(
                Icons.arrow_forward,
                color: Colors.black54,
              ),
            )
          ],
        ),
      ),
    );
  }
}
