import 'package:dropdown_sheet/dropdown_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sipolair/core/params/bimas_params.dart';
import 'package:sipolair/cubit/bimas_form/cubit.dart';
import 'package:sipolair/screen/helpers/modal_manager.dart';
import 'package:sipolair/screen/helpers/snackbar_manager.dart';
import 'package:sipolair/screen/helpers/util.dart';
import 'package:sipolair/screen/widgets/attachment_picker.dart';
import 'package:sipolair/screen/widgets/date_picker.dart';
import 'package:sipolair/screen/widgets/dropdown_form.dart';
import 'package:sipolair/screen/widgets/form_text_field.dart';
import 'package:sipolair/screen/widgets/hide_focus.dart';
import 'package:sipolair/screen/widgets/submit_button.dart';
import 'package:sipolair/screen/widgets/time_picker.dart';

class BimasFormPage extends StatefulWidget {
  const BimasFormPage({Key? key}) : super(key: key);

  @override
  _BimasFormPageState createState() => _BimasFormPageState();
}

class _BimasFormPageState extends State<BimasFormPage> {
  final _formKey = GlobalKey<FormState>();

  late TextEditingController titleController,
      picController,
      participantController,
      dateController,
      timeController,
      locationController,
      contentController;

  bool isValidate = false;

  List<String> timezones = ['WIB', 'WITA', 'WIT'];
  String timezone = 'WIB';
  String? date, time;
  List<String> files = [];

  @override
  void initState() {
    super.initState();
    titleController = TextEditingController();
    picController = TextEditingController();
    participantController = TextEditingController();
    dateController = TextEditingController();
    timeController = TextEditingController();
    locationController = TextEditingController();
    contentController = TextEditingController();
  }

  @override
  void dispose() {
    titleController.dispose();
    picController.dispose();
    participantController.dispose();
    dateController.dispose();
    timeController.dispose();
    locationController.dispose();
    contentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return HideFocus(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: ColorUtil.darkBlue,
          title: Text('Laporan Bimbingan Masyarakat'),
        ),
        body: BlocConsumer<BimasFormCubit, BimasFormState>(
          listener: (context, state) {
            if (state is BimasSubmitLoading) {
              ModalManager.showLoadingIndicator(context);
            } else if (state is BimasSubmitFailure) {
              Navigator.of(context).pop();
              SnackBarManager.showSnackBar(
                  context: context,
                  message: state.message,
                  backgroundColor: Colors.red);
            } else if (state is BimasFormFailure) {
              SnackBarManager.showSnackBar(
                  context: context,
                  message: state.message,
                  backgroundColor: Colors.red);
            } else if (state is BimasFormSubmitted) {
              Navigator.of(context).pop();
              Navigator.of(context).pop(1);
              SnackBarManager.showSnackBar(
                  context: context,
                  message: state.isSync
                      ? 'Laporan berhasil dikirim'
                      : 'Laporan disimpan di penyimpanan lokal',
                  backgroundColor: ColorUtil.green);
            }
          },
          buildWhen: (previous, current) =>
              current is BimasFormLoaded || current is BimasFormFailure,
          builder: (context, state) {
            return SingleChildScrollView(
              child: Form(
                key: _formKey,
                autovalidateMode: isValidate
                    ? AutovalidateMode.always
                    : AutovalidateMode.disabled,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(16, 16, 16, 16),
                  child: Column(
                    children: [
                      FormTextField(
                        controller: titleController,
                        label: 'Nama Kegiatan',
                        hintText: 'Nama Kegiatan',
                        isDense: true,
                        inputAction: TextInputAction.next,
                        validator: (value) => value?.isEmpty ?? false
                            ? 'Nama Kegiatan tidak boleh kosong'
                            : null,
                      ),
                      SizedBox(height: 14),
                      FormTextField(
                        controller: picController,
                        label: 'Penanggung Jawab',
                        hintText: 'Penanggung Jawab',
                        isDense: true,
                        inputAction: TextInputAction.next,
                        validator: (value) => value?.isEmpty ?? false
                            ? 'Penanggung Jawab tidak boleh kosong'
                            : null,
                      ),
                      SizedBox(height: 14),
                      FormTextField(
                        controller: participantController,
                        label: 'Jumlah Peserta',
                        hintText: 'Jumlah Peserta',
                        isDense: true,
                        inputType: TextInputType.number,
                        inputAction: TextInputAction.next,
                        validator: (value) {
                          if (value?.isEmpty ?? true) {
                            return 'Jumlah Peserta tidak boleh kosong';
                          } else if (int.tryParse(value!) == null) {
                            return 'Masukkan angka saja';
                          }
                          return null;
                        },
                      ),
                      SizedBox(height: 14),
                      DatePicker(
                        controller: dateController,
                        onDateChanged: (value) {
                          date = DateHelper.formatDateTime(value, 'yyyy-MM-dd');
                        },
                      ),
                      SizedBox(height: 14),
                      Row(
                        children: [
                          Expanded(
                              child: TimePicker(
                            controller: timeController,
                            onTimeChanged: (value) {
                              time = '${value.hour}:${value.minute}:00';
                            },
                          )),
                          SizedBox(width: 14),
                          Expanded(
                            child: FormDropDown<String>(
                                label: 'Zona Waktu',
                                hintText: 'Pilih Zona Waktu',
                                items: timezones
                                    .map((e) =>
                                        SimpleDialogItem(title: e, value: e))
                                    .toList(),
                                selectedItem: timezone,
                                validator: (value) => value?.isEmpty ?? true
                                    ? 'Zona Waktu tidak boleh kosong'
                                    : null,
                                onSelected: (value) {
                                  setState(() {
                                    timezone = value;
                                  });
                                }),
                          ),
                        ],
                      ),
                      SizedBox(height: 14),
                      FormTextField(
                        controller: locationController,
                        label: 'Lokasi',
                        hintText: 'Lokasi Kegiatan',
                        isDense: true,
                        minLines: 1,
                        maxLines: 3,
                        inputAction: TextInputAction.next,
                        validator: (value) => value?.isEmpty ?? false
                            ? 'Lokasi Kegiatan tidak boleh kosong'
                            : null,
                      ),
                      SizedBox(height: 14),
                      FormTextField(
                        controller: contentController,
                        label: 'Narasi Laporan',
                        hintText: 'Narasi Laporan',
                        isDense: true,
                        minLines: 3,
                        maxLines: 6,
                        validator: (value) => value?.isEmpty ?? false
                            ? 'Narasi Laporan tidak boleh kosong'
                            : null,
                      ),
                      SizedBox(height: 14),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Foto/Video/Dokumen',
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          ),
                          SizedBox(height: 6),
                          AttachmentPicker(onFileSelected: (files) {
                            setState(() {
                              this.files = files.map((e) => e!).toList();
                            });
                          })
                        ],
                      ),
                      SizedBox(height: 24),
                      SizedBox(
                        width: double.infinity,
                        child: SubmitButton(onPressed: () {
                          if (_formKey.currentState?.validate() ?? false) {
                            context.read<BimasFormCubit>().submitReport(
                                  params: BimasParams(
                                      title: titleController.text,
                                      contents: contentController.text,
                                      pic: picController.text,
                                      location: locationController.text,
                                      totalParticipant:
                                          int.parse(participantController.text),
                                      date: date!,
                                      time: time!,
                                      timezone: timezone,
                                      files: files),
                                );
                            return;
                          }
                          setState(() {
                            isValidate = true;
                          });
                        }),
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
