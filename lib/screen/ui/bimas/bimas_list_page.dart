import 'package:dropdown_sheet/dropdown_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:sipolair/core/utils/pref_utils.dart';
import 'package:sipolair/cubit/bimas/cubit.dart';
import 'package:sipolair/cubit/bimas_form/cubit.dart';
import 'package:sipolair/cubit/report/cubit.dart';
import 'package:sipolair/cubit/report_form/cubit.dart';
import 'package:sipolair/models/bimas.dart';
import 'package:sipolair/models/zone.dart';
import 'package:sipolair/routes/routes.dart';
import 'package:sipolair/screen/helpers/modal_manager.dart';
import 'package:sipolair/screen/helpers/snackbar_manager.dart';
import 'package:sipolair/screen/helpers/util.dart';
import 'package:sipolair/screen/widgets/custom_button.dart';
import 'package:sipolair/screen/widgets/dropdown_form.dart';
import 'package:sipolair/screen/widgets/empty_widget.dart';
import 'package:sipolair/screen/widgets/filtered_datepicker.dart';
import 'package:sipolair/screen/widgets/form_text_field.dart';

class BimasListPage extends StatefulWidget {
  const BimasListPage({Key? key}) : super(key: key);

  @override
  _BimasListPageState createState() => _BimasListPageState();
}

class _BimasListPageState extends State<BimasListPage> {
  late TextEditingController _titleController, _picController;
  String? _startDate, _endDate;
  Zone? zone;

  @override
  void initState() {
    super.initState();
    _titleController = TextEditingController();
    _picController = TextEditingController();
    context.read<BimasCubit>().getListBimas(
        idZone: zone?.id,
        title: _titleController.text,
        pic: _picController.text,
        startDate: _startDate,
        endDate: _endDate);
  }

  @override
  void dispose() {
    _titleController.dispose();
    _picController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        context.read<BimasCubit>().getListBimas(
            idZone: zone?.id,
            title: _titleController.text,
            pic: _picController.text,
            startDate: _startDate,
            endDate: _endDate);
      },
      child: BlocBuilder<BimasCubit, BimasState>(
        buildWhen: (previous, current) => current is BimasListLoaded,
        builder: (context, state) {
          if (state is BimasListLoaded) {
            var bimasList = state.listBimas;
            var localBimasList = [];
            bool isEmpty = bimasList.isEmpty && localBimasList.isEmpty;
            return CustomScrollView(
              slivers: [
                if (localBimasList.isNotEmpty)
                  SliverToBoxAdapter(
                    child: Container(
                      color: ColorUtil.lightOrange,
                      padding: EdgeInsets.fromLTRB(8, 12, 8, 12),
                      child: Row(
                        children: [
                          CustomButton(
                            onTap: () {},
                            child: Icon(Icons.info_outline, size: 18),
                          ),
                          SizedBox(width: 4),
                          Text(
                              'Beberapa laporan bimbingan masyarakat perlu disinkronisasi'),
                        ],
                      ),
                    ),
                  ),
                SliverToBoxAdapter(
                  child: ExpansionTile(
                    maintainState: true,
                    title: Text('Filter'),
                    trailing: Icon(Icons.filter_list),
                    children: [
                      searchInput(context),
                      if (PrefUtil.instance.idAgency != 6) zoneFilter(),
                      filterDate()
                    ],
                  ),
                ),
                if (isEmpty)
                  SliverFillRemaining(
                      child: EmptyWidget(message: 'Tidak ada laporan')),
                if (localBimasList.isNotEmpty) ...[
                  SliverToBoxAdapter(
                    child: Container(
                      padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Perlu Sinkronisasi',
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w500),
                          ),
                          BlocProvider(
                            create: (_) => ReportFormCubit(Get.find()),
                            child: Builder(
                              builder: (ctx) => BlocListener<ReportFormCubit,
                                  ReportFormState>(
                                listener: (ctx, state) {
                                  if (state is ReportFormSubmitted) {
                                    Navigator.of(context).pop();
                                    context.read<ReportCubit>().getReports();
                                  } else if (state is ReportSubmitFailure) {
                                    Navigator.of(context).pop();
                                    SnackBarManager.showSnackBar(
                                        context: context,
                                        message: state.message,
                                        backgroundColor: Colors.red);
                                  } else if (state is ReportSubmitLoading) {
                                    ModalManager.showLoadingIndicator(context);
                                  }
                                },
                                child: CustomButton(
                                  onTap: () {
                                    ctx.read<ReportFormCubit>().syncReports();
                                  },
                                  child: Text(
                                    'Perbarui',
                                    style: TextStyle(
                                        fontSize: 13,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  SliverPadding(
                    padding: EdgeInsets.fromLTRB(8, 8, 8, 16),
                    sliver: SliverList(
                        delegate: SliverChildBuilderDelegate(
                      (context, index) {
                        var bimas = localBimasList[index];
                        return Padding(
                          padding: const EdgeInsets.only(bottom: 12),
                          child: itemBimas(bimas),
                        );
                      },
                      childCount: localBimasList.length,
                    )),
                  ),
                ],
                if (bimasList.isNotEmpty)
                  SliverPadding(
                    padding: EdgeInsets.fromLTRB(8, 16, 8, 16),
                    sliver: SliverList(
                        delegate: SliverChildBuilderDelegate(
                      (context, index) {
                        var report = bimasList[index];
                        return Padding(
                            padding: const EdgeInsets.only(bottom: 12),
                            child: itemBimas(report));
                      },
                      childCount: bimasList.length,
                    )),
                  ),
              ],
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  Widget zoneFilter() {
    return BlocProvider(
      create: (_) => BimasFormCubit(Get.find())..getReportForm(),
      child: BlocBuilder<BimasFormCubit, BimasFormState>(
        builder: (context, state) {
          List<Zone> zones = [];
          if (state is BimasFormLoaded) {
            zones = state.zones;
          }

          return Padding(
            padding: const EdgeInsets.fromLTRB(8, 0, 8, 16),
            child: FormDropDown<Zone>(
                backgroundColor: Colors.white,
                label: 'Zona',
                hintText: 'Pilih Zona',
                items: zones
                    .map((e) => SimpleDialogItem(title: e.name, value: e))
                    .toList(),
                selectedItem: zone,
                validator: (value) =>
                    value?.isEmpty ?? true ? 'Zona tidak boleh kosong' : null,
                onSelected: (value) {
                  setState(() {
                    zone = value;
                  });
                  context.read<BimasCubit>().getListBimas(
                      title: _titleController.text,
                      idZone: zone?.id,
                      pic: _picController.text,
                      startDate: _startDate,
                      endDate: _endDate);
                }),
          );
        },
      ),
    );
  }

  Widget searchInput(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 16, 8, 16),
      child: CustomTextField(
        controller: _titleController,
        backgroundColor: Colors.white,
        hintText: 'Cari nama kegiatan...',
        inputAction: TextInputAction.search,
        isDense: true,
        prefixIcon: Icon(Icons.search),
        onChanged: (value) {
          context.read<BimasCubit>().getListBimas(
              idZone: zone?.id,
              title: value,
              pic: _picController.text,
              startDate: _startDate,
              endDate: _endDate);
        },
      ),
    );
  }

  Widget filterDate() {
    return Builder(
      builder: (context) => Align(
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: EdgeInsets.only(left: 8, bottom: 16),
          child: FilterDatePicker(
            onSelected: (dateTimeRange) {
              setState(() {
                _startDate = dateTimeRange != null
                    ? DateHelper.formatDateTime(
                        dateTimeRange.start, 'yyyy-MM-dd')
                    : null;
                _endDate = dateTimeRange != null
                    ? DateHelper.formatDateTime(dateTimeRange.end, 'yyyy-MM-dd')
                    : null;
              });

              context.read<BimasCubit>().getListBimas(
                  idZone: zone?.id,
                  title: _titleController.text,
                  pic: _picController.text,
                  startDate: _startDate,
                  endDate: _endDate);
            },
          ),
        ),
      ),
    );
  }

  Widget itemBimas(Bimas bimas) {
    return GestureDetector(
      onTap: () {
        // if (report.isSync)
        Navigator.of(context)
            .pushNamed(Routes.DETAILBIMASPAGE, arguments: bimas.id);
      },
      child: Container(
        padding: EdgeInsets.all(12),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
        ),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 40),
                    child: Text(
                      bimas.zone?.name ?? '-',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 13,
                      ),
                    ),
                  ),
                  SizedBox(height: 6),
                  Text('Nama Kegiatan : ${bimas.title}'),
                  SizedBox(height: 2),
                  Text('Penanggung Jawab : ${bimas.pic}'),
                  SizedBox(height: 16),
                  Text(
                      '${DateHelper.mappingDateTime(bimas.date)} ${bimas.time.substring(0, 5)}'),
                ],
              ),
            ),
            SizedBox(width: 14),
            Container(
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  border: Border.all(color: Colors.black54),
                  shape: BoxShape.rectangle),
              child: Icon(
                Icons.arrow_forward,
                color: Colors.black54,
              ),
            )
          ],
        ),
      ),
    );
  }
}
