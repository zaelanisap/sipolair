import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sipolair/cubit/bimas/cubit.dart';
import 'package:sipolair/screen/helpers/snackbar_manager.dart';
import 'package:sipolair/screen/helpers/util.dart';
import 'package:sipolair/screen/widgets/attachment_view.dart';
import 'package:sipolair/screen/widgets/form_text_field.dart';
import 'package:sipolair/screen/widgets/hide_focus.dart';

class DetailBimasPage extends StatelessWidget {
  final int id;
  const DetailBimasPage({
    Key? key,
    required this.id,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return HideFocus(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: ColorUtil.darkBlue,
          title: Text('Detail Laporan Bimas'),
        ),
        body: BlocConsumer<BimasCubit, BimasState>(
          listener: (context, state) {
            if (state is BimasFailure) {
              SnackBarManager.showSnackBar(
                  context: context,
                  message: state.message,
                  backgroundColor: Colors.red);
            }
          },
          builder: (context, state) {
            if (state is BimasDetailLoaded) {
              var bimas = state.bimas;

              return RefreshIndicator(
                onRefresh: () async {
                  context.read<BimasCubit>().getDetailBimas(id: id);
                },
                child: SingleChildScrollView(
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      FormTextField(
                        initialValue: bimas.title,
                        readOnly: true,
                        label: 'Nama Kegiatan',
                        hintText: 'Nama Kegiatan',
                        isDense: true,
                      ),
                      SizedBox(height: 14),
                      FormTextField(
                        initialValue: bimas.pic,
                        readOnly: true,
                        label: 'Penanggung Jawab',
                        hintText: 'Penanggung Jawab',
                        isDense: true,
                      ),
                      SizedBox(height: 14),
                      FormTextField(
                        initialValue: bimas.totalParticipant.toString(),
                        readOnly: true,
                        label: 'Jumlah Peserta',
                        hintText: 'Jumlah Peserta',
                        isDense: true,
                      ),
                      SizedBox(height: 14),
                      FormTextField(
                        initialValue:
                            DateHelper.formatDateTimeFromString(bimas.date),
                        readOnly: true,
                        label: 'Tanggal',
                        hintText: 'Tanggal Kegiatan',
                        isDense: true,
                      ),
                      SizedBox(height: 14),
                      Row(
                        children: [
                          Expanded(
                            child: FormTextField(
                              initialValue: bimas.time,
                              readOnly: true,
                              label: 'Waktu',
                              hintText: 'Waktu Kegiatan',
                              isDense: true,
                            ),
                          ),
                          SizedBox(width: 14),
                          Expanded(
                            child: FormTextField(
                              initialValue: bimas.timezone,
                              readOnly: true,
                              label: 'Zona Waktu',
                              hintText: 'Zona Waktu',
                              isDense: true,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 14),
                      FormTextField(
                        initialValue: bimas.location,
                        readOnly: true,
                        label: 'Lokasi',
                        hintText: 'Lokasi Kegiatan',
                        isDense: true,
                        minLines: 1,
                        maxLines: 3,
                      ),
                      SizedBox(height: 14),
                      FormTextField(
                        initialValue: bimas.zone?.name ?? '-',
                        readOnly: true,
                        label: 'Zona',
                        hintText: 'Zona',
                        isDense: true,
                      ),
                      SizedBox(height: 14),
                      FormTextField(
                        initialValue: bimas.contents,
                        readOnly: true,
                        label: 'Narasi Laporan',
                        hintText: 'Narasi Laporan',
                        isDense: true,
                        minLines: 3,
                        maxLines: 6,
                      ),
                      SizedBox(height: 14),
                      AttachmentView(files: bimas.files!)
                    ],
                  ),
                ),
              );
            }
            return Center(child: CircularProgressIndicator());
          },
        ),
      ),
    );
  }
}
