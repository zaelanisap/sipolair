import 'package:dropdown_sheet/dropdown_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sipolair/core/params/report_params.dart';
import 'package:sipolair/cubit/report_form/cubit.dart';
import 'package:sipolair/models/criminal_category.dart';
import 'package:sipolair/models/report_type.dart';
import 'package:sipolair/models/ship.dart';
import 'package:sipolair/screen/helpers/modal_manager.dart';
import 'package:sipolair/screen/helpers/snackbar_manager.dart';
import 'package:sipolair/screen/helpers/util.dart';
import 'package:sipolair/screen/ui/report/suspect_list.dart';
import 'package:sipolair/screen/widgets/attachment_picker.dart';
import 'package:sipolair/screen/widgets/date_picker.dart';
import 'package:sipolair/screen/widgets/dropdown_form.dart';
import 'package:sipolair/screen/widgets/empty_widget.dart';
import 'package:sipolair/screen/widgets/form_text_field.dart';
import 'package:sipolair/screen/widgets/hide_focus.dart';
import 'package:sipolair/screen/widgets/submit_button.dart';
import 'package:sipolair/screen/widgets/time_picker.dart';

class ReportFormPage extends StatefulWidget {
  @override
  _ReportFormPageState createState() => _ReportFormPageState();
}

class _ReportFormPageState extends State<ReportFormPage> {
  final _formKey = GlobalKey<FormState>();
  bool _isValidate = false;
  final now = DateTime.now();

  bool readOnly = false;
  ReportParams report = ReportParams();
  CriminalCategory? _category;
  Ship? ship;
  ReportType reportType = ReportType(id: 1, name: 'Laporan Pengaduan');

  late TextEditingController _codeController,
      _officerController,
      _dateController,
      _timeController,
      _addressController,
      _chronologyController,
      _articleController,
      _evidenceController;

  List<String> elements = ['Intel Air', 'Aparat Patroli', 'Reserse'];
  String? element;

  @override
  void initState() {
    super.initState();
    _codeController = TextEditingController();
    _officerController = TextEditingController();
    _dateController = TextEditingController();
    _timeController = TextEditingController();
    _addressController = TextEditingController();
    _chronologyController = TextEditingController();
    _articleController = TextEditingController();
    _evidenceController = TextEditingController();
  }

  @override
  void dispose() {
    _codeController.dispose();
    _officerController.dispose();
    _dateController.dispose();
    _timeController.dispose();
    _addressController.dispose();
    _chronologyController.dispose();
    _articleController.dispose();
    _evidenceController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return HideFocus(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: ColorUtil.darkBlue,
          title: Text('Laporan Polisi'),
        ),
        body: BlocConsumer<ReportFormCubit, ReportFormState>(
          listener: (context, state) {
            if (state is ReportSubmitLoading) {
              ModalManager.showLoadingIndicator(context);
            } else if (state is ReportSubmitFailure) {
              Navigator.of(context).pop();
              SnackBarManager.showSnackBar(
                  context: context,
                  message: state.message,
                  backgroundColor: Colors.red);
            } else if (state is ReportFormFailure) {
              SnackBarManager.showSnackBar(
                  context: context,
                  message: state.message,
                  backgroundColor: Colors.red);
            } else if (state is ReportFormSubmitted) {
              Navigator.of(context).pop();
              Navigator.of(context).pop(1);
              SnackBarManager.showSnackBar(
                  context: context,
                  message: state.isSync
                      ? 'Laporan berhasil dikirim'
                      : 'Laporan disimpan di penyimpanan lokal',
                  backgroundColor: ColorUtil.green);
            }
          },
          buildWhen: (previous, current) =>
              current is ReportFormLoaded || current is ReportFormFailure,
          builder: (context, state) {
            if (state is ReportFormFailure) {
              return Center(child: EmptyWidget(message: state.message));
            }
            if (state is ReportFormLoaded) {
              var data = state.data;
              return SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  autovalidateMode: _isValidate
                      ? AutovalidateMode.always
                      : AutovalidateMode.disabled,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        color: Colors.white,
                        width: double.infinity,
                        margin: EdgeInsets.only(bottom: 14),
                        padding: const EdgeInsets.fromLTRB(14, 24, 14, 24),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            FormDropDown<ReportType>(
                              label: 'Jenis Laporan',
                              hintText: 'Pilih Jenis Laporan',
                              items: ReportType.reportTypes
                                  .map((e) =>
                                      SimpleDialogItem(title: e.name, value: e))
                                  .toList(),
                              selectedItem: reportType,
                              onSelected: (value) {
                                setState(() {
                                  reportType = value;
                                  _isValidate = false;

                                  if (value.id == 1) {
                                    readOnly = false;
                                    report = ReportParams();
                                    _dateController.clear();
                                    _timeController.clear();
                                    _addressController.clear();
                                    _chronologyController.clear();
                                    _articleController.clear();
                                    _evidenceController.clear();
                                    _officerController.clear();
                                    _codeController.clear();
                                    _category = null;
                                    ship = null;
                                    element = null;
                                  } else {
                                    readOnly = true;
                                    report.date = DateHelper.formatDateTime(
                                        now, 'yyyy-MM-dd');
                                    _dateController.text =
                                        DateHelper.formatDateTime(now);

                                    report.time = DateHelper.formatDateTime(
                                        now, 'HH:mm:ss');
                                    _timeController.text =
                                        DateHelper.formatDateTime(
                                            now, 'HH:mm:ss');

                                    _addressController.text = 'nihil';
                                    _chronologyController.text = 'nihil';
                                    _articleController.text = 'nihil';
                                    _evidenceController.text = 'nihil';
                                    _officerController.clear();
                                    _codeController.text = 'NIHIL';
                                    _category = null;
                                    ship = null;
                                    element = 'nihil';
                                  }
                                });
                              },
                            ),
                          ],
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        width: double.infinity,
                        margin: EdgeInsets.only(bottom: 14),
                        padding: const EdgeInsets.fromLTRB(14, 24, 14, 24),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            if (reportType.id == 1)
                              Padding(
                                padding: const EdgeInsets.only(bottom: 14),
                                child: FormDropDown<String>(
                                  label: 'Unsur',
                                  hintText: 'Pilih Unsur',
                                  items: elements
                                      .map((e) =>
                                          SimpleDialogItem(title: e, value: e))
                                      .toList(),
                                  selectedItem: element,
                                  validator: (value) => value?.isEmpty ?? true
                                      ? 'Unsur tidak boleh kosong'
                                      : null,
                                  onSelected: (value) {
                                    setState(() {
                                      element = value;
                                    });
                                  },
                                ),
                              ),
                            if (element == 'Aparat Patroli')
                              Padding(
                                padding: const EdgeInsets.only(bottom: 14),
                                child: FormDropDown<Ship>(
                                  label: 'Kapal',
                                  hintText: 'Pilih Kapal',
                                  items: data.ships
                                      .map((e) => SimpleDialogItem(
                                          title: e.name, value: e))
                                      .toList(),
                                  selectedItem: ship,
                                  onSelected: (value) {
                                    setState(() {
                                      ship = value;
                                    });
                                  },
                                  validator: (value) => value?.isEmpty ?? true
                                      ? 'Kapal tidak boleh kosong'
                                      : null,
                                ),
                              ),
                            FormTextField(
                              controller: _officerController,
                              label: 'Nama Personil',
                              hintText: 'Nama Personil',
                              isDense: true,
                              inputAction: TextInputAction.next,
                              validator: (value) => value?.isEmpty ?? false
                                  ? 'Nama Personil tidak boleh kosong'
                                  : null,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        width: double.infinity,
                        margin: EdgeInsets.only(bottom: 14),
                        padding: const EdgeInsets.fromLTRB(14, 24, 14, 24),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            FormTextField(
                              controller: _codeController,
                              readOnly: readOnly,
                              label: 'Nomor LP',
                              hintText: 'Nomor LP',
                              isDense: true,
                              textCapitalization: TextCapitalization.characters,
                              inputAction: TextInputAction.next,
                              validator: (value) => value?.isEmpty ?? false
                                  ? 'Nomor LP tidak boleh kosong'
                                  : null,
                            ),
                            SizedBox(height: 14),
                            DatePicker(
                              controller: _dateController,
                              readOnly: readOnly,
                              onDateChanged: (date) {
                                report.date = DateHelper.formatDateTime(
                                    date, 'yyyy-MM-dd');
                              },
                            ),
                            SizedBox(height: 14),
                            TimePicker(
                              controller: _timeController,
                              readOnly: readOnly,
                              onTimeChanged: (time) {
                                report.time = '${time.hour}:${time.minute}:00';
                              },
                            ),
                            if (reportType.id == 1) SizedBox(height: 14),
                            if (reportType.id == 1)
                              FormDropDown<CriminalCategory>(
                                  label: 'Dugaan Pelanggaran',
                                  hintText: 'Pilih Dugaan Pelanggaran',
                                  items: data.categories
                                      .map((e) => SimpleDialogItem(
                                          title: e.name, value: e))
                                      .toList(),
                                  selectedItem: _category,
                                  validator: (value) => value?.isEmpty ?? true
                                      ? 'Dugaan Pelanggaran tidak boleh kosong'
                                      : null,
                                  onSelected: (value) {
                                    setState(() {
                                      _category = value;
                                    });
                                  }),
                            SizedBox(height: 14),
                            FormTextField(
                              controller: _addressController,
                              label: 'Alamat TKP',
                              hintText: 'Tempat kejadian',
                              readOnly: readOnly,
                              isDense: true,
                              minLines: 1,
                              maxLines: 3,
                              validator: (value) => value?.isEmpty ?? false
                                  ? 'Alamat TKP tidak boleh kosong'
                                  : null,
                            ),
                            SizedBox(height: 14),
                            FormTextField(
                              controller: _articleController,
                              readOnly: readOnly,
                              label: 'Pasal',
                              hintText: 'Pasal Pidana',
                              isDense: true,
                              inputAction: TextInputAction.next,
                              validator: (value) => value?.isEmpty ?? false
                                  ? 'Pasal pidana tidak boleh kosong'
                                  : null,
                            ),
                            SizedBox(height: 14),
                            FormTextField(
                              controller: _chronologyController,
                              label: 'Isi Laporan',
                              hintText: 'Jelaskan Isi Laporan',
                              readOnly: readOnly,
                              isDense: true,
                              maxLines: 4,
                              validator: (value) => value?.isEmpty ?? false
                                  ? 'Isi Laporan tidak boleh kosong'
                                  : null,
                            ),
                          ],
                        ),
                      ),
                      if (reportType.id == 1)
                        Container(
                          color: Colors.white,
                          width: double.infinity,
                          margin: EdgeInsets.only(bottom: 14),
                          padding: const EdgeInsets.fromLTRB(14, 24, 14, 24),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Tersangka',
                                style: TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.w500),
                              ),
                              SizedBox(height: 14),
                              SuspectList(
                                religions: data.religions,
                                onSelected: (persons) {
                                  setState(() {
                                    report.suspected = persons;
                                  });
                                  print(report.suspected);
                                },
                              ),
                            ],
                          ),
                        ),
                      Container(
                        color: Colors.white,
                        width: double.infinity,
                        margin: EdgeInsets.only(bottom: 14),
                        padding: const EdgeInsets.fromLTRB(14, 24, 14, 24),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            FormTextField(
                              controller: _evidenceController,
                              label: 'Barang Bukti',
                              hintText: 'Sebutkan barang bukti yang ada',
                              maxLines: 3,
                              readOnly: readOnly,
                              isDense: true,
                              validator: (value) => value?.isEmpty ?? false
                                  ? 'Barang bukti tidak boleh kosong'
                                  : null,
                            ),
                            SizedBox(height: 14),
                            if (reportType.id == 1)
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Foto/Video/Dokumen',
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  SizedBox(height: 6),
                                  AttachmentPicker(onFileSelected: (files) {
                                    setState(() {
                                      report.attachments = files;
                                    });
                                  })
                                ],
                              ),
                          ],
                        ),
                      ),
                      SizedBox(height: 24),
                      Container(
                        width: double.infinity,
                        padding: const EdgeInsets.fromLTRB(14, 0, 14, 24),
                        child: SubmitButton(
                          onPressed: () {
                            if (_formKey.currentState?.validate() ?? false) {
                              var params = report.copyWith(
                                code: _codeController.text,
                                address: _addressController.text,
                                article: _articleController.text,
                                chronology: _chronologyController.text,
                                idCategory: _category?.id,
                                evidence: _evidenceController.text,
                                idShip: ship?.id,
                                officer: _officerController.text,
                                element: element,
                              );
                              context.read<ReportFormCubit>().submitReport(
                                  idReportType: reportType.id, params: params);
                              return;
                            }

                            setState(() {
                              _isValidate = true;
                            });
                          },
                        ),
                      )
                    ],
                  ),
                ),
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }
}
