import 'package:flutter/material.dart';
import 'package:sipolair/models/report_detail.dart';
import 'package:sipolair/screen/helpers/util.dart';
import 'package:sipolair/screen/widgets/empty_widget.dart';
import 'package:timelines/timelines.dart';

class DetailReportStatus extends StatelessWidget {
  final ReportDetail report;

  const DetailReportStatus({Key? key, required this.report}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var listStatus = report.listStatus.reversed.toList();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorUtil.darkBlue,
        title: Text('Detail Status'),
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          width: double.infinity,
          margin: EdgeInsets.only(bottom: 14),
          padding: const EdgeInsets.fromLTRB(14, 24, 14, 24),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Nomor LP',
                      style: TextStyle(),
                    ),
                    SizedBox(width: 8),
                    Expanded(
                      child: Text(
                        report.code,
                        style: TextStyle(fontWeight: FontWeight.w500),
                      ),
                    )
                  ],
                ),
              ),
              Divider(
                color: Colors.black87,
                thickness: 0.2,
              ),
              listStatus.isEmpty
                  ? EmptyWidget(message: 'Tidak ada data')
                  : Timeline.tileBuilder(
                      shrinkWrap: true,
                      padding: EdgeInsets.fromLTRB(16, 16, 16, 24),
                      builder: TimelineTileBuilder(
                        itemCount: listStatus.length,
                        nodePositionBuilder: (context, index) => 0.2,
                        startConnectorBuilder: (context, index) {
                          if (index != 0)
                            return SizedBox(
                                height: 32,
                                child: Connector.solidLine(
                                  color: Colors.black54,
                                  thickness: 0.6,
                                ));
                        },
                        endConnectorBuilder: (context, index) {
                          if (index < listStatus.length - 1)
                            return SizedBox(
                                height: 32,
                                child: Connector.solidLine(
                                  color: Colors.black54,
                                  thickness: 0.6,
                                ));
                        },
                        indicatorBuilder: (context, index) {
                          return DotIndicator(
                              size: 14,
                              color: index == 0 ? ColorUtil.blue : Colors.grey);
                        },
                        oppositeContentsBuilder: (context, index) {
                          String _date = listStatus[index].date ?? '';
                          String _fDate = DateHelper.formatDateTimeFromString(
                              _date, 'd MMM');
                          String _fTime = DateHelper.formatDateTimeFromString(
                              _date, 'HH:mm');
                          return Padding(
                            padding: const EdgeInsets.only(right: 16),
                            child: Text(
                              '$_fDate\n$_fTime',
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                  fontSize: 12,
                                  color:
                                      index == 0 ? Colors.black : Colors.grey),
                            ),
                          );
                        },
                        contentsBuilder: (context, index) {
                          var status = listStatus[index];
                          return Padding(
                            padding: const EdgeInsets.only(left: 8),
                            child: Text(
                              status.name,
                              maxLines: 4,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color: index == 0
                                      ? ColorUtil.darkBlue
                                      : Colors.grey),
                            ),
                          );
                        },
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
