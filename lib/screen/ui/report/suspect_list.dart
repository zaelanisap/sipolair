import 'package:flutter/material.dart';
import 'package:sipolair/models/religion.dart';
import 'package:sipolair/models/report_detail.dart';
import 'package:sipolair/screen/helpers/util.dart';
import 'package:sipolair/screen/ui/report/suspect_form.dart';

class SuspectList extends StatefulWidget {
  const SuspectList({
    Key? key,
    required this.onSelected,
    required this.religions,
  }) : super(key: key);

  final Function(List<Detail> persons) onSelected;
  final List<Religion> religions;

  @override
  _SuspectListState createState() => _SuspectListState();
}

class _SuspectListState extends State<SuspectList> {
  List<Detail> listSuspect = [];
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ...List.generate(
          listSuspect.length,
          (index) {
            var e = listSuspect[index];
            return Container(
              margin: EdgeInsets.only(bottom: 12),
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                color: ColorUtil.lightGrey2,
                borderRadius: BorderRadius.circular(12),
              ),
              child: Row(
                children: [
                  Container(
                    height: 16,
                    width: 20,
                    child: FittedBox(
                      child: Text(
                        '${index + 1}',
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                  SizedBox(width: 16),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: Row(
                                children: [
                                  Text(
                                    e.name,
                                    style:
                                        TextStyle(fontWeight: FontWeight.w500),
                                  ),
                                  SizedBox(width: 4),
                                  Container(
                                    padding: EdgeInsets.fromLTRB(4, 2, 4, 2),
                                    decoration: BoxDecoration(
                                        color: Colors.blue,
                                        borderRadius: BorderRadius.circular(4)),
                                    child: Text(
                                      '${e.age} thn',
                                      style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            PopupMenuButton(
                                padding: EdgeInsets.zero,
                                iconSize: 20,
                                onSelected: (value) async {
                                  switch (value) {
                                    case 1:
                                      var result =
                                          await showSuspectForm(person: e);
                                      if (result != null)
                                        setState(() {
                                          listSuspect.removeAt(index);
                                          listSuspect.insert(index, result);
                                          widget.onSelected(listSuspect);
                                        });

                                      break;
                                    case 2:
                                      setState(() {
                                        listSuspect.removeWhere((x) => x == e);
                                        widget.onSelected(listSuspect);
                                      });
                                      break;
                                  }
                                },
                                itemBuilder: (context) {
                                  return [
                                    PopupMenuItem(
                                      child: Text('Edit'),
                                      value: 1,
                                      textStyle: TextStyle(color: Colors.black),
                                    ),
                                    PopupMenuItem(
                                      child: Text('Hapus'),
                                      value: 2,
                                      textStyle: TextStyle(color: Colors.red),
                                    )
                                  ];
                                })
                          ],
                        ),
                        Text(
                          e.address,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            );
          },
        ),
        GestureDetector(
          onTap: () async {
            Detail? person = await showSuspectForm();
            if (person != null) {
              setState(() {
                listSuspect.add(person);
                widget.onSelected(listSuspect);
              });
            }
          },
          child: Container(
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
              color: Colors.blue.shade400,
              borderRadius: BorderRadius.circular(8),
            ),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(Icons.add, size: 18, color: Colors.white),
                SizedBox(width: 4),
                Text('Tambah tersangka',
                    style: TextStyle(fontSize: 12, color: Colors.white))
              ],
            ),
          ),
        ),
      ],
    );
  }

  Future<Detail?> showSuspectForm({Detail? person}) async {
    return await showModalBottomSheet<Detail?>(
        clipBehavior: Clip.antiAlias,
        enableDrag: false,
        isDismissible: false,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(16))),
        context: context,
        builder: (context) =>
            SuspectForm(person: person, religions: widget.religions));
  }
}
