import 'package:dropdown_sheet/dropdown_sheet.dart';
import 'package:flutter/material.dart';
import 'package:sipolair/models/religion.dart';
import 'package:sipolair/models/report_detail.dart';
import 'package:sipolair/screen/helpers/util.dart';
import 'package:sipolair/screen/widgets/custom_button.dart';
import 'package:sipolair/screen/widgets/dropdown_form.dart';
import 'package:sipolair/screen/widgets/form_text_field.dart';
import 'package:sipolair/screen/widgets/hide_focus.dart';

class SuspectForm extends StatefulWidget {
  const SuspectForm({
    Key? key,
    this.person,
    required this.religions,
  }) : super(key: key);

  final Detail? person;
  final List<Religion> religions;

  @override
  _SuspectFormState createState() => _SuspectFormState();
}

class _SuspectFormState extends State<SuspectForm> {
  final _formKey = GlobalKey<FormState>();
  late TextEditingController _nameController;
  late TextEditingController _ageController;
  late TextEditingController _professionController;
  late TextEditingController _addressController;

  Religion? _religion;

  @override
  void initState() {
    super.initState();
    _nameController = TextEditingController();
    _ageController = TextEditingController();
    _professionController = TextEditingController();
    _addressController = TextEditingController();

    if (widget.person != null) {
      _nameController.text = widget.person!.name;
      _ageController.text = widget.person!.age.toString();
      _professionController.text = widget.person!.profession;
      _addressController.text = widget.person!.address;
      int index =
          widget.religions.indexWhere((e) => e.id == widget.person?.idReligion);
      if (index != -1) _religion = widget.religions[index];
    }
  }

  @override
  void dispose() {
    _nameController.dispose();
    _ageController.dispose();
    _professionController.dispose();
    _addressController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return HideFocus(
      child: Container(
        color: Colors.white,
        padding: EdgeInsets.fromLTRB(
            8, 16, 8, MediaQuery.of(context).padding.bottom),
        height: MediaQuery.of(context).size.height * 0.75,
        child: Stack(
          children: [
            Column(
              children: [
                Text(
                  'Data Tersangka',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                ),
                Divider(height: 32),
                Expanded(
                  child: Form(
                    key: _formKey,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    child: ListView(
                      padding: EdgeInsets.fromLTRB(8, 8, 8, 16),
                      primary: false,
                      children: [
                        FormTextField(
                          label: 'Nama',
                          hintText: 'Nama',
                          isDense: true,
                          controller: _nameController,
                          inputAction: TextInputAction.next,
                          validator: (value) => value?.isEmpty ?? false
                              ? 'Nama tidak boleh kosong'
                              : null,
                        ),
                        SizedBox(height: 12),
                        FormTextField(
                          label: 'Usia',
                          hintText: 'Usia',
                          isDense: true,
                          controller: _ageController,
                          inputType: TextInputType.number,
                          maxLength: 3,
                          inputAction: TextInputAction.next,
                          validator: (value) => value?.isEmpty ?? false
                              ? 'Usia tidak boleh kosong'
                              : null,
                        ),
                        SizedBox(height: 12),
                        FormDropDown<Religion>(
                          label: 'Agama',
                          hintText: 'Pilih Agama Anda',
                          items: widget.religions
                              .map((e) =>
                                  SimpleDialogItem(title: e.name, value: e))
                              .toList(),
                          selectedItem: _religion,
                          onSelected: (value) {
                            setState(() {
                              _religion = value;
                            });
                          },
                          validator: (value) => value?.isEmpty ?? false
                              ? 'Agama tidak boleh kosong'
                              : null,
                        ),
                        SizedBox(height: 12),
                        FormTextField(
                          label: 'Profesi',
                          hintText: 'Profesi',
                          isDense: true,
                          controller: _professionController,
                          inputAction: TextInputAction.next,
                          validator: (value) => value?.isEmpty ?? false
                              ? 'Profesi tidak boleh kosong'
                              : null,
                        ),
                        SizedBox(height: 12),
                        FormTextField(
                          label: 'Alamat',
                          hintText: 'Alamat',
                          isDense: true,
                          controller: _addressController,
                          minLines: 1,
                          maxLines: 3,
                          validator: (value) => value?.isEmpty ?? false
                              ? 'Alamat tidak boleh kosong'
                              : null,
                        ),
                        SizedBox(height: 24),
                        ElevatedButton(
                          onPressed: () {
                            if (_formKey.currentState?.validate() ?? false) {
                              Detail person = Detail(
                                  name: _nameController.text,
                                  age: int.tryParse(_ageController.text) ?? 0,
                                  idReligion: _religion!.id,
                                  profession: _professionController.text,
                                  address: _addressController.text);
                              Navigator.of(context).pop(person);
                            }
                          },
                          style: ElevatedButton.styleFrom(
                              elevation: 0,
                              padding: EdgeInsets.all(12),
                              primary: ColorUtil.darkBlue),
                          child:
                              Text(widget.person == null ? 'Tambah' : 'Ubah'),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
              top: 0,
              right: 0,
              child: CustomButton(
                padding: EdgeInsets.zero,
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Icon(Icons.close),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
