import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:sipolair/cubit/report/cubit.dart';
import 'package:sipolair/repositories/report_repository.dart';
import 'package:sipolair/routes/routes.dart';
import 'package:sipolair/screen/helpers/snackbar_manager.dart';
import 'package:sipolair/screen/helpers/util.dart';
import 'package:sipolair/screen/widgets/attachment_view.dart';
import 'package:sipolair/screen/widgets/custom_button.dart';
import 'package:sipolair/screen/widgets/form_text_field.dart';
import 'package:sipolair/screen/widgets/hide_focus.dart';

class DetailReportPage extends StatefulWidget {
  final int idReport;
  const DetailReportPage({
    Key? key,
    required this.idReport,
  }) : super(key: key);
  @override
  _DetailReportPageState createState() => _DetailReportPageState();
}

class _DetailReportPageState extends State<DetailReportPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return HideFocus(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: ColorUtil.darkBlue,
          title: Text('Detail Laporan'),
        ),
        body: BlocProvider(
          create: (_) => ReportCubit(Get.find<ReportRepository>())
            ..getReportDetail(idReport: widget.idReport),
          child: BlocConsumer<ReportCubit, ReportState>(
            listener: (context, state) {
              if (state is ReportFailure) {
                SnackBarManager.showSnackBar(
                    context: context,
                    message: state.message,
                    backgroundColor: Colors.red);
              }
            },
            builder: (context, state) {
              if (state is ReportDetailLoaded) {
                var report = state.report;

                return RefreshIndicator(
                  onRefresh: () async {
                    context
                        .read<ReportCubit>()
                        .getReportDetail(idReport: widget.idReport);
                  },
                  child: SingleChildScrollView(
                    // padding: const EdgeInsets.fromLTRB(8, 16, 8, 16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          color: Colors.white,
                          width: double.infinity,
                          margin: EdgeInsets.only(bottom: 14),
                          padding: const EdgeInsets.fromLTRB(14, 24, 14, 24),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Nomor LP: ',
                                    style: TextStyle(),
                                  ),
                                  SizedBox(width: 8),
                                  Expanded(
                                    child: Text(
                                      report.code,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Status:',
                                    style: TextStyle(),
                                  ),
                                  Expanded(
                                    child: Container(
                                      padding: EdgeInsets.all(8),
                                      child: Text(
                                        report.listStatus.isNotEmpty
                                            ? report.listStatus.last.name
                                            : '-',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 24),
                                  CustomButton(
                                    onTap: () {
                                      Navigator.of(context).pushNamed(
                                          Routes.DETAILREPORTSTATUSPAGE,
                                          arguments: report);
                                    },
                                    child: Text(
                                      'Lihat',
                                      style:
                                          TextStyle(color: ColorUtil.darkBlue),
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        Container(
                          color: Colors.white,
                          width: double.infinity,
                          margin: EdgeInsets.only(bottom: 14),
                          padding: const EdgeInsets.fromLTRB(14, 24, 14, 24),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              FormTextField(
                                initialValue: report.ship?.name ?? '-',
                                label: 'Kapal',
                                hintText: 'Nama Kapal',
                                isDense: true,
                                readOnly: true,
                              ),
                              SizedBox(height: 14),
                              FormTextField(
                                initialValue: report.officer,
                                label: 'Personil',
                                hintText: 'Nama Personil',
                                isDense: true,
                                readOnly: true,
                              ),
                            ],
                          ),
                        ),
                        Container(
                          color: Colors.white,
                          width: double.infinity,
                          margin: EdgeInsets.only(bottom: 14),
                          padding: const EdgeInsets.fromLTRB(14, 24, 14, 24),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              FormTextField(
                                initialValue:
                                    DateHelper.formatDateTimeFromString(
                                        report.date, 'dd MMMM yyyy'),
                                label: 'Tanggal Kejadian',
                                hintText: 'Pilih tanggal kejadian',
                                suffixIcon: Icon(Icons.date_range),
                                isDense: true,
                                readOnly: true,
                              ),
                              SizedBox(height: 14),
                              FormTextField(
                                initialValue: report.time,
                                label: 'Waktu Kejadian',
                                hintText: 'Pilih Waktu kejadian',
                                suffixIcon: Icon(Icons.access_time),
                                isDense: true,
                                readOnly: true,
                              ),
                              SizedBox(height: 14),
                              if (report.category != null)
                                FormTextField(
                                  initialValue: report.category?.name,
                                  label: 'Dugaan Pelanggaran',
                                  hintText: 'Dugaan Pelanggaran',
                                  isDense: true,
                                  readOnly: true,
                                  minLines: 1,
                                  maxLines: 6,
                                ),
                              SizedBox(height: 14),
                              FormTextField(
                                initialValue:
                                    report.zone?.name ?? 'Belum diatur',
                                label: 'Zona',
                                hintText: 'Zona',
                                isDense: true,
                                readOnly: true,
                                minLines: 1,
                                maxLines: 6,
                              ),
                              SizedBox(height: 14),
                              FormTextField(
                                initialValue: report.tkp,
                                label: 'Alamat TKP',
                                hintText: 'Tempat kejadian',
                                isDense: true,
                                readOnly: true,
                                minLines: 1,
                                maxLines: 6,
                              ),
                              SizedBox(height: 14),
                              FormTextField(
                                initialValue: report.article,
                                label: 'Pasal',
                                hintText: 'Pasal Pidana',
                                readOnly: true,
                                isDense: true,
                                inputAction: TextInputAction.next,
                              ),
                              SizedBox(height: 14),
                              FormTextField(
                                initialValue: report.chronology,
                                label: 'Isi Laporan',
                                hintText: 'Jelaskan Isi Laporan',
                                isDense: true,
                                readOnly: true,
                                minLines: 1,
                                maxLines: 10,
                              ),
                            ],
                          ),
                        ),
                        if (report.details.isNotEmpty)
                          Container(
                            color: Colors.white,
                            width: double.infinity,
                            margin: EdgeInsets.only(bottom: 14),
                            padding: const EdgeInsets.fromLTRB(14, 24, 14, 24),
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Tersangka',
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  SizedBox(height: 14),
                                  if (report.details.isNotEmpty)
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: List.generate(
                                        report.details.length,
                                        (index) {
                                          var e = report.details[index];
                                          return Container(
                                            margin: EdgeInsets.only(bottom: 8),
                                            padding: EdgeInsets.all(8),
                                            decoration: BoxDecoration(
                                                color: ColorUtil.lightGrey,
                                                borderRadius:
                                                    BorderRadius.circular(8)),
                                            child: Row(
                                              children: [
                                                Container(
                                                  height: 16,
                                                  width: 20,
                                                  child: FittedBox(
                                                    child: Text(
                                                      '${index + 1}',
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.w600),
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(width: 16),
                                                Expanded(
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Expanded(
                                                            child: Row(
                                                              children: [
                                                                Text(
                                                                  e.name,
                                                                  style: TextStyle(
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w500),
                                                                ),
                                                                SizedBox(
                                                                    width: 4),
                                                                Container(
                                                                  padding: EdgeInsets
                                                                      .fromLTRB(
                                                                          4,
                                                                          2,
                                                                          4,
                                                                          2),
                                                                  decoration: BoxDecoration(
                                                                      color: Colors
                                                                          .blue,
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              4)),
                                                                  child: Text(
                                                                    '${e.age} thn',
                                                                    style: TextStyle(
                                                                        fontSize:
                                                                            10,
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w500,
                                                                        color: Colors
                                                                            .white),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      Text(
                                                        e.address,
                                                        maxLines: 2,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          );
                                        },
                                      ),
                                    )
                                  else
                                    Text('Tidak ada'),
                                ],
                              ),
                            ),
                          ),
                        Container(
                          color: Colors.white,
                          width: double.infinity,
                          margin: EdgeInsets.only(bottom: 14),
                          padding: const EdgeInsets.fromLTRB(14, 24, 14, 24),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              FormTextField(
                                initialValue: report.evidence,
                                label: 'Barang Bukti',
                                hintText: 'Sebutkan barang bukti yang ada',
                                minLines: 1,
                                maxLines: 6,
                                readOnly: true,
                                isDense: true,
                              ),
                              SizedBox(height: 14),
                              AttachmentView(files: report.files)
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              }
              return Center(child: CircularProgressIndicator());
            },
          ),
        ),
      ),
    );
  }
}
