import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:sipolair/cubit/report/cubit.dart';
import 'package:sipolair/cubit/report_form/cubit.dart';
import 'package:sipolair/models/report.dart';
import 'package:sipolair/routes/routes.dart';
import 'package:sipolair/screen/helpers/modal_manager.dart';
import 'package:sipolair/screen/helpers/snackbar_manager.dart';
import 'package:sipolair/screen/helpers/util.dart';
import 'package:sipolair/screen/widgets/custom_button.dart';
import 'package:sipolair/screen/widgets/empty_widget.dart';
import 'package:sipolair/screen/widgets/filtered_datepicker.dart';
import 'package:sipolair/screen/widgets/form_text_field.dart';

class ReportListPage extends StatefulWidget {
  const ReportListPage({Key? key}) : super(key: key);

  @override
  _ReportListPageState createState() => _ReportListPageState();
}

class _ReportListPageState extends State<ReportListPage> {
  late TextEditingController _searchController;
  String? _startDate, _endDate;

  @override
  void initState() {
    super.initState();
    _searchController = TextEditingController();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        context.read<ReportCubit>().getReports(
            keyword: _searchController.text,
            startDate: _startDate,
            endDate: _endDate);
      },
      child: BlocBuilder<ReportCubit, ReportState>(
        buildWhen: (previous, current) => current is ReportsLoaded,
        builder: (context, state) {
          if (state is ReportsLoaded) {
            var syncReports = state.reportList.syncReports;
            var localReports = state.reportList.localReports;
            bool isEmpty = syncReports.isEmpty && localReports.isEmpty;
            return CustomScrollView(
              slivers: [
                if (localReports.isNotEmpty)
                  SliverToBoxAdapter(
                    child: Container(
                      color: ColorUtil.lightOrange,
                      padding: EdgeInsets.fromLTRB(8, 12, 8, 12),
                      child: Row(
                        children: [
                          CustomButton(
                            onTap: () {},
                            child: Icon(Icons.info_outline, size: 18),
                          ),
                          SizedBox(width: 4),
                          Expanded(
                            child: Text(
                                'Beberapa laporan penegakkan hukum perlu disinkronisasi'),
                          ),
                        ],
                      ),
                    ),
                  ),
                SliverToBoxAdapter(
                  child: ExpansionTile(
                    maintainState: true,
                    title: Text('Filter'),
                    trailing: Icon(Icons.filter_list),
                    children: [searchInput(context), filterDate()],
                  ),
                ),
                if (isEmpty)
                  SliverFillRemaining(
                      child: EmptyWidget(message: 'Tidak ada laporan')),
                if (localReports.isNotEmpty) ...[
                  SliverToBoxAdapter(
                    child: Container(
                      padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Perlu Sinkronisasi',
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w500),
                          ),
                          BlocProvider(
                            create: (_) => ReportFormCubit(Get.find()),
                            child: Builder(
                              builder: (ctx) => BlocListener<ReportFormCubit,
                                  ReportFormState>(
                                listener: (ctx, state) {
                                  if (state is ReportFormSubmitted) {
                                    Navigator.of(context).pop();
                                    context.read<ReportCubit>().getReports();
                                  } else if (state is ReportSubmitFailure) {
                                    Navigator.of(context).pop();
                                    SnackBarManager.showSnackBar(
                                        context: context,
                                        message: state.message,
                                        backgroundColor: Colors.red);
                                  } else if (state is ReportSubmitLoading) {
                                    ModalManager.showLoadingIndicator(context);
                                  }
                                },
                                child: CustomButton(
                                  onTap: () {
                                    ctx.read<ReportFormCubit>().syncReports();
                                  },
                                  child: Text(
                                    'Perbarui',
                                    style: TextStyle(
                                        fontSize: 13,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  SliverPadding(
                    padding: EdgeInsets.fromLTRB(8, 8, 8, 16),
                    sliver: SliverList(
                        delegate: SliverChildBuilderDelegate(
                      (context, index) {
                        var report = localReports[index];
                        return Padding(
                          padding: const EdgeInsets.only(bottom: 12),
                          child: itemReport(report),
                        );
                      },
                      childCount: localReports.length,
                    )),
                  ),
                ],
                if (syncReports.isNotEmpty)
                  SliverPadding(
                    padding: EdgeInsets.fromLTRB(8, 16, 8, 16),
                    sliver: SliverList(
                        delegate: SliverChildBuilderDelegate(
                      (context, index) {
                        var report = syncReports[index];
                        return Padding(
                            padding: const EdgeInsets.only(bottom: 12),
                            child: itemReport(report));
                      },
                      childCount: syncReports.length,
                    )),
                  ),
              ],
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  Widget searchInput(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 16, 8, 16),
      child: CustomTextField(
        controller: _searchController,
        backgroundColor: Colors.white,
        hintText: 'Cari laporan...',
        inputAction: TextInputAction.search,
        isDense: true,
        prefixIcon: Icon(Icons.search),
        onChanged: (value) {
          context.read<ReportCubit>().getReports(
              keyword: value, startDate: _startDate, endDate: _endDate);
        },
      ),
    );
  }

  Widget filterDate() {
    return Builder(
      builder: (context) => Align(
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: EdgeInsets.only(left: 8, bottom: 16),
          child: FilterDatePicker(onSelected: (dateTimeRange) {
            setState(() {
              _startDate = dateTimeRange != null
                  ? DateHelper.formatDateTime(dateTimeRange.start, 'yyyy-MM-dd')
                  : null;
              _endDate = dateTimeRange != null
                  ? DateHelper.formatDateTime(dateTimeRange.end, 'yyyy-MM-dd')
                  : null;
            });

            context.read<ReportCubit>().getReports(
                keyword: _searchController.text,
                startDate: _startDate,
                endDate: _endDate);
          }),
        ),
      ),
    );
  }

  Widget itemReport(Report report) {
    return GestureDetector(
      onTap: () {
        if (report.isSync)
          Navigator.of(context)
              .pushNamed(Routes.DETAILREPORTPAGE, arguments: report.id);
      },
      child: Container(
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
        ),
        child: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 40),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 40),
                    child: Text(
                      report.code ?? '-',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 13,
                      ),
                    ),
                  ),
                  SizedBox(height: 6),
                  Text(report.category?.name ?? '-'),
                  SizedBox(height: 2),
                  Text('Alamat TKP :  ' + report.tkp),
                  SizedBox(height: 16),
                  Text(
                      '${DateHelper.mappingDateTime(report.date)} ${report.time.substring(0, 5)}'),
                ],
              ),
            ),
            Positioned(
              top: 0,
              right: 0,
              child: Container(
                width: 112,
                height: 20,
                padding: EdgeInsets.fromLTRB(8, 2, 8, 2),
                decoration: BoxDecoration(
                    color: Colors.amber.shade100,
                    borderRadius: BorderRadius.circular(4)),
                child: FittedBox(
                  child: Text(
                    report.status?.name ?? '-',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                ),
              ),
            ),
            if (report.isSync)
              Positioned(
                right: 0,
                bottom: 0,
                child: Container(
                    width: 32,
                    height: 28,
                    decoration: BoxDecoration(
                        // color: Colors.blue.shade200,
                        border: Border.all(color: Colors.black54, width: 0.4),
                        borderRadius: BorderRadius.circular(4)),
                    child:
                        Icon(Icons.chevron_right_rounded, color: Colors.black)),
              )
          ],
        ),
      ),
    );
  }
}
