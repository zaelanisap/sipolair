import 'package:sipolair/models/gender.dart';

class User {
  User(
      {required this.id,
      this.username,
      this.email,
      this.account,
      this.idRole,
      this.idAgency,
      this.idZone,
      this.avatar});

  final int id;
  final String? username;
  final String? email;
  final Account? account;
  final int? idRole;
  final int? idAgency;
  final int? idZone;
  final String? avatar;

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        username: json["username"],
        email: json["email"],
        idRole: json["id_role"],
        idAgency: json["id_agency"],
        idZone: json["id_zone"],
        avatar: json["profile_photo_url"],
        account:
            json["account"] != null ? Account.fromJson(json["account"]) : null,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "username": username,
        "email": email,
        "id_role": idRole,
        "id_agency": idAgency,
        "id_zone": idZone,
        "profile_photo_url": avatar,
        "account": account?.toJson(),
      };
}

class Account {
  Account({
    required this.id,
    required this.idUser,
    required this.gender,
    required this.name,
    required this.address,
    required this.identityNum,
    required this.phone,
    this.avatar,
  });

  final int id;
  final int idUser;
  final Gender gender;
  final String name;
  final String address;
  final String identityNum;
  final String phone;
  final String? avatar;

  factory Account.fromJson(Map<String, dynamic> json) => Account(
        id: json["id"],
        idUser: json["id_user"],
        gender: Gender.fromJson(json["gender"]),
        name: json["name"],
        address: json["address"],
        identityNum: json["identity_num"],
        phone: json["phone"],
        avatar: json["avatar"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_user": idUser,
        "gender": gender.toJson(),
        "name": name,
        "address": address,
        "identity_num": identityNum,
        "phone": phone,
        "avatar": avatar,
      };
}
