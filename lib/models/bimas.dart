import 'package:sipolair/models/report_detail.dart';

import 'zone.dart';

class Bimas {
  Bimas(
      {required this.id,
      required this.title,
      required this.contents,
      required this.pic,
      required this.location,
      required this.totalParticipant,
      required this.date,
      required this.time,
      required this.timezone,
      required this.zone,
      this.files});

  final int id;
  final String title;
  final String contents;
  final String pic;
  final String location;
  final int totalParticipant;
  final String date;
  final String time;
  final String timezone;
  final Zone? zone;
  final List<FileReport>? files;

  factory Bimas.fromJson(Map<String, dynamic> json) => Bimas(
        id: json["id"],
        title: json["title"],
        contents: json["contents"],
        pic: json["pic"],
        location: json["location"],
        totalParticipant: json["num_of_participants"],
        date: json["date"],
        time: json["time"],
        timezone: json["timezone"],
        zone: json["zone"] != null ? Zone.fromJson(json["zone"]) : null,
        files: json["files"] != null
            ? List<FileReport>.from(
                json["files"].map((x) => FileReport.fromJson(x)))
            : null,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "contents": contents,
        "pic": pic,
        "location": location,
        "num_of_participants": totalParticipant,
        "date": date,
        "time": time,
        "timezone": timezone,
        "zone": zone?.toJson(),
        "files": files != null
            ? List<dynamic>.from(files!.map((x) => x.toJson()))
            : null
      };
}
