import 'package:equatable/equatable.dart';

import 'package:sipolair/models/criminal_category.dart';
import 'package:sipolair/models/zone.dart';

import 'report_detail.dart';

class ReportList {
  final List<Report> syncReports;
  final List<Report> localReports;
  ReportList({
    required this.syncReports,
    required this.localReports,
  });
}

class Report extends Equatable {
  Report(
      {required this.id,
      required this.idUser,
      required this.idShip,
      required this.officer,
      required this.zone,
      required this.category,
      this.code,
      required this.tkp,
      required this.chronology,
      required this.evidence,
      required this.article,
      required this.date,
      required this.time,
      this.status});

  final int id;
  final int idUser;
  final int? idShip;
  final String officer;
  final Zone? zone;
  final CriminalCategory? category;
  final String? code;
  final String tkp;
  final String chronology;
  final String evidence;
  final String article;
  final String date;
  final String time;
  final Status? status;

  bool get isSync => id != 0;

  factory Report.fromJson(Map<String, dynamic> json) => Report(
        id: json["id"],
        idUser: json["id_user"],
        idShip: json['id_ship'],
        officer: json['officer'],
        zone: json["zone"] != null ? Zone.fromJson(json["zone"]) : null,
        category: json["criminal_category"] != null
            ? CriminalCategory.fromJson(json["criminal_category"])
            : null,
        code: json["code"],
        tkp: json["tkp"],
        chronology: json["chronology"],
        evidence: json["evidence"],
        article: json['article'],
        date: json["date"],
        time: json["time"],
        status: json["status"] != null
            ? Status.fromJson(json["status"]["status"][0])
            : null,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_user": idUser,
        "id_ship": idShip,
        "officer": officer,
        "zone": zone?.toJson(),
        "criminal_category": category?.toJson(),
        "code": code,
        "tkp": tkp,
        "chronology": chronology,
        "evidence": evidence,
        "article": article,
        "date": date,
        "time": time,
        "status": status?.toJson(),
      };

  @override
  List<Object?> get props => [
        id,
        idUser,
        idShip,
        officer,
        zone,
        category,
        code,
        tkp,
        chronology,
        evidence,
        article,
        date,
        time,
        status
      ];
}
