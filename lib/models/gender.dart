class Gender {
  final int idGender;
  final String gender;
  Gender({
    required this.idGender,
    required this.gender,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': idGender,
      'name': gender,
    };
  }

  factory Gender.fromJson(Map<String, dynamic> map) {
    return Gender(
      idGender: map['id'],
      gender: map['name'],
    );
  }

  @override
  String toString() => 'Gender(idGender: $idGender, gender: $gender)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Gender &&
        other.idGender == idGender &&
        other.gender == gender;
  }

  @override
  int get hashCode => idGender.hashCode ^ gender.hashCode;
}
