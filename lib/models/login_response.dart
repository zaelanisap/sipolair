class LoginResponse {
  final int idUser;
  final String token;
  LoginResponse({
    required this.idUser,
    required this.token,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': idUser,
      'access_token': token,
    };
  }

  factory LoginResponse.fromJson(Map<String, dynamic> map) {
    return LoginResponse(
      idUser: map['id'],
      token: map['access_token'],
    );
  }

  @override
  String toString() => 'LoginResponse(idUser: $idUser, token: $token)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is LoginResponse &&
        other.idUser == idUser &&
        other.token == token;
  }

  @override
  int get hashCode => idUser.hashCode ^ token.hashCode;
}
