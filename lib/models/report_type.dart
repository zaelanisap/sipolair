import 'package:equatable/equatable.dart';

class ReportType extends Equatable {
  final int id;
  final String name;
  ReportType({
    required this.id,
    required this.name,
  });

  static List<ReportType> get reportTypes => [
        ReportType(id: 1, name: 'Laporan Pengaduan'),
        ReportType(id: 2, name: 'Laporan Nihil'),
      ];

  @override
  List<Object?> get props => [id, name];
}
