import '../core/utils/exception.dart';

class BaseResponse<T> {
  T? _data;
  AppException? _exception;

  set setData(T data) {
    this._data = data;
  }

  set setException(AppException exception) {
    this._exception = exception;
  }

  T? get data => _data;

  AppException? get getException => _exception;
}
