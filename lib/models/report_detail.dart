import 'package:sipolair/models/criminal_category.dart';
import 'package:sipolair/models/report.dart';
import 'package:sipolair/models/ship.dart';

import 'zone.dart';

class ReportDetail extends Report {
  ReportDetail(
      {required this.id,
      required this.idUser,
      this.idShip,
      this.ship,
      required this.officer,
      required this.zone,
      required this.category,
      required this.code,
      required this.tkp,
      required this.chronology,
      required this.evidence,
      required this.article,
      required this.date,
      required this.time,
      required this.details,
      required this.listStatus,
      required this.files})
      : super(
          id: id,
          idUser: idUser,
          zone: zone,
          category: category,
          code: code,
          tkp: tkp,
          chronology: chronology,
          evidence: evidence,
          article: article,
          date: date,
          time: time,
          idShip: idShip,
          officer: officer,
        );

  final int id;
  final int idUser;
  final int? idShip;
  final String officer;
  final Ship? ship;
  final Zone? zone;
  final CriminalCategory? category;
  final String code;
  final String tkp;
  final String chronology;
  final String evidence;
  final String article;
  final String date;
  final String time;
  final List<Detail> details;
  final List<Status> listStatus;
  final List<FileReport> files;

  factory ReportDetail.fromJson(Map<String, dynamic> json) => ReportDetail(
        id: json["id"],
        idUser: json["id_user"],
        idShip: json["id_ship"],
        ship: json["ship"] != null ? Ship.fromJson(json["ship"]) : null,
        officer: json["officer"],
        zone: json["zone"] != null ? Zone.fromJson(json["zone"]) : null,
        category: json["criminal_category"] != null
            ? CriminalCategory.fromJson(json["criminal_category"])
            : null,
        code: json["code"],
        tkp: json["tkp"],
        chronology: json["chronology"],
        evidence: json["evidence"],
        date: json["date"],
        time: json["time"],
        article: json["article"],
        details:
            List<Detail>.from(json["details"].map((x) => Detail.fromJson(x))),
        listStatus:
            List<Status>.from(json["status"].map((x) => Status.fromJson(x))),
        files: List<FileReport>.from(
            json["files"].map((x) => FileReport.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_user": idUser,
        "ship": ship?.toJson(),
        "id_ship": idShip,
        "officer": officer,
        "zone": zone?.toJson(),
        "criminal_category": category?.toJson(),
        "code": code,
        "tkp": tkp,
        "chronology": chronology,
        "evidence": evidence,
        "date": date,
        "time": time,
        "details": List<dynamic>.from(details.map((x) => x.toJson())),
        "status": List<dynamic>.from(listStatus.map((x) => x.toJson())),
      };
}

class Detail {
  Detail({
    this.id,
    this.idReport,
    required this.idReligion,
    required this.name,
    required this.age,
    required this.address,
    required this.profession,
  });

  final int? id;
  final int? idReport;
  final int idReligion;
  final String name;
  final int age;
  final String address;
  final String profession;

  factory Detail.fromJson(Map<String, dynamic> json) => Detail(
        id: json["id"],
        idReport: json["id_report"],
        idReligion: json["id_religion"],
        name: json["name"],
        age: json["age"],
        address: json["address"],
        profession: json["profession"],
      );

  Map<String, dynamic> toJson() => {
        "id_religion": idReligion,
        "name": name,
        "age": age,
        "address": address,
        "profession": profession,
      };
}

class Status {
  Status({this.id, required this.name, this.date});

  final int? id;
  final String name;
  final String? date;

  factory Status.fromJson(Map<String, dynamic> json) => Status(
        id: json["id"],
        name: json["name"],
        date: json["created_at"],
      );

  Map<String, dynamic> toJson() => {"id": id, "name": name, "created_at": date};
}

class FileReport {
  String fileName;
  String path;
  FileReport({
    required this.fileName,
    required this.path,
  });

  Map<String, dynamic> toJson() {
    return {
      'fileName': fileName,
      'path': path,
    };
  }

  factory FileReport.fromJson(Map<String, dynamic> map) {
    return FileReport(
      fileName: map['filename'],
      path: map['path'],
    );
  }
}
