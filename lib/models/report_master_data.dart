import 'package:equatable/equatable.dart';

import 'package:sipolair/models/criminal_category.dart';
import 'package:sipolair/models/patrol.dart';
import 'package:sipolair/models/religion.dart';
import 'package:sipolair/models/ship.dart';
import 'package:sipolair/models/zone.dart';

class ReportMasterData extends Equatable {
  final List<Religion> religions;
  final List<CriminalCategory> categories;
  final List<Ship> ships;
  ReportMasterData(
      {required this.religions, required this.categories, required this.ships});

  @override
  List<Object?> get props =>
      [List.from(religions), List.from(categories), List.from(ships)];
}

class PatrolReportMasterData extends Equatable {
  final List<Patrol> patrols;
  final List<Zone> zones;
  final List<Ship> ships;
  PatrolReportMasterData({
    required this.patrols,
    required this.zones,
    required this.ships,
  });

  @override
  List<Object?> get props => [patrols, zones, ships];
}
