import 'package:sipolair/models/report_detail.dart';
import 'package:sipolair/models/zone.dart';

import 'patrol.dart';
import 'ship.dart';

class PatrolReport {
  PatrolReport(
      {required this.id,
      required this.idShip,
      required this.idZone,
      required this.idYear,
      required this.idPatrol,
      required this.contents,
      required this.location,
      required this.date,
      required this.startTime,
      required this.endTime,
      required this.totalTime,
      required this.isActive,
      required this.ship,
      required this.zone,
      required this.year,
      required this.patrol,
      this.files});

  final int id;
  final int? idShip;
  final int? idZone;
  final int? idYear;
  final int? idPatrol;
  final String contents;
  final String location;
  final String date;
  final String startTime;
  final String endTime;
  final double totalTime;
  final int isActive;
  final Ship ship;
  final Zone? zone;
  final String year;
  final Patrol patrol;
  final List<FileReport>? files;

  factory PatrolReport.fromJson(Map<String, dynamic> json) => PatrolReport(
        id: json["id"],
        idShip: json["id_ship"],
        idZone: json["id_zone"],
        idYear: json["id_year"],
        idPatrol: json["id_patrol"],
        contents: json["contents"] ?? '-',
        location: json["location"] ?? '-',
        date: json["date"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        totalTime: json["total_time"].toDouble(),
        isActive: json["is_active"],
        ship: Ship.fromJson(json["ship"]),
        zone: json["zone"] != null ? Zone.fromJson(json["zone"]) : null,
        year: json["year"]["name"],
        patrol: Patrol.fromJson(json["patrol"]),
        files: json["files"] != null
            ? List<FileReport>.from(
                json["files"].map((x) => FileReport.fromJson(x)))
            : null,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_ship": idShip,
        "id_zone": idZone,
        "id_year": idYear,
        "id_patrol": idPatrol,
        "contents": contents,
        "location": location,
        "date": date,
        "start_time": startTime,
        "end_time": endTime,
        "total_time": totalTime,
        "is_active": isActive,
        "ship": ship.toJson(),
        "zone": zone?.toJson(),
        "year": year,
        "patrol": patrol.toJson(),
        "files": files != null
            ? List<dynamic>.from(files!.map((x) => x.toJson()))
            : null
      };
}
