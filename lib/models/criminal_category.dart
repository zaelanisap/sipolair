class CriminalCategory {
  CriminalCategory({
    required this.id,
    required this.name,
    required this.description,
  });

  final int id;
  final String name;
  final String description;

  factory CriminalCategory.fromJson(Map<String, dynamic> json) =>
      CriminalCategory(
        id: json["id"],
        name: json["name"],
        description: json["description"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
      };
}
