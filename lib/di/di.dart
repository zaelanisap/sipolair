import 'package:connectivity/connectivity.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:sipolair/core/network/network_info.dart';
import 'package:sipolair/core/utils/pref_utils.dart';
import 'package:sipolair/datasources/authentication_source.dart';
import 'package:sipolair/datasources/bimas_source.dart';
import 'package:sipolair/datasources/file_source.dart';
import 'package:sipolair/datasources/master_data_source.dart';
import 'package:sipolair/datasources/patrol_source.dart';
import 'package:sipolair/datasources/profile_source.dart';
import 'package:sipolair/datasources/report_local_source.dart';
import 'package:sipolair/datasources/report_source.dart';
import 'package:sipolair/repositories/authentication_repository.dart';
import 'package:sipolair/repositories/bimas_repository.dart';
import 'package:sipolair/repositories/patrol_repository.dart';
import 'package:sipolair/repositories/profile_repository.dart';
import 'package:sipolair/repositories/report_repository.dart';
import 'package:sipolair/routes/route_generator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sipolair/screen/notification/notification_manager.dart';

Future<void> initService() async {
  //Local storage
  await Get.putAsync(() async {
    var _pref = await SharedPreferences.getInstance();
    return _pref;
  }, permanent: true);
  Get.put(GetStorage(), permanent: true);

  //Connection Checker
  Get.put(Connectivity(), permanent: true);
  Get.lazyPut(() => NetworkInfoImpl(Get.find()));

  Get.put(PrefUtil(), permanent: true);
  Get.put(RouteGenerator(), permanent: true);

  Get.put<FirebaseMessaging>(FirebaseMessaging.instance, permanent: true);
  Get.put<FlutterLocalNotificationsPlugin>(FlutterLocalNotificationsPlugin(),
      permanent: true);
  Get.put(NotificationManager(
    localNotificationsPlugin: Get.find<FlutterLocalNotificationsPlugin>(),
    channel: AndroidNotificationChannel(
        'sippolair_notif', 'SIPPOLAIR', 'SIPPOLAIR NOTIFICATION',
        importance: Importance.high, enableVibration: true, enableLights: true),
  ));

  Get.lazyPut<AuthenticationSource>(() => AuthenticationSource());
  Get.lazyPut<AuthenticationRepository>(() => AuthenticationRepository(
      authSource: Get.find<AuthenticationSource>(),
      profileSource: Get.find<ProfileSource>(),
      firebaseMessaging: Get.find<FirebaseMessaging>()));

  Get.lazyPut<ProfileSource>(() => ProfileSource());
  Get.lazyPut<ProfileRepository>(
      () => ProfileRepository(remoteSource: Get.find<ProfileSource>()));

  Get.lazyPut<FileSource>(() => FileSource());
  Get.lazyPut<MasterDataSource>(() => MasterDataSource());

  Get.lazyPut<ReportGetStorageSource>(
      () => ReportGetStorageSource(storage: Get.find<GetStorage>()));
  Get.lazyPut<ReportSource>(() => ReportSource());
  Get.lazyPut<ReportRepository>(() => ReportRepository(
      fileSource: Get.find(),
      remoteSource: Get.find(),
      localSource: Get.find<ReportGetStorageSource>(),
      networkInfo: Get.find<NetworkInfoImpl>()));

  Get.lazyPut<BimasSource>(() => BimasSource());
  Get.lazyPut<BimasRepository>(() => BimasRepository(
      fileSource: Get.find(),
      masterDataSource: Get.find(),
      networkInfo: Get.find<NetworkInfoImpl>(),
      remoteSource: Get.find()));

  Get.lazyPut<PatrolSource>(() => PatrolSource());
  Get.lazyPut<PatrolRepository>(() => PatrolRepository(
      remoteSource: Get.find(),
      fileSource: Get.find(),
      masterDataSource: Get.find(),
      networkInfo: Get.find<NetworkInfoImpl>()));
}
