import 'package:sipolair/core/network/network_info.dart';
import 'package:sipolair/core/params/patrol_params.dart';
import 'package:sipolair/core/utils/exception.dart';
import 'package:sipolair/core/utils/pref_utils.dart';
import 'package:sipolair/datasources/file_source.dart';
import 'package:sipolair/datasources/master_data_source.dart';
import 'package:sipolair/datasources/patrol_source.dart';
import 'package:sipolair/models/base_response.dart';
import 'package:sipolair/models/patrol_report.dart';
import 'package:sipolair/models/report_master_data.dart';

class PatrolRepository {
  final PatrolSource remoteSource;
  final FileSource fileSource;
  final MasterDataSource masterDataSource;
  // final ReportLocalDataSource localSource;
  final NetworkInfo networkInfo;

  PatrolRepository({
    required this.remoteSource,
    required this.fileSource,
    required this.masterDataSource,
    // required this.localSource,
    required this.networkInfo,
  });

  Future<BaseResponse<List<PatrolReport>>> getListReport(
      {int? idPatrol, int? idZone, String? startDate, String? endDate}) async {
    int? _idZone =
        PrefUtil.instance.idAgency == 6 ? PrefUtil.instance.idZone : idZone;

    if (await networkInfo.isConnected) {
      try {
        var response = await remoteSource.getListReport(
            idPatrol: idPatrol,
            idZone: _idZone,
            startDate: startDate,
            endDate: endDate);

        return BaseResponse()..setData = response;
      } on AppException catch (e) {
        return BaseResponse()..setException = e;
      }
    } else {
      // var localReports = await localSource.getReports();
      return BaseResponse()..setData = [];
    }
  }

  Future<BaseResponse<PatrolReport>> getDetailReport({required int id}) async {
    try {
      var response = await remoteSource.getDetailReport(id: id);

      return BaseResponse()..setData = response;
    } on AppException catch (e) {
      return BaseResponse()..setException = e;
    }
  }

  Future<BaseResponse<PatrolReportMasterData>> getReportMasterData() async {
    if (await networkInfo.isConnected) {
      try {
        var patrols = await masterDataSource.getPatrols();
        var zones = await masterDataSource.getZones();
        var ships = await masterDataSource.getShips();

        return BaseResponse()
          ..setData = PatrolReportMasterData(
              patrols: patrols, zones: zones, ships: ships);
      } on AppException catch (e) {
        return BaseResponse()..setException = e;
      }
    } else {
      // var localReports = await localSource.getReports();
      return BaseResponse()
        ..setData = PatrolReportMasterData(patrols: [], zones: [], ships: []);
    }
  }

  Future<BaseResponse<bool>> submitReport(
      {required PatrolParams params}) async {
    // if (await networkInfo.isConnected) {
    try {
      var idReport = await remoteSource.submitReport(params: params);

      bool response = true;

      await Future.forEach<String>(
        params.files ?? [],
        (e) async {
          var responseUpload = await fileSource.uploadFile(
              file: e, idReference: idReport, menu: 'patrol');
          response = responseUpload;
        },
      );

      // params.files?.forEach((file) async {
      //   var responseUpload = await fileSource.uploadFile(
      //       file: file, idReference: idReport, menu: 'binmas');
      //   response = responseUpload;
      // });

      return BaseResponse()..setData = response;
    } on AppException catch (e) {
      return BaseResponse()..setException = e;
    }
    // } else {
    //   await localSource
    //       .submitReport(params.copyWith(idUser: PrefUtil.instance.idUser));
    //   return BaseResponse()..setData = false;
    // }
  }
}
