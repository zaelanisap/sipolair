import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:sipolair/core/utils/pref_utils.dart';
import 'package:sipolair/datasources/profile_source.dart';
import 'package:sipolair/models/user.dart';
import '../core/params/login_params.dart';
import '../core/params/register_params.dart';
import '../core/utils/exception.dart';
import '../datasources/authentication_source.dart';
import '../models/base_response.dart';

class AuthenticationRepository {
  final AuthenticationSource authSource;
  final ProfileSource profileSource;
  final FirebaseMessaging firebaseMessaging;
  AuthenticationRepository(
      {required this.authSource,
      required this.profileSource,
      required this.firebaseMessaging});

  Future<BaseResponse<bool>> register(RegisterParams params) async {
    try {
      var response = await authSource.register(params);

      return BaseResponse()..setData = response;
    } on AppException catch (e) {
      return BaseResponse()..setException = e;
    }
  }

  Future<BaseResponse<User>> login(LoginParams params) async {
    try {
      var response = await authSource.login(params);
      PrefUtil.instance.saveToken(response.token);
      PrefUtil.instance.saveUserData(User(id: response.idUser));

      var user = await profileSource.getProfile();
      PrefUtil.instance.saveUserData(user);

      String? token = await firebaseMessaging.getToken();

      await profileSource.updateFCMToken(token);

      return BaseResponse()..setData = user;
    } on AppException catch (e) {
      return BaseResponse()..setException = e;
    }
  }

  Future<BaseResponse<bool>> logout() async {
    try {
      var response = await authSource.logout();
      if (response == true) {
        firebaseMessaging.deleteToken();
        PrefUtil.instance.clearLogin();
      }
      return BaseResponse()..setData = response;
    } on AppException catch (e) {
      return BaseResponse()..setException = e;
    }
  }
}
