import 'package:sipolair/core/network/network_info.dart';
import 'package:sipolair/core/params/bimas_params.dart';
import 'package:sipolair/core/utils/exception.dart';
import 'package:sipolair/core/utils/pref_utils.dart';
import 'package:sipolair/datasources/bimas_source.dart';
import 'package:sipolair/datasources/file_source.dart';
import 'package:sipolair/datasources/master_data_source.dart';
import 'package:sipolair/models/base_response.dart';
import 'package:sipolair/models/bimas.dart';
import 'package:sipolair/models/zone.dart';

class BimasRepository {
  final BimasSource remoteSource;
  final FileSource fileSource;
  final MasterDataSource masterDataSource;
  // final ReportLocalDataSource localSource;
  final NetworkInfo networkInfo;

  BimasRepository({
    required this.remoteSource,
    required this.fileSource,
    required this.masterDataSource,
    // required this.localSource,
    required this.networkInfo,
  });

  Future<BaseResponse<List<Bimas>>> getListBimas(
      {String? title,
      String? pic,
      int? idZone,
      String? startDate,
      String? endDate}) async {
    int? _idZone =
        PrefUtil.instance.idAgency == 6 ? PrefUtil.instance.idZone : idZone;

    if (await networkInfo.isConnected) {
      try {
        var response = await remoteSource.getListBimas(
            title: title,
            pic: pic,
            idZone: _idZone,
            startDate: startDate,
            endDate: endDate);

        return BaseResponse()..setData = response;
      } on AppException catch (e) {
        return BaseResponse()..setException = e;
      }
    } else {
      // var localReports = await localSource.getReports();
      return BaseResponse()..setData = [];
    }
  }

  Future<BaseResponse<Bimas>> getDetailBimas({required int id}) async {
    try {
      var response = await remoteSource.getDetailBimas(id: id);

      return BaseResponse()..setData = response;
    } on AppException catch (e) {
      return BaseResponse()..setException = e;
    }
  }

  Future<BaseResponse<List<Zone>>> getZones() async {
    if (await networkInfo.isConnected) {
      try {
        var response = await masterDataSource.getZones();

        return BaseResponse()..setData = response;
      } on AppException catch (e) {
        return BaseResponse()..setException = e;
      }
    } else {
      // var localReports = await localSource.getReports();
      return BaseResponse()..setData = [];
    }
  }

  Future<BaseResponse<bool>> submitReport({required BimasParams params}) async {
    // if (await networkInfo.isConnected) {
    try {
      var idReport = await remoteSource.submitReport(params: params);

      bool response = true;

      await Future.forEach<String>(params.files ?? [], (e) async {
        var responseUpload = await fileSource.uploadFile(
            file: e, idReference: idReport, menu: 'binmas');
        response = responseUpload;
      });

      // params.files?.forEach((file) async {
      //   var responseUpload = await fileSource.uploadFile(
      //       file: file, idReference: idReport, menu: 'binmas');
      //   response = responseUpload;
      // });

      return BaseResponse()..setData = response;
    } on AppException catch (e) {
      return BaseResponse()..setException = e;
    }
    // } else {
    //   await localSource
    //       .submitReport(params.copyWith(idUser: PrefUtil.instance.idUser));
    //   return BaseResponse()..setData = false;
    // }
  }
}
