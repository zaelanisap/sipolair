import 'package:sipolair/datasources/profile_source.dart';
import 'package:sipolair/models/user.dart';
import '../core/utils/exception.dart';
import '../models/base_response.dart';

class ProfileRepository {
  final ProfileSource remoteSource;

  ProfileRepository({required this.remoteSource});

  Future<BaseResponse<User>> getProfile() async {
    try {
      var response = await remoteSource.getProfile();

      return BaseResponse()..setData = response;
    } on AppException catch (e) {
      return BaseResponse()..setException = e;
    }
  }
}
