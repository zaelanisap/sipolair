import 'package:sipolair/core/network/network_info.dart';
import 'package:sipolair/core/utils/pref_utils.dart';
import 'package:sipolair/datasources/file_source.dart';
import 'package:sipolair/datasources/report_local_source.dart';
import 'package:sipolair/models/report.dart';
import 'package:sipolair/models/report_detail.dart';
import 'package:sipolair/models/report_master_data.dart';

import '../core/params/report_params.dart';
import '../core/utils/exception.dart';
import '../datasources/report_source.dart';
import '../models/base_response.dart';

class ReportRepository {
  final ReportSource remoteSource;
  final FileSource fileSource;
  final ReportLocalDataSource localSource;
  final NetworkInfo networkInfo;

  ReportRepository({
    required this.remoteSource,
    required this.fileSource,
    required this.localSource,
    required this.networkInfo,
  });

  Future<BaseResponse<ReportList>> getReports(
      {String? keyword, String? startDate, String? endDate}) async {
    if (await networkInfo.isConnected) {
      try {
        var syncReports = await remoteSource.getReports(
            keyword: keyword, startDate: startDate, endDate: endDate);
        var localReports = await localSource.getReports();

        return BaseResponse()
          ..setData =
              ReportList(syncReports: syncReports, localReports: localReports);
      } on AppException catch (e) {
        return BaseResponse()..setException = e;
      }
    } else {
      var localReports = await localSource.getReports();
      return BaseResponse()
        ..setData = ReportList(syncReports: [], localReports: localReports);
    }
  }

  Future<BaseResponse<ReportDetail>> getDetailReport(
      {required int idReport}) async {
    try {
      var response = await remoteSource.getDetailReport(idReport: idReport);

      return BaseResponse()..setData = response;
    } on AppException catch (e) {
      return BaseResponse()..setException = e;
    }
  }

  Future<BaseResponse<ReportMasterData>> getReportMasterData() async {
    print(await networkInfo.isConnected);

    if (await networkInfo.isConnected) {
      try {
        var categories = await remoteSource.getCriminalCategories();
        var religions = await remoteSource.getReligions();
        var ships = await remoteSource.getShips();

        await localSource.cacheReportCategories(categories);
        await localSource.cacheReligions(religions);
        await localSource.cacheShips(ships);

        return BaseResponse()
          ..setData = ReportMasterData(
              religions: religions, categories: categories, ships: ships);
      } on AppException catch (e) {
        return BaseResponse()..setException = e;
      }
    } else {
      try {
        var categories = await localSource.getReportCategories();
        var religions = await localSource.getReligions();
        var ships = await localSource.getShips();

        return BaseResponse()
          ..setData = ReportMasterData(
              religions: religions, categories: categories, ships: ships);
      } on AppException catch (e) {
        return BaseResponse()..setException = e;
      }
    }
  }

  Future<BaseResponse<bool>> submitReport(
      {required ReportParams params}) async {
    if (await networkInfo.isConnected) {
      try {
        var idReport = await remoteSource.submitReport(
            params: params.copyWith(idUser: PrefUtil.instance.idUser));

        bool response = true;

        params.attachments?.forEach((file) async {
          var responseUpload = await fileSource.uploadFile(
              file: file!, idReference: idReport, menu: 'gakkum');
          response = responseUpload;
        });

        return BaseResponse()..setData = response;
      } on AppException catch (e) {
        return BaseResponse()..setException = e;
      }
    } else {
      await localSource
          .submitReport(params.copyWith(idUser: PrefUtil.instance.idUser));
      return BaseResponse()..setData = false;
    }
  }

  Future<BaseResponse<bool>> syncReports() async {
    try {
      var reports = await localSource.getReportParams();

      bool response = false;

      await Future.forEach<ReportParams>(reports, (e) async {
        var idReport = await remoteSource.submitReport(params: e);
        response = true;
        if (e.attachments != null)
          await Future.forEach<String?>(e.attachments!, (file) async {
            var responseUpload = await remoteSource.submitFilesReport(
                file: file!, idReport: idReport);
            response = responseUpload;
          });
      });

      if (response) {
        localSource.deleteReports();
      }

      return BaseResponse()..setData = response;
    } on AppException catch (e) {
      return BaseResponse()..setException = e;
    }
  }
}
