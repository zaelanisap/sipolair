import 'dart:async';
import 'package:rxdart/rxdart.dart';
import 'package:bloc/bloc.dart';
import 'package:sipolair/repositories/bimas_repository.dart';

import 'cubit.dart';

class BimasCubit extends Cubit<BimasState> {
  BimasCubit({required this.repository}) : super(BimasLoading()) {
    titleController = StreamController();
    titleSubs = titleController.stream
        .debounceTime(Duration(milliseconds: 500))
        .listen((event) {
      if (event != null)
        _fetchListBimas(
            title: event,
            pic: _pic,
            idZone: _idZone,
            startDate: _startDate,
            endDate: _endDate);
    });
  }
  final BimasRepository repository;

  late StreamController<String?> titleController;
  late StreamSubscription titleSubs;

  String? _title, _pic;
  String? _startDate, _endDate;
  int? _idZone;

  @override
  Future<void> close() {
    titleSubs.cancel();
    titleController.close();
    print('close');
    return super.close();
  }

  void getListBimas(
      {String? title,
      String? pic,
      int? idZone,
      String? startDate,
      String? endDate}) async {
    _title = title;
    _pic = pic;
    _idZone = idZone;
    _startDate = startDate;
    _endDate = endDate;

    titleController.add(title);

    if (title == null) _fetchListBimas();
  }

  void _fetchListBimas(
      {String? title,
      String? pic,
      int? idZone,
      String? startDate,
      String? endDate}) async {
    emit(BimasLoading());

    var response = await repository.getListBimas(
        title: title,
        pic: pic,
        idZone: idZone,
        startDate: startDate,
        endDate: endDate);

    if (response.getException != null) {
      var error = response.getException!;
      emit(BimasFailure(title: error.errorTitle, message: error.errorMessage));
      return;
    }

    emit(BimasListLoaded(listBimas: response.data!));
  }

  void getDetailBimas({required int id}) async {
    emit(BimasLoading());

    var response = await repository.getDetailBimas(id: id);

    if (response.getException != null) {
      var error = response.getException!;
      emit(BimasFailure(title: error.errorTitle, message: error.errorMessage));
      return;
    }

    emit(BimasDetailLoaded(bimas: response.data!));
  }
}
