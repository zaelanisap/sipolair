import 'package:equatable/equatable.dart';

import 'package:sipolair/models/bimas.dart';

abstract class BimasState extends Equatable {
  const BimasState();

  @override
  List<Object?> get props => [];
}

class BimasLoading extends BimasState {}

class BimasListLoaded extends BimasState {
  final List<Bimas> listBimas;
  BimasListLoaded({
    required this.listBimas,
  });

  @override
  List<Object?> get props => [listBimas];
}

class BimasDetailLoaded extends BimasState {
  final Bimas bimas;
  BimasDetailLoaded({
    required this.bimas,
  });

  @override
  List<Object?> get props => [bimas];
}

class BimasFailure extends BimasState {
  final String title;
  final String message;
  BimasFailure({
    required this.title,
    required this.message,
  });

  @override
  List<Object?> get props => [title, message];
}
