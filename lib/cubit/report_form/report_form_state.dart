import 'package:equatable/equatable.dart';

import 'package:sipolair/models/report_master_data.dart';

abstract class ReportFormState extends Equatable {
  const ReportFormState();

  @override
  List<Object> get props => [];
}

class ReportFormInitial extends ReportFormState {}

class ReportFormLoading extends ReportFormState {}

class ReportFormFailure extends ReportFormState {
  final String title;
  final String message;
  ReportFormFailure({
    required this.title,
    required this.message,
  });

  @override
  List<Object> get props => [title, message];
}

class ReportFormLoaded extends ReportFormState {
  final ReportMasterData data;
  ReportFormLoaded({
    required this.data,
  });

  @override
  List<Object> get props => [data];
}

class ReportSubmitLoading extends ReportFormState {}

class ReportSubmitFailure extends ReportFormState {
  final String title;
  final String message;
  ReportSubmitFailure({
    required this.title,
    required this.message,
  });

  @override
  List<Object> get props => [title, message];
}

class ReportFormSubmitted extends ReportFormState {
  final bool isSync;
  ReportFormSubmitted({
    required this.isSync,
  });
  @override
  List<Object> get props => [isSync];
}
