import 'package:bloc/bloc.dart';
import 'package:sipolair/core/params/report_params.dart';
import 'package:sipolair/repositories/report_repository.dart';

import 'cubit.dart';

class ReportFormCubit extends Cubit<ReportFormState> {
  ReportFormCubit(this.repository) : super(ReportFormInitial());

  final ReportRepository repository;

  void getReportForm() async {
    emit(ReportFormLoading());
    var data = await repository.getReportMasterData();

    if (data.getException != null) {
      var error = data.getException!;
      emit(ReportFormFailure(
        title: error.errorTitle,
        message: error.errorMessage,
      ));
      return;
    }

    emit(ReportFormLoaded(data: data.data!));
  }

  void submitReport(
      {required int idReportType, required ReportParams params}) async {
    print(params.toString());

    emit(ReportSubmitLoading());

    if (idReportType == 1) {
      if (params.isSuspectedEmpty) {
        emit(ReportSubmitFailure(
          title: 'Ada yang kurang...',
          message: 'Data Tersangka tidak boleh kosong',
        ));
        return;
      }

      if (params.isAttachmentEmpty) {
        emit(ReportSubmitFailure(
          title: 'Ada yang kurang...',
          message: 'Lampiran tidak boleh kosong',
        ));
        return;
      }
    }
    var data = await repository.submitReport(params: params);

    if (data.getException != null) {
      var error = data.getException!;
      emit(ReportSubmitFailure(
        title: error.errorTitle,
        message: error.errorMessage,
      ));
      return;
    }

    emit(ReportFormSubmitted(isSync: data.data!));
  }

  void syncReports() async {
    if (state is ReportSubmitLoading) return;

    emit(ReportSubmitLoading());

    var data = await repository.syncReports();

    if (data.getException != null) {
      var error = data.getException!;
      emit(ReportSubmitFailure(
        title: error.errorTitle,
        message: error.errorMessage,
      ));
      return;
    }

    emit(ReportFormSubmitted(isSync: true));
  }
}
