import 'package:equatable/equatable.dart';

import 'package:sipolair/models/report.dart';
import 'package:sipolair/models/report_detail.dart';

abstract class ReportState extends Equatable {
  const ReportState();

  @override
  List<Object> get props => [];
}

class ReportInitial extends ReportState {}

class ReportLoading extends ReportState {}

class ReportFailure extends ReportState {
  final String title;
  final String message;
  ReportFailure({
    required this.title,
    required this.message,
  });

  @override
  List<Object> get props => [title, message];
}

class EmptyReport extends ReportState {
  final String title;
  final String message;
  EmptyReport({
    required this.title,
    required this.message,
  });

  @override
  List<Object> get props => [title, message];
}

class ReportsLoaded extends ReportState {
  final ReportList reportList;
  ReportsLoaded({
    required this.reportList,
  });

  @override
  List<Object> get props => [reportList];
}

class ReportDetailLoaded extends ReportState {
  final ReportDetail report;
  ReportDetailLoaded({
    required this.report,
  });

  @override
  List<Object> get props => [report];
}

class ReportStatusLoaded extends ReportState {}
