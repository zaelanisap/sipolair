import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:sipolair/screen/helpers/util.dart';
import 'cubit.dart';
import '../../repositories/report_repository.dart';

class ReportCubit extends Cubit<ReportState> {
  ReportCubit(this.repository) : super(ReportInitial()) {
    report = StreamController();
    reportSubs = report.stream
        .debounceTime(Duration(milliseconds: 500))
        .listen((keyword) {
      fetchReports(keyword: keyword, startDate: _startDate, endDate: _endDate);
    });
  }

  final ReportRepository repository;

  late StreamController<String> report;
  late StreamSubscription reportSubs;
  String? _startDate, _endDate;

  @override
  Future<void> close() {
    reportSubs.cancel();
    report.close();
    return super.close();
  }

  void getReports({String? keyword, String? startDate, String? endDate}) async {
    String _keyword = keyword ?? '';
    DateTime dateTime = DateTime.now();

    String _start = startDate ??
        DateHelper.formatDateTime(
            dateTime.subtract(Duration(days: 10)), 'yyyy-MM-dd');
    String _end = endDate ?? DateHelper.formatDateTime(dateTime, 'yyyy-MM-dd');

    _startDate = _start;
    _endDate = _end;

    report.add(_keyword);
  }

  void fetchReports(
      {String? keyword, String? startDate, String? endDate}) async {
    emit(ReportLoading());

    var response = await repository.getReports(
        keyword: keyword, startDate: startDate, endDate: endDate);

    if (response.getException != null) {
      var error = response.getException!;
      emit(ReportFailure(title: error.errorTitle, message: error.errorMessage));
      return;
    }

    emit(ReportsLoaded(reportList: response.data!));
  }

  void getReportDetail({required int idReport}) async {
    emit(ReportLoading());

    var response = await repository.getDetailReport(idReport: idReport);

    if (response.getException != null) {
      var error = response.getException!;
      emit(ReportFailure(title: error.errorTitle, message: error.errorMessage));
      return;
    }

    emit(ReportDetailLoaded(report: response.data!));
  }
}
