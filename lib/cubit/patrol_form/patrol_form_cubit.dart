import 'package:bloc/bloc.dart';
import 'package:sipolair/core/params/patrol_params.dart';
import 'package:sipolair/repositories/patrol_repository.dart';

import 'cubit.dart';

class PatrolFormCubit extends Cubit<PatrolFormState> {
  PatrolFormCubit(this.repository) : super(PatrolFormLoading());

  final PatrolRepository repository;

  void getReportForm() async {
    emit(PatrolFormLoading());
    var data = await repository.getReportMasterData();

    if (data.getException != null) {
      var error = data.getException!;
      emit(PatrolFormFailure(
        title: error.errorTitle,
        message: error.errorMessage,
      ));
      return;
    }

    emit(PatrolFormLoaded(
        patrols: data.data!.patrols,
        ships: data.data!.ships,
        zones: data.data!.zones));
  }

  void submitReport({required PatrolParams params}) async {
    print(params.toString());

    emit(PatrolSubmitLoading());

    var response = await repository.submitReport(params: params);

    if (response.getException != null) {
      var error = response.getException!;
      emit(PatrolSubmitFailure(
        title: error.errorTitle,
        message: error.errorMessage,
      ));
      return;
    }

    emit(PatrolFormSubmitted(isSync: response.data!));
  }
}
