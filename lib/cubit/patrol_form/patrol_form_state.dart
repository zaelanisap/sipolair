import 'package:equatable/equatable.dart';
import 'package:sipolair/models/patrol.dart';
import 'package:sipolair/models/ship.dart';
import 'package:sipolair/models/zone.dart';

abstract class PatrolFormState extends Equatable {
  const PatrolFormState();

  @override
  List<Object?> get props => [];
}

class PatrolFormLoading extends PatrolFormState {}

class PatrolFormLoaded extends PatrolFormState {
  final List<Patrol> patrols;
  final List<Ship> ships;
  final List<Zone> zones;
  PatrolFormLoaded(
      {required this.patrols, required this.ships, required this.zones});

  @override
  List<Object?> get props => [patrols, ships, zones];
}

class PatrolFormFailure extends PatrolFormState {
  final String title;
  final String message;
  PatrolFormFailure({
    required this.title,
    required this.message,
  });

  @override
  List<Object?> get props => [title, message];
}

class PatrolSubmitLoading extends PatrolFormState {}

class PatrolSubmitFailure extends PatrolFormState {
  final String title;
  final String message;
  PatrolSubmitFailure({
    required this.title,
    required this.message,
  });

  @override
  List<Object?> get props => [title, message];
}

class PatrolFormSubmitted extends PatrolFormState {
  final bool isSync;
  PatrolFormSubmitted({
    required this.isSync,
  });

  @override
  List<Object?> get props => [isSync];
}
