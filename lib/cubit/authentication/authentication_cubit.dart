import 'package:bloc/bloc.dart';
import '../../core/params/register_params.dart';
import 'cubit.dart';
import '../../repositories/authentication_repository.dart';
import '../../core/params/login_params.dart';

class AuthenticationCubit extends Cubit<AuthenticationState> {
  AuthenticationCubit(this.repository) : super(AuthenticationInitial());

  final AuthenticationRepository repository;

  void register({required RegisterParams params}) async {
    emit(AuthenticationLoading());

    var response = await repository.register(params);

    if (response.getException != null) {
      var error = response.getException;
      emit(AuthenticationFailure(
          title: error!.errorTitle, message: error.errorMessage));
      return;
    }

    emit(RegisterSuccess());
  }

  void login({required String email, required String password}) async {
    emit(AuthenticationLoading());

    var response =
        await repository.login(LoginParams(email: email, password: password));

    if (response.getException != null) {
      var error = response.getException;
      emit(AuthenticationFailure(
          title: error!.errorTitle, message: error.errorMessage));
      return;
    }

    emit(LoginSuccess());
  }

  void logout() async {
    emit(AuthenticationLoading());

    var response = await repository.logout();

    if (response.getException != null) {
      var error = response.getException;
      emit(AuthenticationFailure(
          title: error!.errorTitle, message: error.errorMessage));
      return;
    }

    emit(LogoutSuccess());
  }
}
