import 'package:equatable/equatable.dart';

abstract class AuthenticationState extends Equatable {
  const AuthenticationState();

  @override
  List<Object> get props => [];
}

class AuthenticationInitial extends AuthenticationState {}

class AuthenticationLoading extends AuthenticationState {}

class AuthenticationFailure extends AuthenticationState {
  final String title;
  final String message;
  AuthenticationFailure({
    required this.title,
    required this.message,
  });
}

class RegisterSuccess extends AuthenticationState {}

class LoginSuccess extends AuthenticationState {}

class LogoutSuccess extends AuthenticationState {}
