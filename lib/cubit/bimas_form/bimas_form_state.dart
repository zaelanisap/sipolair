import 'package:equatable/equatable.dart';

import 'package:sipolair/models/zone.dart';

abstract class BimasFormState extends Equatable {
  const BimasFormState();

  @override
  List<Object?> get props => [];
}

class BimasFormLoading extends BimasFormState {}

class BimasFormLoaded extends BimasFormState {
  final List<Zone> zones;
  BimasFormLoaded({
    required this.zones,
  });

  @override
  List<Object?> get props => [zones];
}

class BimasFormFailure extends BimasFormState {
  final String title;
  final String message;
  BimasFormFailure({
    required this.title,
    required this.message,
  });

  @override
  List<Object?> get props => [title, message];
}

class BimasSubmitLoading extends BimasFormState {}

class BimasSubmitFailure extends BimasFormState {
  final String title;
  final String message;
  BimasSubmitFailure({
    required this.title,
    required this.message,
  });

  @override
  List<Object?> get props => [title, message];
}

class BimasFormSubmitted extends BimasFormState {
  final bool isSync;
  BimasFormSubmitted({
    required this.isSync,
  });

  @override
  List<Object?> get props => [isSync];
}
