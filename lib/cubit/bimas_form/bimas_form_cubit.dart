import 'package:bloc/bloc.dart';
import 'package:sipolair/core/params/bimas_params.dart';
import 'package:sipolair/repositories/bimas_repository.dart';

import 'cubit.dart';

class BimasFormCubit extends Cubit<BimasFormState> {
  BimasFormCubit(this.repository) : super(BimasFormLoading());

  final BimasRepository repository;

  void getReportForm() async {
    emit(BimasFormLoading());
    var data = await repository.getZones();

    if (data.getException != null) {
      var error = data.getException!;
      emit(BimasFormFailure(
        title: error.errorTitle,
        message: error.errorMessage,
      ));
      return;
    }

    emit(BimasFormLoaded(zones: data.data!));
  }

  void submitReport({required BimasParams params}) async {
    print(params.toString());

    emit(BimasSubmitLoading());

    var response = await repository.submitReport(params: params);

    if (response.getException != null) {
      var error = response.getException!;
      emit(BimasSubmitFailure(
        title: error.errorTitle,
        message: error.errorMessage,
      ));
      return;
    }

    emit(BimasFormSubmitted(isSync: response.data!));
  }
}
