import 'package:bloc/bloc.dart';
import 'package:sipolair/repositories/profile_repository.dart';

import 'cubit.dart';

class ProfileCubit extends Cubit<ProfileState> {
  ProfileCubit(this.repository) : super(ProfileInitial());

  final ProfileRepository repository;

  void getProfile() async {
    emit(ProfileLoading());
    var response = await repository.getProfile();

    if (response.getException != null) {
      var error = response.getException!;
      emit(
          ProfileFailure(title: error.errorTitle, message: error.errorMessage));
      return;
    }

    emit(ProfileLoaded(user: response.data!));
  }
}
