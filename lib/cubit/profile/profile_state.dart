import 'package:equatable/equatable.dart';
import 'package:sipolair/models/user.dart';

abstract class ProfileState extends Equatable {
  const ProfileState();

  @override
  List<Object> get props => [];
}

class ProfileInitial extends ProfileState {}

class ProfileLoading extends ProfileState {}

class ProfileFailure extends ProfileState {
  final String title;
  final String message;
  ProfileFailure({
    required this.title,
    required this.message,
  });
}

class ProfileLoaded extends ProfileState {
  final User user;
  ProfileLoaded({
    required this.user,
  });

  @override
  List<Object> get props => [user];
}
