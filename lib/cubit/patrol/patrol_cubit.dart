import 'package:bloc/bloc.dart';
import 'package:sipolair/repositories/patrol_repository.dart';

import 'cubit.dart';

class PatrolCubit extends Cubit<PatrolState> {
  PatrolCubit(this.repository) : super(PatrolLoading());

  final PatrolRepository repository;

  void getListReport(
      {int? idPatrol, int? idZone, String? startDate, String? endDate}) async {
    emit(PatrolLoading());

    var response = await repository.getListReport(
        idPatrol: idPatrol,
        idZone: idZone,
        startDate: startDate,
        endDate: endDate);

    if (response.getException != null) {
      var error = response.getException!;
      emit(PatrolFailure(title: error.errorTitle, message: error.errorMessage));
      return;
    }

    emit(PatrolListLoaded(listPatrol: response.data!));
  }

  void getDetailReport({required int id}) async {
    emit(PatrolLoading());

    var response = await repository.getDetailReport(id: id);

    if (response.getException != null) {
      var error = response.getException!;
      emit(PatrolFailure(title: error.errorTitle, message: error.errorMessage));
      return;
    }

    emit(PatrolDetailLoaded(report: response.data!));
  }
}
