import 'package:equatable/equatable.dart';
import 'package:sipolair/models/patrol_report.dart';

abstract class PatrolState extends Equatable {
  const PatrolState();

  @override
  List<Object?> get props => [];
}

class PatrolLoading extends PatrolState {}

class PatrolListLoaded extends PatrolState {
  final List<PatrolReport> listPatrol;
  PatrolListLoaded({
    required this.listPatrol,
  });

  @override
  List<Object?> get props => [listPatrol];
}

class PatrolDetailLoaded extends PatrolState {
  final PatrolReport report;
  PatrolDetailLoaded({
    required this.report,
  });

  @override
  List<Object?> get props => [report];
}

class PatrolFailure extends PatrolState {
  final String title;
  final String message;
  PatrolFailure({
    required this.title,
    required this.message,
  });

  @override
  List<Object?> get props => [title, message];
}
