import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:get_storage/get_storage.dart';
import 'package:sipolair/core/params/report_params.dart';
import 'package:sipolair/core/utils/exception.dart';
import 'package:sipolair/models/criminal_category.dart';
import 'package:sipolair/models/religion.dart';
import 'package:sipolair/models/ship.dart';

import 'package:sipolair/models/report.dart';

abstract class ReportLocalDataSource {
  Future<List<Report>> getReports();
  Future<List<ReportParams>> getReportParams();
  Future<List<Religion>> getReligions();
  Future<List<CriminalCategory>> getReportCategories();
  Future<List<Ship>> getShips();
  Future<void> cacheReligions(List<Religion> value);
  Future<void> cacheReportCategories(List<CriminalCategory> value);
  Future<void> cacheShips(List<Ship> value);
  Future<void> submitReport(ReportParams params);
  Future<void> deleteReports();
}

const String reportsKey = 'report_list';
const String religionsKey = 'religion_list';
const String reportCategoriesKey = 'report_categories';
const String shipsKey = 'ship_list';

class ReportGetStorageSource extends ReportLocalDataSource {
  final GetStorage storage;
  ReportGetStorageSource({
    required this.storage,
  });
  @override
  Future<List<Report>> getReports() async {
    final json = storage.read(reportsKey);

    if (json != null) {
      List response = jsonDecode(json);
      List<ReportParams> list =
          response.map((e) => ReportParams.fromJson(e)).toList();

      List<Report> reports = [];

      list.forEach((e) async {
        CriminalCategory? category = e.idCategory != null
            ? await _getReportCategoriesById(e.idCategory!)
            : null;
        reports.add(Report(
          id: 0,
          code: e.code,
          idUser: e.idUser!,
          idShip: e.idShip,
          officer: e.officer!,
          zone: null,
          category: category,
          tkp: e.address!,
          chronology: e.chronology!,
          evidence: e.evidence!,
          article: e.article!,
          date: e.date!,
          time: e.time!,
        ));
      });
      return reports;
    }
    return [];
  }

  @override
  Future<List<ReportParams>> getReportParams() async {
    final json = storage.read(reportsKey);

    if (json != null) {
      List response = jsonDecode(json);
      return response.map((e) => ReportParams.fromJson(e)).toList();
    }
    return [];
  }

  @override
  Future<void> submitReport(ReportParams params) {
    final json = storage.read(reportsKey);

    if (json != null) {
      List response = jsonDecode(json);

      return storage.write(
          reportsKey, jsonEncode(response..add(params.toJson())));
    } else {
      debugPrint('Cache report:' + jsonEncode([params.toJson()]));

      return storage.write(reportsKey, jsonEncode([params.toJson()]));
    }
  }

  @override
  Future<void> cacheReligions(List<Religion> value) async {
    var list = value.map((e) => e.toJson()).toList();
    await storage.write(religionsKey, jsonEncode(list));
    debugPrint('Cache Religion: ${storage.read(religionsKey)}');
  }

  @override
  Future<void> cacheReportCategories(List<CriminalCategory> value) async {
    var list = value.map((e) => e.toJson()).toList();

    await storage.write(reportCategoriesKey, jsonEncode(list));
    debugPrint('Cache Categories: ${storage.read(reportCategoriesKey)}');
  }

  @override
  Future<List<Religion>> getReligions() async {
    debugPrint('Religion: ${storage.read(religionsKey)}');

    final json = storage.read(religionsKey);

    if (json != null) {
      List response = jsonDecode(json);

      return response.map((e) => Religion.fromJson(e)).toList();
    }
    throw AppException.withMessage(
        'No Data', 'There is no data in your local disk',
        code: 0);
  }

  @override
  Future<List<CriminalCategory>> getReportCategories() async {
    debugPrint('Category: ${storage.read(reportCategoriesKey)}');

    final json = storage.read(reportCategoriesKey);

    if (json != null) {
      List response = jsonDecode(json);

      return response.map((e) => CriminalCategory.fromJson(e)).toList();
    }
    throw AppException.withMessage(
        'No Data', 'There is no data in your local disk',
        code: 0);
  }

  Future<CriminalCategory?> _getReportCategoriesById(int id) async {
    debugPrint('Category: ${storage.read(reportCategoriesKey)}');

    try {
      final categories = await getReportCategories();
      return categories.singleWhere((e) => e.id == id);
    } catch (_) {
      return null;
    }
  }

  @override
  Future<void> deleteReports() {
    debugPrint('Delete all report');
    return storage.remove(reportsKey);
  }

  @override
  Future<void> cacheShips(List<Ship> value) async {
    var list = value.map((e) => e.toJson()).toList();

    await storage.write(shipsKey, jsonEncode(list));
    debugPrint('Cache Ships: ${storage.read(shipsKey)}');
  }

  @override
  Future<List<Ship>> getShips() async {
    final json = storage.read(shipsKey);

    if (json != null) {
      List response = jsonDecode(json);

      return response.map((e) => Ship.fromJson(e)).toList();
    }
    throw AppException.withMessage('No Data', 'No data', code: 0);
  }
}
