import 'package:dio/dio.dart';
import 'package:sipolair/core/utils/exception.dart';
import 'package:sipolair/core/utils/header_interceptor.dart';
import 'package:sipolair/core/utils/logging_interceptor.dart';
import 'package:sipolair/core/utils/strings.dart';
import 'package:sipolair/models/patrol.dart';
import 'package:sipolair/models/religion.dart';
import 'package:sipolair/models/ship.dart';
import 'package:sipolair/models/zone.dart';

class MasterDataSource {
  late Dio _dio;
  MasterDataSource() {
    _dio = Dio();
    _dio.options
      ..baseUrl = Strings.BASE_URL
      ..connectTimeout = 15000
      ..sendTimeout = 15000
      ..receiveTimeout = 15000
      ..contentType = Headers.formUrlEncodedContentType;
    _dio.interceptors.addAll([LoggingInterceptor(), HeaderInterceptor()]);
  }

  Future<List<Ship>> getShips() async {
    var response = await _getMasterData('api/master-data/ships');
    return response.map((e) => Ship.fromJson(e)).toList();
  }

  Future<List<Religion>> getReligions() async {
    var response = await _getMasterData('api/master-data/religions');
    return response.map((e) => Religion.fromJson(e)).toList();
  }

  Future<List<Zone>> getZones() async {
    var response = await _getMasterData('api/master-data/zones');
    return response.map((e) => Zone.fromJson(e)).toList();
  }

  Future<List<Patrol>> getPatrols() async {
    var response = await _getMasterData('api/master-data/patrols');
    return response.map((e) => Patrol.fromJson(e)).toList();
  }

  Future<List> _getMasterData(String url) async {
    try {
      var response = await _dio.get(url,
          options: Options(headers: {'requirestoken': true}));
      return response.data['data'];
    } on DioError catch (e) {
      throw AppException.withError(e);
    }
  }
}
