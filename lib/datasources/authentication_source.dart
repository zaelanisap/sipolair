import 'package:dio/dio.dart';
import 'package:sipolair/core/utils/pref_utils.dart';
import 'package:sipolair/models/login_response.dart';
import '../core/utils/strings.dart';
import '../core/utils/logging_interceptor.dart';
import '../core/utils/header_interceptor.dart';
import '../core/utils/exception.dart';
import '../core/params/login_params.dart';
import '../core/params/register_params.dart';

class AuthenticationSource {
  late Dio _dio;
  AuthenticationSource() {
    _dio = Dio();
    _dio.options
      ..baseUrl = Strings.BASE_URL
      ..connectTimeout = 15000
      ..sendTimeout = 15000
      ..receiveTimeout = 15000
      ..contentType = Headers.formUrlEncodedContentType;
    _dio.interceptors.addAll([LoggingInterceptor(), HeaderInterceptor()]);
  }

  Future<bool> register(RegisterParams params) async {
    try {
      var response = await _dio.post('api/register', data: params.toJson());
      bool isSuccess = response.data['success'];
      return isSuccess;
    } on DioError catch (e) {
      throw AppException.withError(e);
    }
  }

  Future<LoginResponse> login(LoginParams params) async {
    try {
      var response = await _dio.post('api/login', data: params.toJson());
      LoginResponse data = LoginResponse.fromJson(response.data['data']);
      return data;
    } on DioError catch (e) {
      throw AppException.withError(e);
    }
  }

  Future<bool> logout() async {
    try {
      var response = await _dio.post(
        'api/logout',
        queryParameters: {
          'requiresuserid': true,
          'id_token': PrefUtil.instance.token
        },
        options: Options(headers: {'requirestoken': true}),
      );
      bool isSuccess = response.data['success'] ?? false;
      return isSuccess;
    } on DioError catch (e) {
      throw AppException.withError(e);
    }
  }
}
