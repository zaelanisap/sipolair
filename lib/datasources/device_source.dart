import 'dart:io';

import 'package:file_picker/file_picker.dart';

class DeviceSource {
  Future<List<String?>> getMultipleFile({bool allowMultiple = false}) async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowMultiple: true,
      allowedExtensions: ['jpg', 'png', 'jpeg' 'pdf', 'doc'],
    );

    List<String?> files =
        result?.paths.map((e) => File(e!).path).toList() ?? [];

    return files;
  }
}
