import 'package:dio/dio.dart';
import 'package:sipolair/core/utils/pref_utils.dart';
import 'package:sipolair/models/criminal_category.dart';
import 'package:sipolair/models/religion.dart';
import 'package:sipolair/models/report.dart';
import 'package:sipolair/models/report_detail.dart';
import 'package:sipolair/models/ship.dart';
import '../core/utils/strings.dart';
import '../core/utils/logging_interceptor.dart';
import '../core/utils/header_interceptor.dart';
import '../core/utils/exception.dart';
import '../core/params/report_params.dart';

class ReportSource {
  late Dio _dio;
  ReportSource() {
    _dio = Dio();
    _dio.options
      ..baseUrl = Strings.BASE_URL
      ..connectTimeout = 15000
      ..sendTimeout = 15000
      ..receiveTimeout = 15000
      ..contentType = Headers.formUrlEncodedContentType;
    _dio.interceptors.addAll([LoggingInterceptor(), HeaderInterceptor()]);
  }

  Future<List<Report>> getReports(
      {String? keyword, String? startDate, String? endDate}) async {
    try {
      var response = await _dio.get(
        'api/report',
        queryParameters: {
          'keyword': keyword,
          'start_date': startDate,
          'end_date': endDate,
          'requiresuserid': true
        },
        options: Options(headers: {'requirestoken': true}),
      );
      List list = response.data['data'];
      return list.map((e) => Report.fromJson(e)).toList();
    } on DioError catch (e) {
      throw AppException.withError(e);
    }
  }

  Future<ReportDetail> getDetailReport({required int idReport}) async {
    try {
      var response = await _dio.get(
        'api/report/detail',
        queryParameters: {'id_report': idReport},
        options: Options(headers: {'requirestoken': true}),
      );
      ReportDetail report = ReportDetail.fromJson(response.data['data']);
      return report;
    } on DioError catch (e) {
      throw AppException.withError(e);
    }
  }

  Future<List<CriminalCategory>> getCriminalCategories() async {
    try {
      var response = await _dio.get('api/master-data/criminal-categories',
          options: Options(headers: {'requirestoken': true}));
      List categories = response.data['data'];
      return categories.map((e) => CriminalCategory.fromJson(e)).toList();
    } on DioError catch (e) {
      throw AppException.withError(e);
    }
  }

  Future<List<Ship>> getShips() async {
    try {
      var response = await _dio.get('api/master-data/ships',
          options: Options(headers: {'requirestoken': true}));
      List ships = response.data['data'];
      return ships.map((e) => Ship.fromJson(e)).toList();
    } on DioError catch (e) {
      throw AppException.withError(e);
    }
  }

  Future<List<Religion>> getReligions() async {
    try {
      var response = await _dio.get('api/master-data/religions',
          options: Options(headers: {'requirestoken': true}));
      List religions = response.data['data'];
      return religions.map((e) => Religion.fromJson(e)).toList();
    } on DioError catch (e) {
      throw AppException.withError(e);
    }
  }

  Future<int> submitReport({required ReportParams params}) async {
    try {
      var response = await _dio.post(
        'api/report/submit',
        data: params.toJson(),
        options: Options(headers: {'requirestoken': true}),
      );

      int idReport = response.data['data']['id_report'];
      return idReport;
    } on DioError catch (e) {
      throw AppException.withError(e);
    }
  }

  Future<bool> submitFilesReport(
      {required String file, required int idReport}) async {
    FormData data = FormData.fromMap({
      'file': await MultipartFile.fromFile(file),
      'id_reference': idReport,
      'id_user': PrefUtil.instance.idUser
    });
    try {
      var response = await _dio.post(
        'api/report/upload',
        data: data,
        options: Options(headers: {'requirestoken': true}),
      );

      bool isSuccess = response.data['success'];
      return isSuccess;
    } on DioError catch (e) {
      throw AppException.withError(e);
    }
  }
}
