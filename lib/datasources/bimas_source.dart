import 'package:dio/dio.dart';
import 'package:sipolair/core/params/bimas_params.dart';
import 'package:sipolair/core/utils/exception.dart';
import 'package:sipolair/core/utils/header_interceptor.dart';
import 'package:sipolair/core/utils/logging_interceptor.dart';
import 'package:sipolair/core/utils/pref_utils.dart';
import 'package:sipolair/core/utils/strings.dart';
import 'package:sipolair/models/bimas.dart';

class BimasSource {
  late Dio _dio;
  BimasSource() {
    _dio = Dio();
    _dio.options
      ..baseUrl = Strings.BASE_URL
      ..connectTimeout = 15000
      ..sendTimeout = 15000
      ..receiveTimeout = 15000
      ..contentType = Headers.formUrlEncodedContentType;
    _dio.interceptors.addAll([LoggingInterceptor(), HeaderInterceptor()]);
  }

  Future<List<Bimas>> getListBimas(
      {String? title,
      String? pic,
      int? idZone,
      String? startDate,
      String? endDate}) async {
    try {
      var response = await _dio.get(
        'api/binmas',
        queryParameters: {
          'title': title,
          'pic': pic,
          'zone': idZone,
          'start_date': startDate,
          'end_date': endDate
        },
        options: Options(headers: {'requirestoken': true}),
      );
      List list = response.data['data'];
      return list.map((e) => Bimas.fromJson(e)).toList();
    } on DioError catch (e) {
      throw AppException.withError(e);
    }
  }

  Future<Bimas> getDetailBimas({required int id}) async {
    try {
      var response = await _dio.get(
        'api/binmas/detail',
        queryParameters: {'id': id},
        options: Options(headers: {'requirestoken': true}),
      );
      return Bimas.fromJson(response.data['data']);
    } on DioError catch (e) {
      throw AppException.withError(e);
    }
  }

  Future<int> submitReport({required BimasParams params}) async {
    Map<String, dynamic> data = {
      'id_user': PrefUtil.instance.idUser,
      'id_zone': PrefUtil.instance.idZone
    };
    var _params = params.toJson()..remove('files');

    data.addAll(_params);
    try {
      var response = await _dio.post(
        'api/binmas/submit',
        data: data,
        options: Options(headers: {'requirestoken': true}),
      );

      int idReport = response.data['data']['id'];
      return idReport;
    } on DioError catch (e) {
      throw AppException.withError(e);
    }
  }
}
