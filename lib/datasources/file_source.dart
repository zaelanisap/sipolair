import 'package:dio/dio.dart';
import 'package:sipolair/core/utils/exception.dart';
import 'package:sipolair/core/utils/header_interceptor.dart';
import 'package:sipolair/core/utils/logging_interceptor.dart';
import 'package:sipolair/core/utils/pref_utils.dart';
import 'package:sipolair/core/utils/strings.dart';

class FileSource {
  late Dio _dio;
  FileSource() {
    _dio = Dio();
    _dio.options
      ..baseUrl = Strings.BASE_URL
      ..connectTimeout = 15000
      ..sendTimeout = 15000
      ..receiveTimeout = 15000
      ..contentType = Headers.formUrlEncodedContentType;
    _dio.interceptors.addAll([LoggingInterceptor(), HeaderInterceptor()]);
  }

  Future<bool> uploadFile(
      {required String file,
      required int idReference,
      required String menu}) async {
    FormData data = FormData.fromMap({
      'file': await MultipartFile.fromFile(file),
      'id_reference': idReference,
      'table': menu,
      'id_user': PrefUtil.instance.idUser
    });
    try {
      var response = await _dio.post(
        'api/upload-file',
        data: data,
        options: Options(headers: {'requirestoken': true}),
      );

      bool isSuccess = response.data['success'];
      return isSuccess;
    } on DioError catch (e) {
      throw AppException.withError(e);
    }
  }
}
