import 'package:dio/dio.dart';
import 'package:sipolair/models/user.dart';
import '../core/utils/strings.dart';
import '../core/utils/logging_interceptor.dart';
import '../core/utils/header_interceptor.dart';
import '../core/utils/exception.dart';

class ProfileSource {
  late Dio _dio;
  ProfileSource() {
    _dio = Dio();
    _dio.options
      ..baseUrl = Strings.BASE_URL
      ..connectTimeout = 15000
      ..sendTimeout = 15000
      ..receiveTimeout = 15000
      ..contentType = Headers.formUrlEncodedContentType;
    _dio.interceptors.addAll([LoggingInterceptor(), HeaderInterceptor()]);
  }

  Future<User> getProfile() async {
    try {
      var response = await _dio.get('api/profile',
          queryParameters: {'requiresuserid': true},
          options: Options(headers: {'requirestoken': true}));
      User user = User.fromJson(response.data['data']);
      return user;
    } on DioError catch (e) {
      throw AppException.withError(e);
    }
  }

  Future<bool> updateFCMToken(String? token) async {
    try {
      var response = await _dio.post(
        'api/profile/update-token',
        data: {'fcm_token': token},
        queryParameters: {'requiresuserid': true},
        options: Options(headers: {'requirestoken': true}),
      );
      bool isSuccess = response.data['success'] ?? false;
      return isSuccess;
    } on DioError catch (e) {
      throw AppException.withError(e);
    }
  }
}
